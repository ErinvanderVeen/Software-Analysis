-- vim: sw=2 ts=2 et ai:
module Main where

import Control.Monad
import Data.ByteString (hGetContents)
import Data.LLVM.BitCode
import Data.Maybe
import Data.List (delete, intersect, (\\), union)
import Debug.Trace
import System.IO (putStrLn, openFile, IOMode(ReadMode))
import Text.LLVM.AST

argsFix :: Define -> BasicBlock -> ([Ident], [Ident])
argsFix func bb =
  let iOld = argsIn func bb [] in
  let oOld = argsOut func bb [] in
  argsFix' func bb iOld oOld
  where
    argsFix' :: Define -> BasicBlock -> [Ident] -> [Ident] -> ([Ident], [Ident])
    argsFix' func bb iOld oOld
      | iNew == iOld && oNew == oOld = (iNew, oNew)
      | otherwise = argsFix' func bb iNew oNew
      where
        iNew = argsIn func bb oOld
        oNew = argsOut func bb iOld

argsOut :: Define -> BasicBlock -> [Ident] -> [Ident]
argsOut = collectPhimap

nextBlocks :: Define -> BasicBlock -> [BasicBlock]
nextBlocks func bb = let blocks = defBody func in
  filter (\b -> any (\x -> hasLabel b x) (blockDests bb)) blocks
  where
    blockDests :: BasicBlock -> [Ident]
    blockDests bb = map labelToIdent (brTargets bb)

    labelToIdent :: BlockLabel -> Ident
    labelToIdent (Named i) = i
    labelToIdent (Anon i) = (Ident (show i))

    hasLabel :: BasicBlock -> Ident -> Bool
    hasLabel bb ident = case bbLabel bb of
      Just lab -> (labelToIdent lab) == ident
      Nothing -> False

    traceLabel :: BasicBlock -> a -> a
    traceLabel bb = traceShow ((fromJust . bbLabel) bb)

collectPhimap :: Define -> BasicBlock -> [Ident] -> [Ident]
collectPhimap func bb aIn = let dests = nextBlocks func bb in
  foldr (\x xs -> phimap func bb x ++ xs) [] dests
  where
    phimap :: Define -> BasicBlock -> BasicBlock -> [Ident]
    phimap func from to =
      let from_vars = vars from in
      let to_vars = aIn in
      intersect from_vars to_vars

    getPhiInst :: [Stmt] -> [Instr]
    getPhiInst xs = takeWhile isPhi (map stmtInstr xs)

    getPhiVars :: [Instr] -> [Ident]
    getPhiVars = foldr (\x xs -> getPhiVars' x ++ xs) []

    -- Get all variables on which a phi instruction depends
    getPhiVars' :: Instr -> [Ident]
    getPhiVars' (Phi _ vs) = let idents = map (getIdent . fst) vs in
      let vars' = filter isJust idents in
      map fromJust vars'

argsIn :: Define -> BasicBlock -> [Ident] -> [Ident]
argsIn func bb aOut
  | bbEq bb (head (getBasicBlocks func)) = getFuncArgs func
  | otherwise = (aOut \\ (kill bb)) `union` (gen bb)

getFuncArgs :: Define -> [Ident]
getFuncArgs func = map typedValue (defArgs func)

-- Given a Basic Block, returns all used variables (similar to kill)
vars :: BasicBlock -> [Ident]
vars bb = vars' (bbStmts bb)
  where
    vars' :: [Stmt] -> [Ident]
    vars' (s:ss) =
      case getStmtIdent s of
        Just i
          | elem i xs -> xs
          | otherwise -> (i:xs)
        Nothing -> xs
      where xs = vars' ss
    vars' [] = []

-- Tests Equality on Basic Blocks based on their Label
bbEq :: BasicBlock -> BasicBlock -> Bool
bbEq x y = ((fromJust . bbLabel) x) == ((fromJust . bbLabel) y)

-- Calculates genBlk
genBlk :: BasicBlock -> [Ident]
genBlk bb = let branch = getBranch bb in
  case branch of
    Just b -> case getBranchVar b of
      Just var -> (slice bb var)
      Nothing -> []
    Nothing -> []

-- Calculates genFn
-- We always return [] since we do not have calls in our benchmark
genFn :: BasicBlock -> [Ident]
genFn _ = []

-- Calculates gen (the union of genBlk and genFn)
gen :: BasicBlock -> [Ident]
gen bb = genBlk bb ++ (genFn bb)

-- Returns the list of Idents that should be killed
kill :: BasicBlock -> [Ident]
kill bb = kill' (bbStmts bb)
  where
    kill' :: [Stmt] -> [Ident]
    kill' (s:ss)
      | shouldKill s = case getStmtIdent s of
        Just i
          | elem i xs -> xs
          | otherwise -> (i:xs)
        Nothing -> xs
      | otherwise = xs
      where xs = kill' ss
    kill' [] = []

    shouldKill :: Stmt -> Bool
    shouldKill (Result _ inst _) = shouldKill' inst
    shouldKill _ = False

    -- NOTE: Very Dangerous, should check if actually true
    shouldKill' :: Instr -> Bool
    shouldKill' (Phi _ _) = False
    shouldKill' _ = True

-- Given a variable, performs symbolic evaluation and returns the variables
-- that influence the original
slice :: BasicBlock -> Ident -> [Ident]
slice bb ident = do
  result <- slice' [ident] ((reverse . bbStmts) bb)
  return result
  where
    slice' :: [Ident] -> [Stmt] -> [Ident]
    slice' idents (x:xs) = case getStmtIdent x of
      Just ri
        | elem ri idents -> let vars = (getSliceVars ri x) in
          let idents' = filter (\x -> x /= ri) idents in
          slice' (idents' ++ vars) xs
        | otherwise -> slice' idents xs
      _ -> slice' idents xs
    slice' idents [] = idents

-- Given a Stmt returns the Identifier of the variable that is assigned to
-- if it exists
getStmtIdent :: Stmt -> Maybe Ident
getStmtIdent (Result ident _ _) = Just ident
getStmtIdent _ = Nothing

-- Given one Ident and one Typed Value, checks if they reference the same thing
eqTypedValue :: Ident -> Typed Value -> Bool
eqTypedValue ident typed = case typedValue typed of
  ValIdent tident -> ident == tident
  _ -> False

-- Given a Value returns its identifier, if it exists.
getIdent :: Value -> Maybe Ident
getIdent (ValIdent ident) = Just ident
getIdent _ = Nothing

-- Given a Typed Value returns its identifier, if it exists.
getTypedIdent :: Typed Value -> Maybe Ident
getTypedIdent = getIdent . typedValue

-- Given a Ident and a statement, checks if the statement influences the
-- identifier. If so, return the Identifiers that influence it.
getSliceVars :: Ident -> Stmt -> [Ident]
getSliceVars ident stmt = case stmt of
  Result ident' (inst) _
    | ident == ident' -> map fromJust (filter isJust (getSliceVars' inst))
    | otherwise -> []
  _ -> []
  where
    getSliceVars' :: Instr -> [Maybe Ident]
    getSliceVars' (Arith _ v1 v2) = [getTypedIdent v1, getIdent v2]
    getSliceVars' (Bit _ v1 v2) = [getTypedIdent v1, getIdent v2]
    getSliceVars' (ICmp _ v1 v2) = [getTypedIdent v1, getIdent v2]
    getSliceVars' (Phi _ xs) = map (getIdent . fst) xs
    getSliceVars' i = error ("Could not match instruction: " ++ (show i))

-- Given a Function, return its basic blocks
getBasicBlocks :: Define -> [BasicBlock]
getBasicBlocks = defBody

-- Given a Basic Block return its branch instruction.
-- Note: Branch instructions are always the last instruction in a basic block
getBranch :: BasicBlock -> Maybe Instr
getBranch bb = case last (bbStmts bb) of
  Effect inst _ -> Just inst
  _ -> Nothing

-- Given a Branch Instruction, return the condition if it exists
getBranchVar :: Instr -> Maybe Ident
getBranchVar (Br val _ _) = case typedValue val of
  ValIdent ident -> Just ident
  _ -> Nothing
getBranchVar _ = Nothing

main :: IO ()
main = do
  handle <- openFile "sources/cpp/cpp.bc" ReadMode
  bitcode <- hGetContents handle
  mod <- parseBitCode bitcode
  case mod of
    Left e  -> putStrLn (errMessage e)
    Right m ->
      let func = (head . modDefines) m in
      let blocks = (getBasicBlocks func) in
      do
        mapM_ (putStrLn . show . (argsFix func)) blocks
