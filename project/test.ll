define zeroext i1 @_Z1fbb(i1 zeroext, i1 zeroext) #0 {
	%3 = alloca i8, align 1
	%4 = alloca i8, align 1
	%5 = alloca i8, align 1
	%6 = zext i1 %0 to i8
	store i8 %6, i8* %3, align 1
	%7 = zext i1 %1 to i8
	store i8 %7, i8* %4, align 1
	%8 = load i8, i8* %3, align 1
	%9 = trunc i8 %8 to i1
	br i1 %9, label %13, label %10

	; <label>:10: ; preds = %2
	%11 = load i8, i8* %4, align 1
	%12 = trunc i8 %11 to i1
	br label %13

	; <label>:13: ; preds = %10, %2
	%14 = phi i1 [ true, %2 ], [ %12, %10 ]
	%15 = zext i1 %14 to i8
	store i8 %15, i8* %5, align 1
	%16 = load i8, i8* %5, align 1
	%17 = trunc i8 %16 to i1
	ret i1 %17
}
