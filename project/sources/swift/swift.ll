; ModuleID = '-'
source_filename = "-"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%swift.refcounted = type { %swift.type*, i64 }
%swift.type = type { i64 }
%TSi = type <{ i64 }>
%swift.protocol = type { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i32, i32, i16, i16, i32 }
%Ts27_ContiguousArrayStorageBaseC = type <{ %swift.refcounted, %Ts10_ArrayBodyV }>
%Ts10_ArrayBodyV = type <{ %TSC22_SwiftArrayBodyStorageV }>
%TSC22_SwiftArrayBodyStorageV = type <{ %TSi, %TSu }>
%TSu = type <{ i64 }>
%Ts9_Buffer32V = type <{ %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V, %Ts5UInt8V }>
%Ts5UInt8V = type <{ i8 }>
%Ts11_StringCoreV = type <{ %TSvSg, %TSu, %TyXlSg }>
%TSvSg = type <{ [8 x i8] }>
%TyXlSg = type <{ [8 x i8] }>
%Ts27UnsafeBufferPointerIteratorVys6UInt16VG = type <{ %TSPys6UInt16VGSg, %TSPys6UInt16VGSg }>
%TSPys6UInt16VGSg = type <{ [8 x i8] }>
%Ts6UInt16V = type <{ i16 }>
%TSS9UTF16ViewV = type <{ %TSi, %TSi, %Ts11_StringCoreV }>
%Ts16IndexingIteratorVySS9UTF16ViewVG = type <{ %TSS9UTF16ViewV, %TSS5IndexV }>
%TSS5IndexV = type <{ %Ts6UInt64V, %TSS5IndexV6_CacheO }>
%Ts6UInt64V = type <{ i64 }>
%TSS5IndexV6_CacheO = type <{ [8 x i8], [1 x i8] }>
%TSS = type <{ %Ts11_StringCoreV }>

@_swift_retain = external local_unnamed_addr global %swift.refcounted* (%swift.refcounted*)*
@_swift_release = external local_unnamed_addr global void (%swift.refcounted*)*
@_T05swift3numSivp = hidden local_unnamed_addr global %TSi zeroinitializer, align 8
@_T0s27_ContiguousArrayStorageBaseC16countAndCapacitys01_B4BodyVvpWvd = external local_unnamed_addr global i64, align 8
@_T0s23_ContiguousArrayStorageCyypGML = linkonce_odr hidden local_unnamed_addr global %swift.type* null, align 8
@_T0ypML = linkonce_odr hidden local_unnamed_addr global %swift.type* null, align 8
@_swift_getExistentialTypeMetadata = external local_unnamed_addr global %swift.type* (i1, %swift.type*, i64, %swift.protocol**)*
@_swift_allocObject = external local_unnamed_addr global %swift.refcounted* (%swift.type*, i64, i64)*
@_T0SSN = external global %swift.type, align 8
@_T0s23_ContiguousArrayStorageCySSGML = linkonce_odr hidden local_unnamed_addr global %swift.type* null, align 8
@0 = private unnamed_addr constant [1 x i8] zeroinitializer
@_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGML = linkonce_odr hidden local_unnamed_addr global %swift.type* null, align 8
@_T0s18_StringBufferIVarsVN = external global %swift.type, align 8
@_T0s6UInt16VN = external global %swift.type, align 8
@_T0s7UnicodeO4UTF8ON = external global %swift.type, align 8
@_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGML = linkonce_odr hidden local_unnamed_addr global %swift.type* null, align 8
@__swift_reflection_version = linkonce_odr hidden constant i16 3
@_swift1_autolink_entries = private constant [12 x i8] c"-lswiftCore\00", section ".swift1_autolink_entries", align 8
@llvm.used = appending global [3 x i8*] [i8* bitcast (i64 (i64)* @_T05swift3fibS2i1n_tF to i8*), i8* bitcast (i16* @__swift_reflection_version to i8*), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @_swift1_autolink_entries, i32 0, i32 0)], section "llvm.metadata"
@_swift_retain_n = external global %swift.refcounted* (%swift.refcounted*, i32)*

; Function Attrs: noinline nounwind
define linkonce_odr hidden %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned) local_unnamed_addr #0 {
entry:
  %load = load %swift.refcounted* (%swift.refcounted*)*, %swift.refcounted* (%swift.refcounted*)** @_swift_retain, align 8
  %1 = tail call %swift.refcounted* %load(%swift.refcounted* returned %0) #8
  ret %swift.refcounted* %1
}

; Function Attrs: noinline nounwind
define linkonce_odr hidden void @swift_rt_swift_release(%swift.refcounted*) local_unnamed_addr #0 {
entry:
  %load = load void (%swift.refcounted*)*, void (%swift.refcounted*)** @_swift_release, align 8
  tail call void %load(%swift.refcounted* %0) #8
  ret void
}

define protected i32 @main(i32, i8** nocapture readnone) local_unnamed_addr #1 {
entry:
  %protocols.i.i = alloca [0 x %swift.protocol*], align 8
  %reference.raw10 = alloca [104 x i8], align 8
  %2 = tail call swiftcc i8* @_T0s11CommandLineO9argumentsSaySSGvau()
  %3 = bitcast i8* %2 to i8**
  %4 = load i8*, i8** %3, align 8
  %offset = load i64, i64* @_T0s27_ContiguousArrayStorageBaseC16countAndCapacitys01_B4BodyVvpWvd, align 8
  %5 = getelementptr inbounds i8, i8* %4, i64 %offset
  %.countAndCapacity._storage.count._value = bitcast i8* %5 to i64*
  %6 = load i64, i64* %.countAndCapacity._storage.count._value, align 8, !range !3
  %7 = icmp ult i64 %6, 2
  br i1 %7, label %93, label %8

; <label>:8:                                      ; preds = %entry
  %9 = getelementptr inbounds i8, i8* %4, i64 56
  %10 = bitcast i8* %9 to i64*
  %11 = load i64, i64* %10, align 8
  %._core._countAndFlags = getelementptr inbounds i8, i8* %4, i64 64
  %._core._countAndFlags._value = bitcast i8* %._core._countAndFlags to i64*
  %12 = load i64, i64* %._core._countAndFlags._value, align 8
  %._core._owner = getelementptr inbounds i8, i8* %4, i64 72
  %13 = bitcast i8* %._core._owner to i64*
  %14 = load i64, i64* %13, align 8
  %15 = inttoptr i64 %14 to %swift.refcounted*
  %16 = tail call %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned %15) #8
  %17 = tail call swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsExSgqd___Si5radixtcs14StringProtocolRd__lufCSi_SSTg5Tf4gXnd_n(i64 %11, i64 %12, i64 %14, i64 10)
  tail call void @swift_rt_swift_release(%swift.refcounted* %15) #8
  %18 = extractvalue { i64, i8 } %17, 0
  %19 = extractvalue { i64, i8 } %17, 1
  %20 = and i8 %19, 1
  %21 = icmp eq i8 %20, 0
  br i1 %21, label %22, label %94

; <label>:22:                                     ; preds = %8
  store i64 %18, i64* getelementptr inbounds (%TSi, %TSi* @_T05swift3numSivp, i64 0, i32 0), align 8
  %23 = load %swift.type*, %swift.type** @_T0s23_ContiguousArrayStorageCyypGML, align 8
  %24 = icmp eq %swift.type* %23, null
  br i1 %24, label %cacheIsNull.i, label %_T0s23_ContiguousArrayStorageCyypGMa.exit

cacheIsNull.i:                                    ; preds = %22
  %25 = load %swift.type*, %swift.type** @_T0ypML, align 8
  %26 = icmp eq %swift.type* %25, null
  br i1 %26, label %cacheIsNull.i.i, label %_T0ypMa.exit.i

cacheIsNull.i.i:                                  ; preds = %cacheIsNull.i
  %27 = bitcast [0 x %swift.protocol*]* %protocols.i.i to i8*
  call void @llvm.lifetime.start.p0i8(i64 0, i8* nonnull %27) #8
  %28 = getelementptr inbounds [0 x %swift.protocol*], [0 x %swift.protocol*]* %protocols.i.i, i64 0, i64 0
  %29 = call %swift.type* @swift_rt_swift_getExistentialTypeMetadata(i1 true, %swift.type* null, i64 0, %swift.protocol** nonnull %28) #8
  call void @llvm.lifetime.end.p0i8(i64 0, i8* nonnull %27) #8
  store atomic %swift.type* %29, %swift.type** @_T0ypML release, align 8
  br label %_T0ypMa.exit.i

_T0ypMa.exit.i:                                   ; preds = %cacheIsNull.i.i, %cacheIsNull.i
  %30 = phi %swift.type* [ %25, %cacheIsNull.i ], [ %29, %cacheIsNull.i.i ]
  %31 = call %swift.type* @_T0s23_ContiguousArrayStorageCMa(%swift.type* %30) #11
  store atomic %swift.type* %31, %swift.type** @_T0s23_ContiguousArrayStorageCyypGML release, align 8
  br label %_T0s23_ContiguousArrayStorageCyypGMa.exit

_T0s23_ContiguousArrayStorageCyypGMa.exit:        ; preds = %22, %_T0ypMa.exit.i
  %32 = phi %swift.type* [ %23, %22 ], [ %31, %_T0ypMa.exit.i ]
  %33 = bitcast %swift.type* %32 to i8*
  %34 = getelementptr inbounds %swift.type, %swift.type* %32, i64 6
  %35 = bitcast %swift.type* %34 to i32*
  %36 = load i32, i32* %35, align 8
  %37 = zext i32 %36 to i64
  %38 = getelementptr inbounds i8, i8* %33, i64 52
  %39 = bitcast i8* %38 to i16*
  %40 = load i16, i16* %39, align 4
  %41 = add nuw nsw i64 %37, 7
  %42 = and i64 %41, 8589934584
  %43 = add nuw nsw i64 %42, 32
  %44 = or i16 %40, 7
  %45 = zext i16 %44 to i64
  %46 = call noalias %swift.refcounted* @swift_rt_swift_allocObject(%swift.type* %32, i64 %43, i64 %45) #8
  %47 = bitcast %swift.refcounted* %46 to %Ts27_ContiguousArrayStorageBaseC*
  %offset1 = load i64, i64* @_T0s27_ContiguousArrayStorageBaseC16countAndCapacitys01_B4BodyVvpWvd, align 8
  %48 = bitcast %swift.refcounted* %46 to i8*
  %49 = getelementptr inbounds i8, i8* %48, i64 %offset1
  %50 = bitcast i8* %49 to <2 x i64>*
  store <2 x i64> <i64 1, i64 2>, <2 x i64>* %50, align 8
  %51 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %46, i64 2
  %52 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %46, i64 3, i32 1
  %53 = bitcast i64* %52 to %swift.type**
  store %swift.type* @_T0SSN, %swift.type** %53, align 8
  %54 = load %swift.type*, %swift.type** @_T0s23_ContiguousArrayStorageCySSGML, align 8
  %55 = icmp eq %swift.type* %54, null
  br i1 %55, label %cacheIsNull.i11, label %_T0s23_ContiguousArrayStorageCySSGMa.exit

cacheIsNull.i11:                                  ; preds = %_T0s23_ContiguousArrayStorageCyypGMa.exit
  %56 = call %swift.type* @_T0s23_ContiguousArrayStorageCMa(%swift.type* nonnull @_T0SSN) #11
  store atomic %swift.type* %56, %swift.type** @_T0s23_ContiguousArrayStorageCySSGML release, align 8
  br label %_T0s23_ContiguousArrayStorageCySSGMa.exit

_T0s23_ContiguousArrayStorageCySSGMa.exit:        ; preds = %_T0s23_ContiguousArrayStorageCyypGMa.exit, %cacheIsNull.i11
  %57 = phi %swift.type* [ %54, %_T0s23_ContiguousArrayStorageCyypGMa.exit ], [ %56, %cacheIsNull.i11 ]
  %58 = bitcast [104 x i8]* %reference.raw10 to %swift.refcounted*
  %reference.new = call %swift.refcounted* @swift_initStackObject(%swift.type* %57, %swift.refcounted* nonnull %58) #8
  %59 = bitcast %swift.refcounted* %reference.new to %Ts27_ContiguousArrayStorageBaseC*
  %offset4 = load i64, i64* @_T0s27_ContiguousArrayStorageBaseC16countAndCapacitys01_B4BodyVvpWvd, align 8
  %60 = bitcast %swift.refcounted* %reference.new to i8*
  %61 = getelementptr inbounds i8, i8* %60, i64 %offset4
  %62 = bitcast i8* %61 to <2 x i64>*
  store <2 x i64> <i64 3, i64 6>, <2 x i64>* %62, align 8
  %63 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 2
  %64 = call swiftcc { i64, i64, i64 } @_T0s27_toStringReadOnlyStreamableSSxs010TextOutputE0RzlFSS_Tg5(i64 ptrtoint ([1 x i8]* @0 to i64), i64 0, i64 0)
  %65 = extractvalue { i64, i64, i64 } %64, 0
  %66 = extractvalue { i64, i64, i64 } %64, 1
  %67 = extractvalue { i64, i64, i64 } %64, 2
  %68 = bitcast %swift.refcounted* %63 to i64*
  store i64 %65, i64* %68, align 8
  %tailaddr6._core._countAndFlags = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 2, i32 1
  store i64 %66, i64* %tailaddr6._core._countAndFlags, align 8
  %tailaddr6._core._owner = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 3
  %69 = bitcast %swift.refcounted* %tailaddr6._core._owner to i64*
  store i64 %67, i64* %69, align 8
  %70 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 3, i32 1
  %71 = call swiftcc i64 @_T05swift3fibS2i1n_tF(i64 %18)
  %72 = call swiftcc { i64, i64, i64 } @_T0s26_toStringReadOnlyPrintableSSxs06CustomB11ConvertibleRzlFSi_Tg5(i64 %71)
  %73 = extractvalue { i64, i64, i64 } %72, 0
  %74 = extractvalue { i64, i64, i64 } %72, 1
  %75 = extractvalue { i64, i64, i64 } %72, 2
  store i64 %73, i64* %70, align 8
  %._core7._countAndFlags = getelementptr inbounds i64, i64* %70, i64 1
  store i64 %74, i64* %._core7._countAndFlags, align 8
  %._core7._owner = getelementptr inbounds i64, i64* %70, i64 2
  store i64 %75, i64* %._core7._owner, align 8
  %76 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 5
  %77 = bitcast %swift.refcounted* %76 to i64*
  store i64 %65, i64* %77, align 8
  %._core8._countAndFlags = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 5, i32 1
  store i64 %66, i64* %._core8._countAndFlags, align 8
  %._core8._owner = getelementptr inbounds %swift.refcounted, %swift.refcounted* %reference.new, i64 6
  %78 = bitcast %swift.refcounted* %._core8._owner to i64*
  store i64 %67, i64* %78, align 8
  %79 = call swiftcc { i64, i64, i64 } @_T0S3S19stringInterpolationd_tcfCTf4nd_n(%Ts27_ContiguousArrayStorageBaseC* %59)
  %80 = extractvalue { i64, i64, i64 } %79, 0
  %81 = extractvalue { i64, i64, i64 } %79, 1
  %82 = extractvalue { i64, i64, i64 } %79, 2
  call void @llvm.lifetime.end.p0i8(i64 -1, i8* %60)
  %83 = bitcast %swift.refcounted* %51 to i64*
  store i64 %80, i64* %83, align 8
  %._core9._countAndFlags = getelementptr inbounds %swift.refcounted, %swift.refcounted* %46, i64 2, i32 1
  store i64 %81, i64* %._core9._countAndFlags, align 8
  %._core9._owner = getelementptr inbounds %swift.refcounted, %swift.refcounted* %46, i64 3
  %84 = bitcast %swift.refcounted* %._core9._owner to i64*
  store i64 %82, i64* %84, align 8
  %85 = call swiftcc { i64, i64, i64 } @_T0s5printyypd_SS9separatorSS10terminatortFfA0_()
  %86 = extractvalue { i64, i64, i64 } %85, 0
  %87 = extractvalue { i64, i64, i64 } %85, 1
  %88 = extractvalue { i64, i64, i64 } %85, 2
  %89 = call swiftcc { i64, i64, i64 } @_T0s5printyypd_SS9separatorSS10terminatortFfA1_()
  %90 = extractvalue { i64, i64, i64 } %89, 0
  %91 = extractvalue { i64, i64, i64 } %89, 1
  %92 = extractvalue { i64, i64, i64 } %89, 2
  call swiftcc void @_T0s5printyypd_SS9separatorSS10terminatortF(%Ts27_ContiguousArrayStorageBaseC* %47, i64 %86, i64 %87, i64 %88, i64 %90, i64 %91, i64 %92)
  ret i32 0

; <label>:93:                                     ; preds = %entry
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:94:                                     ; preds = %8
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable
}

declare swiftcc i8* @_T0s11CommandLineO9argumentsSaySSGvau() local_unnamed_addr #1

; Function Attrs: noreturn nounwind
declare void @llvm.trap() #2

; Function Attrs: nounwind
define protected swiftcc i64 @_T05swift3fibS2i1n_tF(i64) #3 {
entry:
  %1 = icmp sgt i64 %0, 1
  br i1 %1, label %2, label %13

; <label>:2:                                      ; preds = %entry
  %3 = add i64 %0, -1
  %4 = tail call swiftcc i64 @_T05swift3fibS2i1n_tF(i64 %3)
  %5 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %0, i64 2)
  %6 = extractvalue { i64, i1 } %5, 1
  br i1 %6, label %15, label %7

; <label>:7:                                      ; preds = %2
  %8 = extractvalue { i64, i1 } %5, 0
  %9 = tail call swiftcc i64 @_T05swift3fibS2i1n_tF(i64 %8)
  %10 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %4, i64 %9)
  %11 = extractvalue { i64, i1 } %10, 0
  %12 = extractvalue { i64, i1 } %10, 1
  br i1 %12, label %16, label %13

; <label>:13:                                     ; preds = %7, %entry
  %14 = phi i64 [ 1, %entry ], [ %11, %7 ]
  ret i64 %14

; <label>:15:                                     ; preds = %2
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:16:                                     ; preds = %7
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable
}

declare swiftcc i64 @_T0s13_StringBufferV9usedCountSivg(%swift.refcounted*) local_unnamed_addr #1

; Function Attrs: noinline norecurse
define linkonce_odr hidden swiftcc { i64, i64, i64 } @_T0s27_toStringReadOnlyStreamableSSxs010TextOutputE0RzlFSS_Tg5(i64, i64, i64) local_unnamed_addr #4 {
entry:
  %3 = insertvalue { i64, i64, i64 } undef, i64 %0, 0
  %4 = insertvalue { i64, i64, i64 } %3, i64 %1, 1
  %5 = insertvalue { i64, i64, i64 } %4, i64 %2, 2
  ret { i64, i64, i64 } %5
}

; Function Attrs: noinline
define linkonce_odr hidden swiftcc { i64, i64, i64 } @_T0s26_toStringReadOnlyPrintableSSxs06CustomB11ConvertibleRzlFSi_Tg5(i64) local_unnamed_addr #5 {
entry:
  %1 = alloca %Ts9_Buffer32V, align 1
  %2 = getelementptr inbounds %Ts9_Buffer32V, %Ts9_Buffer32V* %1, i64 0, i32 0, i32 0
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %2)
  call void @llvm.memset.p0i8.i64(i8* nonnull %2, i8 0, i64 32, i32 1, i1 false)
  %3 = call swiftcc i64 @swift_int64ToString(i8* nonnull %2, i64 32, i64 %0, i64 10, i1 false)
  %4 = icmp slt i64 %3, 0
  br i1 %4, label %22, label %5

; <label>:5:                                      ; preds = %entry
  %6 = ptrtoint %Ts9_Buffer32V* %1 to i64
  %7 = getelementptr inbounds %Ts9_Buffer32V, %Ts9_Buffer32V* %1, i64 0, i32 0
  %8 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %7, i64 %3, i32 0
  %9 = ptrtoint i8* %8 to i64
  %10 = call swiftcc { i64, i64, i64, i8 } @_T0SS21_fromCodeUnitSequenceSSSgxm_q_5inputts16_UnicodeEncodingRzs10CollectionR_7ElementQy_0bC0Rtzr0_lFZs0F0O4UTF8O_SRys5UInt8VGTgq5Tf4nxd_n(%swift.type* nonnull @_T0s7UnicodeO4UTF8ON, i64 %6, i64 %9)
  %11 = extractvalue { i64, i64, i64, i8 } %10, 3
  %12 = and i8 %11, 1
  %13 = icmp eq i8 %12, 0
  br i1 %13, label %14, label %21

; <label>:14:                                     ; preds = %5
  %15 = extractvalue { i64, i64, i64, i8 } %10, 2
  %16 = extractvalue { i64, i64, i64, i8 } %10, 1
  %17 = extractvalue { i64, i64, i64, i8 } %10, 0
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %2)
  %18 = insertvalue { i64, i64, i64 } undef, i64 %17, 0
  %19 = insertvalue { i64, i64, i64 } %18, i64 %16, 1
  %20 = insertvalue { i64, i64, i64 } %19, i64 %15, 2
  ret { i64, i64, i64 } %20

; <label>:21:                                     ; preds = %5
  call void asm sideeffect "", "n"(i32 1) #8
  call void @llvm.trap()
  unreachable

; <label>:22:                                     ; preds = %entry
  call void asm sideeffect "", "n"(i32 2) #8
  call void @llvm.trap()
  unreachable
}

declare swiftcc void @_T0s11_StringCoreV12_copyInPlaceySi7newSize_Si0F8CapacitySi15minElementWidthtF(i64, i64, i64, %Ts11_StringCoreV* nocapture swiftself dereferenceable(24)) local_unnamed_addr #1

; Function Attrs: noinline
define linkonce_odr hidden swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsE19_parseASCIISlowPathqd_0_Sgqd__z9codeUnits_qd_0_5radixts16IteratorProtocolRd__sAARd_0_s08UnsignedC07ElementRpd__r0_lFZSi_s019UnsafeBufferPointerJ0Vys6UInt16VGSiTg5Tf4nnd_n(%Ts27UnsafeBufferPointerIteratorVys6UInt16VG* nocapture dereferenceable(16), i64) local_unnamed_addr #5 {
entry:
  %temp-coercion.coerced.sroa.4.i52 = alloca i8, align 2
  %temp-coercion.coerced.sroa.4.i = alloca i8, align 2
  %2 = bitcast %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %0 to i64*
  %3 = load i64, i64* %2, align 8
  %._end = getelementptr inbounds %Ts27UnsafeBufferPointerIteratorVys6UInt16VG, %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %0, i64 0, i32 1
  %4 = bitcast %TSPys6UInt16VGSg* %._end to i64*
  %5 = load i64, i64* %4, align 8
  %6 = icmp eq i64 %3, 0
  %7 = inttoptr i64 %3 to %Ts6UInt16V*
  br i1 %6, label %18, label %8

; <label>:8:                                      ; preds = %entry
  %9 = inttoptr i64 %3 to i8*
  %10 = icmp ne i64 %5, 0
  %11 = inttoptr i64 %5 to i8*
  %12 = icmp eq i8* %9, %11
  %or.cond = and i1 %10, %12
  br i1 %or.cond, label %.loopexit, label %13

; <label>:13:                                     ; preds = %8
  %._value = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %7, i64 0, i32 0
  %14 = load i16, i16* %._value, align 2
  %15 = bitcast %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %0 to %Ts6UInt16V**
  %16 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %7, i64 1
  store %Ts6UInt16V* %16, %Ts6UInt16V** %15, align 8
  %17 = ptrtoint %Ts6UInt16V* %16 to i64
  switch i16 %14, label %154 [
    i16 43, label %20
    i16 45, label %20
  ], !prof !4

; <label>:18:                                     ; preds = %entry
  %19 = icmp eq i64 %5, 0
  br i1 %19, label %.loopexit, label %207

; <label>:20:                                     ; preds = %13, %13
  %21 = bitcast %Ts6UInt16V* %16 to i8*
  %22 = icmp eq i8* %21, %11
  %or.cond.i = and i1 %10, %22
  br i1 %or.cond.i, label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit, label %23

; <label>:23:                                     ; preds = %20
  %._value.i = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %16, i64 0, i32 0
  %24 = load i16, i16* %._value.i, align 2
  %25 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %7, i64 2
  store %Ts6UInt16V* %25, %Ts6UInt16V** %15, align 8
  %26 = ptrtoint %Ts6UInt16V* %25 to i64
  br label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit

_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit: ; preds = %20, %23
  %27 = phi %Ts6UInt16V* [ %25, %23 ], [ %16, %20 ]
  %28 = phi i64 [ %26, %23 ], [ %17, %20 ]
  %29 = phi i16 [ %24, %23 ], [ 0, %20 ]
  %30 = phi i1 [ false, %23 ], [ true, %20 ]
  call void @llvm.lifetime.start.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i)
  %temp-coercion.coerced.sroa.4.i.0..sroa_cast = bitcast i8* %temp-coercion.coerced.sroa.4.i to i1*
  store i1 %30, i1* %temp-coercion.coerced.sroa.4.i.0..sroa_cast, align 2
  %temp-coercion.coerced.sroa.4.i.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i = load i8, i8* %temp-coercion.coerced.sroa.4.i, align 2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i)
  %31 = and i8 %temp-coercion.coerced.sroa.4.i.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i, 1
  %32 = icmp eq i8 %31, 0
  br i1 %32, label %33, label %.loopexit

; <label>:33:                                     ; preds = %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit
  %34 = icmp eq i16 %14, 45
  %temp-coercion.coerced.sroa.0.0.extract.trunc.off = add i16 %29, -48
  %35 = icmp ugt i16 %temp-coercion.coerced.sroa.0.0.extract.trunc.off, 9
  br i1 %34, label %36, label %91, !prof !5

; <label>:36:                                     ; preds = %33
  br i1 %35, label %37, label %74, !prof !6

; <label>:37:                                     ; preds = %36
  %.off71 = add i16 %29, -65
  %38 = icmp ugt i16 %.off71, 25
  br i1 %38, label %39, label %74, !prof !6

; <label>:39:                                     ; preds = %37
  %.off72 = add i16 %29, -97
  %40 = icmp ugt i16 %.off72, 25
  br i1 %40, label %.loopexit, label %74, !prof !6

.loopexit:                                        ; preds = %119, %133, %127, %121, %130, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62, %59, %66, %55, %63, %69, %53, %176, %183, %172, %180, %186, %170, %.lr.ph89, %.lr.ph, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit, %158, %191, %199, %196, %94, %138, %146, %143, %39, %74, %83, %80, %18, %8
  %.sroa.0.0 = phi i64 [ 0, %8 ], [ 0, %18 ], [ 0, %80 ], [ 0, %83 ], [ 0, %74 ], [ 0, %39 ], [ 0, %143 ], [ 0, %146 ], [ 0, %138 ], [ 0, %94 ], [ 0, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit ], [ 0, %196 ], [ 0, %199 ], [ 0, %191 ], [ 0, %158 ], [ %203, %.lr.ph ], [ %77, %.lr.ph89 ], [ 0, %170 ], [ 0, %186 ], [ 0, %180 ], [ 0, %172 ], [ 0, %183 ], [ %177, %176 ], [ 0, %53 ], [ 0, %69 ], [ 0, %63 ], [ 0, %55 ], [ 0, %66 ], [ %60, %59 ], [ %98, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62 ], [ 0, %119 ], [ 0, %133 ], [ 0, %127 ], [ 0, %121 ], [ 0, %130 ]
  %.sink = phi i8 [ 1, %8 ], [ 1, %18 ], [ 1, %80 ], [ 1, %83 ], [ 1, %74 ], [ 1, %39 ], [ 1, %143 ], [ 1, %146 ], [ 1, %138 ], [ 1, %94 ], [ 1, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit ], [ 1, %196 ], [ 1, %199 ], [ 1, %191 ], [ 1, %158 ], [ 0, %.lr.ph ], [ 0, %.lr.ph89 ], [ 1, %170 ], [ 1, %186 ], [ 1, %180 ], [ 1, %172 ], [ 1, %183 ], [ 0, %176 ], [ 1, %53 ], [ 1, %69 ], [ 1, %63 ], [ 1, %55 ], [ 1, %66 ], [ 0, %59 ], [ 0, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62 ], [ 1, %119 ], [ 1, %133 ], [ 1, %127 ], [ 1, %121 ], [ 1, %130 ]
  %41 = insertvalue { i64, i8 } undef, i64 %.sroa.0.0, 0
  %42 = insertvalue { i64, i8 } %41, i8 %.sink, 1
  ret { i64, i8 } %42

.lr.ph131:                                        ; preds = %.lr.ph131.preheader, %59
  %43 = phi i64 [ %60, %59 ], [ %77, %.lr.ph131.preheader ]
  %44 = phi i64 [ %50, %59 ], [ %28, %.lr.ph131.preheader ]
  %45 = phi %Ts6UInt16V* [ %48, %59 ], [ %27, %.lr.ph131.preheader ]
  %46 = inttoptr i64 %44 to %Ts6UInt16V*
  %._value9 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %46, i64 0, i32 0
  %47 = load i16, i16* %._value9, align 2
  %48 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %45, i64 1
  store %Ts6UInt16V* %48, %Ts6UInt16V** %15, align 8
  %.off68 = add i16 %47, -48
  %49 = icmp ugt i16 %.off68, 9
  %50 = ptrtoint %Ts6UInt16V* %48 to i64
  br i1 %49, label %51, label %63, !prof !6

; <label>:51:                                     ; preds = %.lr.ph131
  %.off69 = add i16 %47, -65
  %52 = icmp ugt i16 %.off69, 25
  br i1 %52, label %53, label %63, !prof !6

; <label>:53:                                     ; preds = %51
  %.off70 = add i16 %47, -97
  %54 = icmp ugt i16 %.off70, 25
  br i1 %54, label %.loopexit, label %63, !prof !6

; <label>:55:                                     ; preds = %66
  %56 = extractvalue { i64, i1 } %67, 0
  %57 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %56, i64 %70)
  %58 = extractvalue { i64, i1 } %57, 1
  br i1 %58, label %.loopexit, label %59, !prof !7

; <label>:59:                                     ; preds = %55
  %60 = extractvalue { i64, i1 } %57, 0
  %61 = bitcast %Ts6UInt16V* %48 to i8*
  %62 = icmp eq i8* %61, %11
  %or.cond16 = and i1 %10, %62
  br i1 %or.cond16, label %.loopexit, label %.lr.ph131

; <label>:63:                                     ; preds = %53, %.lr.ph131, %51
  %.sink29 = phi i16 [ -55, %51 ], [ -48, %.lr.ph131 ], [ -87, %53 ]
  %64 = add i16 %.sink29, %47
  %65 = icmp eq i16 %64, 0
  %or.cond17 = and i1 %82, %65
  br i1 %or.cond17, label %.loopexit, label %69

; <label>:66:                                     ; preds = %69
  %67 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %43, i64 %1)
  %68 = extractvalue { i64, i1 } %67, 1
  br i1 %68, label %.loopexit, label %55

; <label>:69:                                     ; preds = %63
  %70 = zext i16 %64 to i64
  %71 = icmp slt i64 %70, %1
  %72 = icmp ult i16 %64, %84
  %73 = select i1 %86, i1 %72, i1 %71
  br i1 %73, label %66, label %.loopexit, !prof !5

; <label>:74:                                     ; preds = %39, %36, %37
  %.sink33 = phi i16 [ -55, %37 ], [ -48, %36 ], [ -87, %39 ]
  %75 = add i16 %.sink33, %29
  %76 = icmp slt i64 %1, 0
  br i1 %76, label %.loopexit, label %80

.lr.ph89:                                         ; preds = %83
  %77 = sub nsw i64 0, %87
  %78 = inttoptr i64 %28 to i8*
  %79 = icmp eq i8* %78, %11
  %or.cond16130 = and i1 %10, %79
  br i1 %or.cond16130, label %.loopexit, label %.lr.ph131.preheader

.lr.ph131.preheader:                              ; preds = %.lr.ph89
  br label %.lr.ph131

; <label>:80:                                     ; preds = %74
  %81 = icmp eq i16 %75, 0
  %82 = icmp eq i64 %1, 0
  %or.cond18 = and i1 %82, %81
  br i1 %or.cond18, label %.loopexit, label %83

; <label>:83:                                     ; preds = %80
  %84 = trunc i64 %1 to i16
  %85 = and i64 %1, 65535
  %86 = icmp eq i64 %85, %1
  %87 = zext i16 %75 to i64
  %88 = icmp slt i64 %87, %1
  %89 = icmp ult i16 %75, %84
  %90 = select i1 %86, i1 %89, i1 %88
  br i1 %90, label %.lr.ph89, label %.loopexit, !prof !5

; <label>:91:                                     ; preds = %33
  br i1 %35, label %92, label %138, !prof !6

; <label>:92:                                     ; preds = %91
  %.off66 = add i16 %29, -65
  %93 = icmp ugt i16 %.off66, 25
  br i1 %93, label %94, label %138, !prof !6

; <label>:94:                                     ; preds = %92
  %.off67 = add i16 %29, -97
  %95 = icmp ugt i16 %.off67, 25
  br i1 %95, label %.loopexit, label %138, !prof !6

; <label>:96:                                     ; preds = %125, %141
  %97 = phi i64 [ %28, %141 ], [ %110, %125 ]
  %98 = phi i64 [ %150, %141 ], [ %126, %125 ]
  %99 = icmp eq i64 %97, 0
  %100 = inttoptr i64 %97 to %Ts6UInt16V*
  br i1 %99, label %104, label %101

; <label>:101:                                    ; preds = %96
  %102 = inttoptr i64 %97 to i8*
  %103 = icmp eq i8* %102, %11
  %or.cond.i54 = and i1 %10, %103
  br i1 %or.cond.i54, label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62, label %105

; <label>:104:                                    ; preds = %96
  br i1 %142, label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62, label %109

; <label>:105:                                    ; preds = %101
  %._value.i60 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %100, i64 0, i32 0
  %106 = load i16, i16* %._value.i60, align 2
  %107 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %100, i64 1
  store %Ts6UInt16V* %107, %Ts6UInt16V** %15, align 8
  %108 = ptrtoint %Ts6UInt16V* %107 to i64
  br label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62

; <label>:109:                                    ; preds = %104
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62: ; preds = %101, %104, %105
  %110 = phi i64 [ %108, %105 ], [ 0, %104 ], [ %97, %101 ]
  %111 = phi i16 [ %106, %105 ], [ 0, %104 ], [ 0, %101 ]
  %112 = phi i1 [ false, %105 ], [ true, %104 ], [ true, %101 ]
  call void @llvm.lifetime.start.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i52)
  store i1 %112, i1* %temp-coercion.coerced.sroa.4.i52.0..sroa_cast, align 2
  %temp-coercion.coerced.sroa.4.i52.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i56 = load i8, i8* %temp-coercion.coerced.sroa.4.i52, align 2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i52)
  %113 = and i8 %temp-coercion.coerced.sroa.4.i52.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i56, 1
  %114 = icmp eq i8 %113, 0
  br i1 %114, label %115, label %.loopexit

; <label>:115:                                    ; preds = %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs6UInt16V_Tg5.exit62
  %temp-coercion.coerced6.sroa.0.0.extract.trunc.off = add i16 %111, -48
  %116 = icmp ugt i16 %temp-coercion.coerced6.sroa.0.0.extract.trunc.off, 9
  br i1 %116, label %117, label %127, !prof !6

; <label>:117:                                    ; preds = %115
  %.off = add i16 %111, -65
  %118 = icmp ugt i16 %.off, 25
  br i1 %118, label %119, label %127, !prof !6

; <label>:119:                                    ; preds = %117
  %.off65 = add i16 %111, -97
  %120 = icmp ugt i16 %.off65, 25
  br i1 %120, label %.loopexit, label %127, !prof !6

; <label>:121:                                    ; preds = %130
  %122 = extractvalue { i64, i1 } %131, 0
  %123 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %122, i64 %134)
  %124 = extractvalue { i64, i1 } %123, 1
  br i1 %124, label %.loopexit, label %125, !prof !7

; <label>:125:                                    ; preds = %121
  %126 = extractvalue { i64, i1 } %123, 0
  br label %96

; <label>:127:                                    ; preds = %119, %115, %117
  %.sink37 = phi i16 [ -55, %117 ], [ -48, %115 ], [ -87, %119 ]
  %128 = add i16 %.sink37, %111
  %129 = icmp eq i16 %128, 0
  %or.cond19 = and i1 %145, %129
  br i1 %or.cond19, label %.loopexit, label %133

; <label>:130:                                    ; preds = %133
  %131 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %98, i64 %1)
  %132 = extractvalue { i64, i1 } %131, 1
  br i1 %132, label %.loopexit, label %121

; <label>:133:                                    ; preds = %127
  %134 = zext i16 %128 to i64
  %135 = icmp slt i64 %134, %1
  %136 = icmp ult i16 %128, %147
  %137 = select i1 %149, i1 %136, i1 %135
  br i1 %137, label %130, label %.loopexit, !prof !5

; <label>:138:                                    ; preds = %94, %91, %92
  %.sink41 = phi i16 [ -55, %92 ], [ -48, %91 ], [ -87, %94 ]
  %139 = add i16 %.sink41, %29
  %140 = icmp slt i64 %1, 0
  br i1 %140, label %.loopexit, label %143

; <label>:141:                                    ; preds = %146
  %142 = icmp eq i64 %5, 0
  %temp-coercion.coerced.sroa.4.i52.0..sroa_cast = bitcast i8* %temp-coercion.coerced.sroa.4.i52 to i1*
  br label %96

; <label>:143:                                    ; preds = %138
  %144 = icmp eq i16 %139, 0
  %145 = icmp eq i64 %1, 0
  %or.cond20 = and i1 %145, %144
  br i1 %or.cond20, label %.loopexit, label %146

; <label>:146:                                    ; preds = %143
  %147 = trunc i64 %1 to i16
  %148 = and i64 %1, 65535
  %149 = icmp eq i64 %148, %1
  %150 = zext i16 %139 to i64
  %151 = icmp slt i64 %150, %1
  %152 = icmp ult i16 %139, %147
  %153 = select i1 %149, i1 %152, i1 %151
  br i1 %153, label %141, label %.loopexit, !prof !5

; <label>:154:                                    ; preds = %13
  %.off73 = add i16 %14, -48
  %155 = icmp ugt i16 %.off73, 9
  br i1 %155, label %156, label %191, !prof !6

; <label>:156:                                    ; preds = %154
  %.off77 = add i16 %14, -65
  %157 = icmp ugt i16 %.off77, 25
  br i1 %157, label %158, label %191, !prof !6

; <label>:158:                                    ; preds = %156
  %.off78 = add i16 %14, -97
  %159 = icmp ugt i16 %.off78, 25
  br i1 %159, label %.loopexit, label %191, !prof !6

.lr.ph113:                                        ; preds = %.lr.ph113.preheader, %176
  %160 = phi i64 [ %177, %176 ], [ %203, %.lr.ph113.preheader ]
  %161 = phi i64 [ %167, %176 ], [ %17, %.lr.ph113.preheader ]
  %162 = phi %Ts6UInt16V* [ %165, %176 ], [ %16, %.lr.ph113.preheader ]
  %163 = inttoptr i64 %161 to %Ts6UInt16V*
  %._value3 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %163, i64 0, i32 0
  %164 = load i16, i16* %._value3, align 2
  %165 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %162, i64 1
  store %Ts6UInt16V* %165, %Ts6UInt16V** %15, align 8
  %.off74 = add i16 %164, -48
  %166 = icmp ugt i16 %.off74, 9
  %167 = ptrtoint %Ts6UInt16V* %165 to i64
  br i1 %166, label %168, label %180, !prof !6

; <label>:168:                                    ; preds = %.lr.ph113
  %.off75 = add i16 %164, -65
  %169 = icmp ugt i16 %.off75, 25
  br i1 %169, label %170, label %180, !prof !6

; <label>:170:                                    ; preds = %168
  %.off76 = add i16 %164, -97
  %171 = icmp ugt i16 %.off76, 25
  br i1 %171, label %.loopexit, label %180, !prof !6

; <label>:172:                                    ; preds = %183
  %173 = extractvalue { i64, i1 } %184, 0
  %174 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %173, i64 %187)
  %175 = extractvalue { i64, i1 } %174, 1
  br i1 %175, label %.loopexit, label %176, !prof !7

; <label>:176:                                    ; preds = %172
  %177 = extractvalue { i64, i1 } %174, 0
  %178 = bitcast %Ts6UInt16V* %165 to i8*
  %179 = icmp eq i8* %178, %11
  %or.cond22 = and i1 %10, %179
  br i1 %or.cond22, label %.loopexit, label %.lr.ph113

; <label>:180:                                    ; preds = %170, %.lr.ph113, %168
  %.sink45 = phi i16 [ -55, %168 ], [ -48, %.lr.ph113 ], [ -87, %170 ]
  %181 = add i16 %.sink45, %164
  %182 = icmp eq i16 %181, 0
  %or.cond23 = and i1 %198, %182
  br i1 %or.cond23, label %.loopexit, label %186

; <label>:183:                                    ; preds = %186
  %184 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %160, i64 %1)
  %185 = extractvalue { i64, i1 } %184, 1
  br i1 %185, label %.loopexit, label %172

; <label>:186:                                    ; preds = %180
  %187 = zext i16 %181 to i64
  %188 = icmp slt i64 %187, %1
  %189 = icmp ult i16 %181, %200
  %190 = select i1 %202, i1 %189, i1 %188
  br i1 %190, label %183, label %.loopexit, !prof !5

; <label>:191:                                    ; preds = %158, %154, %156
  %.sink49 = phi i16 [ -55, %156 ], [ -48, %154 ], [ -87, %158 ]
  %192 = add i16 %.sink49, %14
  %193 = icmp slt i64 %1, 0
  br i1 %193, label %.loopexit, label %196

.lr.ph:                                           ; preds = %199
  %194 = bitcast %Ts6UInt16V* %16 to i8*
  %195 = icmp eq i8* %194, %11
  %or.cond22112 = and i1 %10, %195
  br i1 %or.cond22112, label %.loopexit, label %.lr.ph113.preheader

.lr.ph113.preheader:                              ; preds = %.lr.ph
  br label %.lr.ph113

; <label>:196:                                    ; preds = %191
  %197 = icmp eq i16 %192, 0
  %198 = icmp eq i64 %1, 0
  %or.cond24 = and i1 %198, %197
  br i1 %or.cond24, label %.loopexit, label %199

; <label>:199:                                    ; preds = %196
  %200 = trunc i64 %1 to i16
  %201 = and i64 %1, 65535
  %202 = icmp eq i64 %201, %1
  %203 = zext i16 %192 to i64
  %204 = icmp slt i64 %203, %1
  %205 = icmp ult i16 %192, %200
  %206 = select i1 %202, i1 %205, i1 %204
  br i1 %206, label %.lr.ph, label %.loopexit, !prof !5

; <label>:207:                                    ; preds = %18
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable
}

define linkonce_odr hidden swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64, %TSS9UTF16ViewV* noalias nocapture swiftself dereferenceable(40)) local_unnamed_addr #1 {
entry:
  %.sroa.012.0..sroa_idx = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %1, i64 0, i32 2
  %.sroa.012.0..sroa_cast13 = bitcast %Ts11_StringCoreV* %.sroa.012.0..sroa_idx to i64*
  %.sroa.012.0.copyload = load i64, i64* %.sroa.012.0..sroa_cast13, align 8
  %.sroa.4.0..sroa_idx15 = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %1, i64 0, i32 2, i32 1, i32 0
  %.sroa.4.0.copyload = load i64, i64* %.sroa.4.0..sroa_idx15, align 8
  %.sroa.010.0..sroa_idx = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %1, i64 0, i32 0, i32 0
  %.sroa.010.0.copyload = load i64, i64* %.sroa.010.0..sroa_idx, align 8
  %2 = shl i64 %.sroa.010.0.copyload, 2
  %.sroa.0.0..sroa_idx = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %1, i64 0, i32 1, i32 0
  %.sroa.0.0.copyload = load i64, i64* %.sroa.0.0..sroa_idx, align 8
  %3 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %.sroa.010.0.copyload, i64 %.sroa.0.0.copyload)
  %4 = extractvalue { i64, i1 } %3, 1
  br i1 %4, label %81, label %5

; <label>:5:                                      ; preds = %entry
  %6 = extractvalue { i64, i1 } %3, 0
  %7 = icmp ugt i64 %2, %0
  %8 = shl i64 %6, 2
  %9 = icmp ule i64 %8, %0
  %10 = lshr i64 %0, 2
  %11 = or i64 %8, %2
  %12 = icmp slt i64 %11, 0
  %13 = or i1 %7, %12
  %14 = or i1 %9, %13
  br i1 %14, label %82, label %15

; <label>:15:                                     ; preds = %5
  %16 = and i64 %.sroa.4.0.copyload, 4611686018427387903
  %17 = icmp ult i64 %16, %10
  br i1 %17, label %83, label %18

; <label>:18:                                     ; preds = %15
  %19 = icmp eq i64 %.sroa.012.0.copyload, 0
  br i1 %19, label %84, label %20

; <label>:20:                                     ; preds = %18
  %21 = inttoptr i64 %.sroa.012.0.copyload to i8*
  %22 = lshr i64 %.sroa.4.0.copyload, 63
  %23 = shl i64 %10, %22
  %24 = getelementptr inbounds i8, i8* %21, i64 %23
  %25 = load i8, i8* %24, align 1
  %26 = getelementptr inbounds i8, i8* %24, i64 1
  %27 = load i8, i8* %26, align 1
  %28 = zext i8 %27 to i16
  %29 = shl nuw nsw i64 %22, 8
  %30 = trunc i64 %29 to i16
  %31 = mul nuw i16 %28, %30
  %32 = zext i8 %25 to i16
  %33 = tail call { i16, i1 } @llvm.uadd.with.overflow.i16(i16 %32, i16 %31)
  %34 = extractvalue { i16, i1 } %33, 0
  %35 = extractvalue { i16, i1 } %33, 1
  br i1 %35, label %85, label %36

; <label>:36:                                     ; preds = %20
  %.mask = and i16 %34, -2048
  %37 = icmp eq i16 %.mask, -10240
  br i1 %37, label %38, label %79, !prof !7

; <label>:38:                                     ; preds = %36
  %.mask22 = and i16 %34, -1024
  %39 = icmp eq i16 %.mask22, -10240
  br i1 %39, label %60, label %40

; <label>:40:                                     ; preds = %38
  %41 = icmp eq i64 %10, 0
  br i1 %41, label %59, label %42

; <label>:42:                                     ; preds = %40
  %43 = add nsw i64 %10, -1
  %44 = icmp slt i64 %16, %43
  br i1 %44, label %86, label %45

; <label>:45:                                     ; preds = %42
  %46 = shl i64 %43, %22
  %47 = getelementptr inbounds i8, i8* %21, i64 %46
  %48 = load i8, i8* %47, align 1
  %49 = getelementptr inbounds i8, i8* %47, i64 1
  %50 = load i8, i8* %49, align 1
  %51 = zext i8 %50 to i16
  %52 = mul nuw i16 %51, %30
  %53 = zext i8 %48 to i16
  %54 = tail call { i16, i1 } @llvm.uadd.with.overflow.i16(i16 %53, i16 %52)
  %55 = extractvalue { i16, i1 } %54, 1
  br i1 %55, label %56, label %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit

; <label>:56:                                     ; preds = %45
  tail call void asm sideeffect "", "n"(i32 4) #8
  tail call void @llvm.trap()
  unreachable

_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit: ; preds = %45
  %57 = extractvalue { i16, i1 } %54, 0
  %.mask23 = and i16 %57, -1024
  %58 = icmp eq i16 %.mask23, -10240
  br i1 %58, label %79, label %59, !prof !5

; <label>:59:                                     ; preds = %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit, %40
  br label %79

; <label>:60:                                     ; preds = %38
  %61 = add nuw nsw i64 %10, 1
  %62 = icmp ult i64 %61, %16
  br i1 %62, label %63, label %79

; <label>:63:                                     ; preds = %60
  %64 = icmp ugt i64 %16, %10
  br i1 %64, label %65, label %87

; <label>:65:                                     ; preds = %63
  %66 = shl i64 %61, %22
  %67 = getelementptr inbounds i8, i8* %21, i64 %66
  %68 = load i8, i8* %67, align 1
  %69 = getelementptr inbounds i8, i8* %67, i64 1
  %70 = load i8, i8* %69, align 1
  %71 = zext i8 %70 to i16
  %72 = mul nuw i16 %71, %30
  %73 = zext i8 %68 to i16
  %74 = tail call { i16, i1 } @llvm.uadd.with.overflow.i16(i16 %73, i16 %72)
  %75 = extractvalue { i16, i1 } %74, 1
  br i1 %75, label %76, label %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit25

; <label>:76:                                     ; preds = %65
  tail call void asm sideeffect "", "n"(i32 4) #8
  tail call void @llvm.trap()
  unreachable

_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit25: ; preds = %65
  %77 = extractvalue { i16, i1 } %74, 0
  %.mask24 = and i16 %77, -1024
  %78 = icmp eq i16 %.mask24, -9216
  %. = select i1 %78, i16 %34, i16 -3, !prof !5
  br label %79

; <label>:79:                                     ; preds = %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit25, %60, %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit, %36, %59
  %80 = phi i16 [ -3, %59 ], [ %34, %36 ], [ %34, %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit ], [ -3, %60 ], [ %., %_T0s11_StringCoreV14_nthContiguouss6UInt16VSiFTf4nx_n.exit25 ]
  ret i16 %80

; <label>:81:                                     ; preds = %entry
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:82:                                     ; preds = %5
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable

; <label>:83:                                     ; preds = %15
  tail call void asm sideeffect "", "n"(i32 2) #8
  tail call void @llvm.trap()
  unreachable

; <label>:84:                                     ; preds = %18
  tail call void asm sideeffect "", "n"(i32 3) #8
  tail call void @llvm.trap()
  unreachable

; <label>:85:                                     ; preds = %20
  tail call void asm sideeffect "", "n"(i32 7) #8
  tail call void @llvm.trap()
  unreachable

; <label>:86:                                     ; preds = %42
  tail call void asm sideeffect "", "n"(i32 10) #8
  tail call void @llvm.trap()
  unreachable

; <label>:87:                                     ; preds = %63
  tail call void asm sideeffect "", "n"(i32 14) #8
  tail call void @llvm.trap()
  unreachable
}

; Function Attrs: noinline
define linkonce_odr hidden swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsE19_parseASCIISlowPathqd_0_Sgqd__z9codeUnits_qd_0_5radixts16IteratorProtocolRd__sAARd_0_s08UnsignedC07ElementRpd__r0_lFZSi_s08IndexingJ0VySS9UTF16ViewVGSiTg5Tf4nnd_n(%Ts16IndexingIteratorVySS9UTF16ViewVG* nocapture dereferenceable(57), i64) local_unnamed_addr #5 {
entry:
  %2 = alloca %TSS9UTF16ViewV, align 8
  %3 = bitcast %TSS9UTF16ViewV* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* nonnull %3)
  %._position._compoundOffset._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 1, i32 0, i32 0
  %4 = load i64, i64* %._position._compoundOffset._value, align 8
  %._elements._offset._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 0, i32 0, i32 0
  %5 = load i64, i64* %._elements._offset._value, align 8
  %._elements._length._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 0, i32 1, i32 0
  %6 = load i64, i64* %._elements._length._value, align 8
  %7 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %5, i64 %6)
  %8 = extractvalue { i64, i1 } %7, 1
  br i1 %8, label %208, label %9

; <label>:9:                                      ; preds = %entry
  %10 = extractvalue { i64, i1 } %7, 0
  %11 = shl i64 %10, 2
  %12 = icmp slt i64 %11, 0
  br i1 %12, label %209, label %13

; <label>:13:                                     ; preds = %9
  %14 = icmp eq i64 %4, %11
  br i1 %14, label %.loopexit, label %15

; <label>:15:                                     ; preds = %13
  %._elements._core = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 0, i32 2
  %._elements._core._owner = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 0, i32 2, i32 2
  %16 = bitcast %Ts11_StringCoreV* %._elements._core to <2 x i64>*
  %17 = load <2 x i64>, <2 x i64>* %16, align 8
  %18 = bitcast %TyXlSg* %._elements._core._owner to i64*
  %19 = load i64, i64* %18, align 8
  %._offset._value = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %2, i64 0, i32 0, i32 0
  store i64 %5, i64* %._offset._value, align 8
  %._length._value = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %2, i64 0, i32 1, i32 0
  store i64 %6, i64* %._length._value, align 8
  %._core = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %2, i64 0, i32 2
  %20 = bitcast %Ts11_StringCoreV* %._core to <2 x i64>*
  store <2 x i64> %17, <2 x i64>* %20, align 8
  %._core._owner = getelementptr inbounds %TSS9UTF16ViewV, %TSS9UTF16ViewV* %2, i64 0, i32 2, i32 2
  %21 = bitcast %TyXlSg* %._core._owner to i64*
  store i64 %19, i64* %21, align 8
  %22 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %4, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %23 = add i64 %4, 4
  %24 = and i64 %23, -4
  %25 = icmp slt i64 %24, 0
  br i1 %25, label %210, label %26

; <label>:26:                                     ; preds = %15
  store i64 %24, i64* %._position._compoundOffset._value, align 8
  %._position._cache = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 1, i32 1
  %27 = bitcast %TSS5IndexV6_CacheO* %._position._cache to i64*
  store i64 0, i64* %27, align 8
  %28 = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %0, i64 0, i32 1, i32 1, i32 1, i64 0
  store i8 3, i8* %28, align 8
  switch i16 %22, label %152 [
    i16 43, label %29
    i16 45, label %29
  ], !prof !4

; <label>:29:                                     ; preds = %26, %26
  %30 = icmp eq i64 %24, %11
  br i1 %30, label %.loopexit, label %31

; <label>:31:                                     ; preds = %29
  %32 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %24, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %33 = add i64 %4, 8
  %34 = and i64 %33, -4
  %35 = icmp slt i64 %34, 0
  br i1 %35, label %211, label %36

; <label>:36:                                     ; preds = %31
  store i64 %34, i64* %._position._compoundOffset._value, align 8
  store i64 0, i64* %27, align 8
  store i8 3, i8* %28, align 8
  %37 = icmp eq i16 %22, 45
  %.off = add i16 %32, -48
  %38 = icmp ugt i16 %.off, 9
  br i1 %37, label %39, label %97, !prof !5

; <label>:39:                                     ; preds = %36
  br i1 %38, label %40, label %80, !prof !6

; <label>:40:                                     ; preds = %39
  %.off54 = add i16 %32, -65
  %41 = icmp ugt i16 %.off54, 25
  br i1 %41, label %42, label %80, !prof !6

; <label>:42:                                     ; preds = %40
  %.off55 = add i16 %32, -97
  %43 = icmp ugt i16 %.off55, 25
  br i1 %43, label %.loopexit, label %80, !prof !6

.loopexit:                                        ; preds = %112, %127, %121, %114, %124, %119, %60, %75, %69, %62, %72, %67, %168, %183, %177, %170, %180, %175, %156, %188, %200, %197, %191, %100, %132, %144, %141, %135, %42, %80, %89, %86, %83, %29, %13
  %.sroa.0.0 = phi i64 [ 0, %13 ], [ 0, %29 ], [ %84, %83 ], [ 0, %86 ], [ 0, %89 ], [ 0, %80 ], [ 0, %42 ], [ %148, %135 ], [ 0, %141 ], [ 0, %144 ], [ 0, %132 ], [ 0, %100 ], [ %204, %191 ], [ 0, %197 ], [ 0, %200 ], [ 0, %188 ], [ 0, %156 ], [ 0, %168 ], [ 0, %183 ], [ 0, %177 ], [ 0, %170 ], [ 0, %180 ], [ %173, %175 ], [ 0, %60 ], [ 0, %75 ], [ 0, %69 ], [ 0, %62 ], [ 0, %72 ], [ %65, %67 ], [ 0, %112 ], [ 0, %127 ], [ 0, %121 ], [ 0, %114 ], [ 0, %124 ], [ %117, %119 ]
  %.sink = phi i8 [ 1, %13 ], [ 1, %29 ], [ 0, %83 ], [ 1, %86 ], [ 1, %89 ], [ 1, %80 ], [ 1, %42 ], [ 0, %135 ], [ 1, %141 ], [ 1, %144 ], [ 1, %132 ], [ 1, %100 ], [ 0, %191 ], [ 1, %197 ], [ 1, %200 ], [ 1, %188 ], [ 1, %156 ], [ 1, %168 ], [ 1, %183 ], [ 1, %177 ], [ 1, %170 ], [ 1, %180 ], [ 0, %175 ], [ 1, %60 ], [ 1, %75 ], [ 1, %69 ], [ 1, %62 ], [ 1, %72 ], [ 0, %67 ], [ 1, %112 ], [ 1, %127 ], [ 1, %121 ], [ 1, %114 ], [ 1, %124 ], [ 0, %119 ]
  call void @llvm.lifetime.end.p0i8(i64 40, i8* nonnull %3)
  %44 = insertvalue { i64, i8 } undef, i64 %.sroa.0.0, 0
  %45 = insertvalue { i64, i8 } %44, i8 %.sink, 1
  ret { i64, i8 } %45

; <label>:46:                                     ; preds = %83
  %47 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %34, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %48 = add i64 %34, 4
  %49 = icmp slt i64 %48, 0
  br i1 %49, label %._crit_edge70, label %.lr.ph69.preheader

.lr.ph69.preheader:                               ; preds = %46
  br label %.lr.ph69

; <label>:50:                                     ; preds = %67
  %51 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %54, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %52 = add i64 %54, 4
  %53 = icmp slt i64 %52, 0
  br i1 %53, label %._crit_edge70, label %.lr.ph69

.lr.ph69:                                         ; preds = %.lr.ph69.preheader, %50
  %54 = phi i64 [ %52, %50 ], [ %48, %.lr.ph69.preheader ]
  %55 = phi i16 [ %51, %50 ], [ %47, %.lr.ph69.preheader ]
  %56 = phi i64 [ %65, %50 ], [ %84, %.lr.ph69.preheader ]
  store i64 %54, i64* %._position._compoundOffset._value, align 8
  store i64 0, i64* %27, align 8
  store i8 3, i8* %28, align 8
  %.off51 = add i16 %55, -48
  %57 = icmp ugt i16 %.off51, 9
  br i1 %57, label %58, label %69, !prof !6

; <label>:58:                                     ; preds = %.lr.ph69
  %.off52 = add i16 %55, -65
  %59 = icmp ugt i16 %.off52, 25
  br i1 %59, label %60, label %69, !prof !6

; <label>:60:                                     ; preds = %58
  %.off53 = add i16 %55, -97
  %61 = icmp ugt i16 %.off53, 25
  br i1 %61, label %.loopexit, label %69, !prof !6

; <label>:62:                                     ; preds = %72
  %63 = extractvalue { i64, i1 } %73, 0
  %64 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %63, i64 %76)
  %65 = extractvalue { i64, i1 } %64, 0
  %66 = extractvalue { i64, i1 } %64, 1
  br i1 %66, label %.loopexit, label %67, !prof !7

; <label>:67:                                     ; preds = %62
  %68 = icmp eq i64 %54, %11
  br i1 %68, label %.loopexit, label %50

; <label>:69:                                     ; preds = %60, %.lr.ph69, %58
  %.sink23 = phi i16 [ -55, %58 ], [ -48, %.lr.ph69 ], [ -87, %60 ]
  %70 = add i16 %.sink23, %55
  %71 = icmp eq i16 %70, 0
  %or.cond = and i1 %88, %71
  br i1 %or.cond, label %.loopexit, label %75

; <label>:72:                                     ; preds = %75
  %73 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %56, i64 %1)
  %74 = extractvalue { i64, i1 } %73, 1
  br i1 %74, label %.loopexit, label %62

; <label>:75:                                     ; preds = %69
  %76 = zext i16 %70 to i64
  %77 = icmp slt i64 %76, %1
  %78 = icmp ult i16 %70, %90
  %79 = select i1 %92, i1 %78, i1 %77
  br i1 %79, label %72, label %.loopexit, !prof !5

; <label>:80:                                     ; preds = %42, %39, %40
  %.sink27 = phi i16 [ -55, %40 ], [ -48, %39 ], [ -87, %42 ]
  %81 = add i16 %.sink27, %32
  %82 = icmp slt i64 %1, 0
  br i1 %82, label %.loopexit, label %86

; <label>:83:                                     ; preds = %89
  %84 = sub nsw i64 0, %93
  %85 = icmp eq i64 %34, %11
  br i1 %85, label %.loopexit, label %46

; <label>:86:                                     ; preds = %80
  %87 = icmp eq i16 %81, 0
  %88 = icmp eq i64 %1, 0
  %or.cond14 = and i1 %88, %87
  br i1 %or.cond14, label %.loopexit, label %89

; <label>:89:                                     ; preds = %86
  %90 = trunc i64 %1 to i16
  %91 = and i64 %1, 65535
  %92 = icmp eq i64 %91, %1
  %93 = zext i16 %81 to i64
  %94 = icmp slt i64 %93, %1
  %95 = icmp ult i16 %81, %90
  %96 = select i1 %92, i1 %95, i1 %94
  br i1 %96, label %83, label %.loopexit, !prof !5

; <label>:97:                                     ; preds = %36
  br i1 %38, label %98, label %132, !prof !6

; <label>:98:                                     ; preds = %97
  %.off49 = add i16 %32, -65
  %99 = icmp ugt i16 %.off49, 25
  br i1 %99, label %100, label %132, !prof !6

; <label>:100:                                    ; preds = %98
  %.off50 = add i16 %32, -97
  %101 = icmp ugt i16 %.off50, 25
  br i1 %101, label %.loopexit, label %132, !prof !6

; <label>:102:                                    ; preds = %119
  %103 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %106, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %104 = add i64 %106, 4
  %105 = icmp slt i64 %104, 0
  br i1 %105, label %._crit_edge73, label %.lr.ph72

.lr.ph72:                                         ; preds = %.lr.ph72.preheader, %102
  %106 = phi i64 [ %104, %102 ], [ %139, %.lr.ph72.preheader ]
  %107 = phi i16 [ %103, %102 ], [ %138, %.lr.ph72.preheader ]
  %108 = phi i64 [ %117, %102 ], [ %148, %.lr.ph72.preheader ]
  store i64 %106, i64* %._position._compoundOffset._value, align 8
  store i64 0, i64* %27, align 8
  store i8 3, i8* %28, align 8
  %.off46 = add i16 %107, -48
  %109 = icmp ugt i16 %.off46, 9
  br i1 %109, label %110, label %121, !prof !6

; <label>:110:                                    ; preds = %.lr.ph72
  %.off47 = add i16 %107, -65
  %111 = icmp ugt i16 %.off47, 25
  br i1 %111, label %112, label %121, !prof !6

; <label>:112:                                    ; preds = %110
  %.off48 = add i16 %107, -97
  %113 = icmp ugt i16 %.off48, 25
  br i1 %113, label %.loopexit, label %121, !prof !6

; <label>:114:                                    ; preds = %124
  %115 = extractvalue { i64, i1 } %125, 0
  %116 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %115, i64 %128)
  %117 = extractvalue { i64, i1 } %116, 0
  %118 = extractvalue { i64, i1 } %116, 1
  br i1 %118, label %.loopexit, label %119, !prof !7

; <label>:119:                                    ; preds = %114
  %120 = icmp eq i64 %106, %11
  br i1 %120, label %.loopexit, label %102

; <label>:121:                                    ; preds = %112, %.lr.ph72, %110
  %.sink31 = phi i16 [ -55, %110 ], [ -48, %.lr.ph72 ], [ -87, %112 ]
  %122 = add i16 %.sink31, %107
  %123 = icmp eq i16 %122, 0
  %or.cond15 = and i1 %143, %123
  br i1 %or.cond15, label %.loopexit, label %127

; <label>:124:                                    ; preds = %127
  %125 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %108, i64 %1)
  %126 = extractvalue { i64, i1 } %125, 1
  br i1 %126, label %.loopexit, label %114

; <label>:127:                                    ; preds = %121
  %128 = zext i16 %122 to i64
  %129 = icmp slt i64 %128, %1
  %130 = icmp ult i16 %122, %145
  %131 = select i1 %147, i1 %130, i1 %129
  br i1 %131, label %124, label %.loopexit, !prof !5

; <label>:132:                                    ; preds = %100, %97, %98
  %.sink35 = phi i16 [ -55, %98 ], [ -48, %97 ], [ -87, %100 ]
  %133 = add i16 %.sink35, %32
  %134 = icmp slt i64 %1, 0
  br i1 %134, label %.loopexit, label %141

; <label>:135:                                    ; preds = %144
  %136 = icmp eq i64 %34, %11
  br i1 %136, label %.loopexit, label %137

; <label>:137:                                    ; preds = %135
  %138 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %34, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %139 = add i64 %34, 4
  %140 = icmp slt i64 %139, 0
  br i1 %140, label %._crit_edge73, label %.lr.ph72.preheader

.lr.ph72.preheader:                               ; preds = %137
  br label %.lr.ph72

; <label>:141:                                    ; preds = %132
  %142 = icmp eq i16 %133, 0
  %143 = icmp eq i64 %1, 0
  %or.cond16 = and i1 %143, %142
  br i1 %or.cond16, label %.loopexit, label %144

; <label>:144:                                    ; preds = %141
  %145 = trunc i64 %1 to i16
  %146 = and i64 %1, 65535
  %147 = icmp eq i64 %146, %1
  %148 = zext i16 %133 to i64
  %149 = icmp slt i64 %148, %1
  %150 = icmp ult i16 %133, %145
  %151 = select i1 %147, i1 %150, i1 %149
  br i1 %151, label %135, label %.loopexit, !prof !5

; <label>:152:                                    ; preds = %26
  %.off56 = add i16 %22, -48
  %153 = icmp ugt i16 %.off56, 9
  br i1 %153, label %154, label %188, !prof !6

; <label>:154:                                    ; preds = %152
  %.off60 = add i16 %22, -65
  %155 = icmp ugt i16 %.off60, 25
  br i1 %155, label %156, label %188, !prof !6

; <label>:156:                                    ; preds = %154
  %.off61 = add i16 %22, -97
  %157 = icmp ugt i16 %.off61, 25
  br i1 %157, label %.loopexit, label %188, !prof !6

; <label>:158:                                    ; preds = %175
  %159 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %162, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %160 = add i64 %162, 4
  %161 = icmp slt i64 %160, 0
  br i1 %161, label %._crit_edge, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph.preheader, %158
  %162 = phi i64 [ %160, %158 ], [ %195, %.lr.ph.preheader ]
  %163 = phi i16 [ %159, %158 ], [ %194, %.lr.ph.preheader ]
  %164 = phi i64 [ %173, %158 ], [ %204, %.lr.ph.preheader ]
  store i64 %162, i64* %._position._compoundOffset._value, align 8
  store i64 0, i64* %27, align 8
  store i8 3, i8* %28, align 8
  %.off57 = add i16 %163, -48
  %165 = icmp ugt i16 %.off57, 9
  br i1 %165, label %166, label %177, !prof !6

; <label>:166:                                    ; preds = %.lr.ph
  %.off58 = add i16 %163, -65
  %167 = icmp ugt i16 %.off58, 25
  br i1 %167, label %168, label %177, !prof !6

; <label>:168:                                    ; preds = %166
  %.off59 = add i16 %163, -97
  %169 = icmp ugt i16 %.off59, 25
  br i1 %169, label %.loopexit, label %177, !prof !6

; <label>:170:                                    ; preds = %180
  %171 = extractvalue { i64, i1 } %181, 0
  %172 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %171, i64 %184)
  %173 = extractvalue { i64, i1 } %172, 0
  %174 = extractvalue { i64, i1 } %172, 1
  br i1 %174, label %.loopexit, label %175, !prof !7

; <label>:175:                                    ; preds = %170
  %176 = icmp eq i64 %162, %11
  br i1 %176, label %.loopexit, label %158

; <label>:177:                                    ; preds = %168, %.lr.ph, %166
  %.sink39 = phi i16 [ -55, %166 ], [ -48, %.lr.ph ], [ -87, %168 ]
  %178 = add i16 %.sink39, %163
  %179 = icmp eq i16 %178, 0
  %or.cond17 = and i1 %199, %179
  br i1 %or.cond17, label %.loopexit, label %183

; <label>:180:                                    ; preds = %183
  %181 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %164, i64 %1)
  %182 = extractvalue { i64, i1 } %181, 1
  br i1 %182, label %.loopexit, label %170

; <label>:183:                                    ; preds = %177
  %184 = zext i16 %178 to i64
  %185 = icmp slt i64 %184, %1
  %186 = icmp ult i16 %178, %201
  %187 = select i1 %203, i1 %186, i1 %185
  br i1 %187, label %180, label %.loopexit, !prof !5

; <label>:188:                                    ; preds = %156, %152, %154
  %.sink43 = phi i16 [ -55, %154 ], [ -48, %152 ], [ -87, %156 ]
  %189 = add i16 %.sink43, %22
  %190 = icmp slt i64 %1, 0
  br i1 %190, label %.loopexit, label %197

; <label>:191:                                    ; preds = %200
  %192 = icmp eq i64 %24, %11
  br i1 %192, label %.loopexit, label %193

; <label>:193:                                    ; preds = %191
  %194 = call swiftcc i16 @_T0SS9UTF16ViewVs6UInt16VSS5IndexVcigTf4xn_n(i64 %24, %TSS9UTF16ViewV* noalias nocapture nonnull swiftself dereferenceable(40) %2)
  %195 = add i64 %24, 4
  %196 = icmp slt i64 %195, 0
  br i1 %196, label %._crit_edge, label %.lr.ph.preheader

.lr.ph.preheader:                                 ; preds = %193
  br label %.lr.ph

; <label>:197:                                    ; preds = %188
  %198 = icmp eq i16 %189, 0
  %199 = icmp eq i64 %1, 0
  %or.cond18 = and i1 %199, %198
  br i1 %or.cond18, label %.loopexit, label %200

; <label>:200:                                    ; preds = %197
  %201 = trunc i64 %1 to i16
  %202 = and i64 %1, 65535
  %203 = icmp eq i64 %202, %1
  %204 = zext i16 %189 to i64
  %205 = icmp slt i64 %204, %1
  %206 = icmp ult i16 %189, %201
  %207 = select i1 %203, i1 %206, i1 %205
  br i1 %207, label %191, label %.loopexit, !prof !5

; <label>:208:                                    ; preds = %entry
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:209:                                    ; preds = %9
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable

; <label>:210:                                    ; preds = %15
  tail call void asm sideeffect "", "n"(i32 2) #8
  tail call void @llvm.trap()
  unreachable

._crit_edge:                                      ; preds = %158, %193
  tail call void asm sideeffect "", "n"(i32 3) #8
  tail call void @llvm.trap()
  unreachable

; <label>:211:                                    ; preds = %31
  tail call void asm sideeffect "", "n"(i32 4) #8
  tail call void @llvm.trap()
  unreachable

._crit_edge73:                                    ; preds = %102, %137
  tail call void asm sideeffect "", "n"(i32 5) #8
  tail call void @llvm.trap()
  unreachable

._crit_edge70:                                    ; preds = %50, %46
  tail call void asm sideeffect "", "n"(i32 6) #8
  tail call void @llvm.trap()
  unreachable
}

define linkonce_odr hidden swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsExSgqd___Si5radixtcs14StringProtocolRd__lufCSi_SSTg5Tf4gXnd_n(i64, i64, i64, i64) local_unnamed_addr #1 {
entry:
  %temp-coercion.coerced.sroa.4.i = alloca i8, align 1
  %4 = alloca %Ts16IndexingIteratorVySS9UTF16ViewVG, align 8
  %5 = alloca %Ts27UnsafeBufferPointerIteratorVys6UInt16VG, align 8
  %.off = add i64 %3, -2
  %6 = icmp ugt i64 %.off, 34
  br i1 %6, label %208, label %7

; <label>:7:                                      ; preds = %entry
  %8 = icmp eq i64 %0, 0
  br i1 %8, label %192, label %9

; <label>:9:                                      ; preds = %7
  %10 = icmp sgt i64 %1, -1
  %11 = and i64 %1, 4611686018427387903
  br i1 %10, label %12, label %178, !prof !5

; <label>:12:                                     ; preds = %9
  %13 = inttoptr i64 %0 to %Ts5UInt8V*
  %14 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %13, i64 %11, i32 0
  %15 = inttoptr i64 %0 to i8*
  %16 = icmp ne i8* %14, null
  %17 = icmp eq i8* %14, %15
  %or.cond = and i1 %16, %17
  br i1 %or.cond, label %.loopexit, label %26

.loopexit:                                        ; preds = %106, %116, %108, %122, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit, %59, %68, %55, %62, %53, %155, %164, %151, %158, %149, %71, %167, %140, %169, %86, %127, %42, %74, %30, %12
  %.sroa.0.0 = phi i64 [ 0, %12 ], [ 0, %30 ], [ 0, %74 ], [ 0, %42 ], [ 0, %127 ], [ 0, %86 ], [ 0, %169 ], [ 0, %140 ], [ %174, %167 ], [ %72, %71 ], [ %156, %155 ], [ 0, %164 ], [ 0, %151 ], [ 0, %158 ], [ 0, %149 ], [ %60, %59 ], [ 0, %68 ], [ 0, %55 ], [ 0, %62 ], [ 0, %53 ], [ %90, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit ], [ 0, %106 ], [ 0, %116 ], [ 0, %108 ], [ 0, %122 ]
  %.sink = phi i1 [ true, %12 ], [ true, %30 ], [ true, %74 ], [ true, %42 ], [ true, %127 ], [ true, %86 ], [ true, %169 ], [ true, %140 ], [ false, %167 ], [ false, %71 ], [ false, %155 ], [ true, %164 ], [ true, %151 ], [ true, %158 ], [ true, %149 ], [ false, %59 ], [ true, %68 ], [ true, %55 ], [ true, %62 ], [ true, %53 ], [ false, %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit ], [ true, %106 ], [ true, %116 ], [ true, %108 ], [ true, %122 ]
  %18 = inttoptr i64 %2 to %swift.refcounted*
  %19 = tail call %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned %18) #8
  br label %20

; <label>:20:                                     ; preds = %.loopexit, %178, %192
  %.pre-phi = phi %swift.refcounted* [ %18, %.loopexit ], [ %190, %178 ], [ %206, %192 ]
  %21 = phi i64 [ %.sroa.0.0, %.loopexit ], [ %186, %178 ], [ %200, %192 ]
  %22 = phi i1 [ %.sink, %.loopexit ], [ %189, %178 ], [ %203, %192 ]
  tail call void @swift_rt_swift_release(%swift.refcounted* %.pre-phi) #8
  %23 = zext i1 %22 to i8
  %24 = insertvalue { i64, i8 } undef, i64 %21, 0
  %25 = insertvalue { i64, i8 } %24, i8 %23, 1
  ret { i64, i8 } %25

; <label>:26:                                     ; preds = %12
  %._value = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %13, i64 0, i32 0
  %27 = load i8, i8* %._value, align 1
  %28 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %13, i64 1, i32 0
  %29 = zext i8 %27 to i16
  switch i8 %27, label %136 [
    i8 43, label %30
    i8 45, label %30
  ], !prof !4

; <label>:30:                                     ; preds = %26, %26
  %31 = icmp eq i64 %11, 1
  %or.cond25 = and i1 %16, %31
  br i1 %or.cond25, label %.loopexit, label %32

; <label>:32:                                     ; preds = %30
  %33 = load i8, i8* %28, align 1
  %34 = getelementptr inbounds i8, i8* %28, i64 1
  %35 = ptrtoint i8* %34 to i64
  %36 = icmp eq i8 %27, 45
  %37 = zext i8 %33 to i16
  %.off79 = add i8 %33, -48
  %38 = icmp ugt i8 %.off79, 9
  br i1 %36, label %39, label %83, !prof !5

; <label>:39:                                     ; preds = %32
  br i1 %38, label %40, label %74, !prof !6

; <label>:40:                                     ; preds = %39
  %.off88 = add i8 %33, -65
  %41 = icmp ugt i8 %.off88, 25
  br i1 %41, label %42, label %74, !prof !6

; <label>:42:                                     ; preds = %40
  %.off89 = add i8 %33, -97
  %43 = icmp ugt i8 %.off89, 25
  br i1 %43, label %.loopexit, label %74, !prof !6

.lr.ph119:                                        ; preds = %.lr.ph119.preheader, %59
  %44 = phi i64 [ %60, %59 ], [ %72, %.lr.ph119.preheader ]
  %.sroa.0.078118 = phi i64 [ %48, %59 ], [ %35, %.lr.ph119.preheader ]
  %45 = inttoptr i64 %.sroa.0.078118 to %Ts5UInt8V*
  %._value18 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %45, i64 0, i32 0
  %46 = load i8, i8* %._value18, align 1
  %47 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %45, i64 1, i32 0
  %48 = ptrtoint i8* %47 to i64
  %49 = zext i8 %46 to i16
  %.off85 = add i8 %46, -48
  %50 = icmp ugt i8 %.off85, 9
  br i1 %50, label %51, label %62, !prof !6

; <label>:51:                                     ; preds = %.lr.ph119
  %.off86 = add i8 %46, -65
  %52 = icmp ugt i8 %.off86, 25
  br i1 %52, label %53, label %62, !prof !6

; <label>:53:                                     ; preds = %51
  %.off87 = add i8 %46, -97
  %54 = icmp ugt i8 %.off87, 25
  br i1 %54, label %.loopexit, label %62, !prof !6

; <label>:55:                                     ; preds = %68
  %56 = extractvalue { i64, i1 } %69, 0
  %57 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %56, i64 %64)
  %58 = extractvalue { i64, i1 } %57, 1
  br i1 %58, label %.loopexit, label %59, !prof !7

; <label>:59:                                     ; preds = %55
  %60 = extractvalue { i64, i1 } %57, 0
  %61 = icmp eq i8* %14, %47
  %or.cond27 = and i1 %16, %61
  br i1 %or.cond27, label %.loopexit, label %.lr.ph119

; <label>:62:                                     ; preds = %53, %.lr.ph119, %51
  %.sink42 = phi i16 [ -55, %51 ], [ -48, %.lr.ph119 ], [ -87, %53 ]
  %63 = add nsw i16 %.sink42, %49
  %64 = zext i16 %63 to i64
  %65 = icmp slt i64 %64, %3
  %66 = icmp ult i16 %63, %76
  %67 = select i1 %78, i1 %66, i1 %65
  br i1 %67, label %68, label %.loopexit, !prof !5

; <label>:68:                                     ; preds = %62
  %69 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %44, i64 %3)
  %70 = extractvalue { i64, i1 } %69, 1
  br i1 %70, label %.loopexit, label %55

; <label>:71:                                     ; preds = %74
  %72 = sub nsw i64 0, %79
  %73 = icmp eq i8* %14, %34
  %or.cond27117 = and i1 %16, %73
  br i1 %or.cond27117, label %.loopexit, label %.lr.ph119.preheader

.lr.ph119.preheader:                              ; preds = %71
  br label %.lr.ph119

; <label>:74:                                     ; preds = %42, %39, %40
  %.sink46 = phi i16 [ -55, %40 ], [ -48, %39 ], [ -87, %42 ]
  %75 = add nsw i16 %.sink46, %37
  %76 = trunc i64 %3 to i16
  %77 = and i64 %3, 65535
  %78 = icmp eq i64 %77, %3
  %79 = zext i16 %75 to i64
  %80 = icmp slt i64 %79, %3
  %81 = icmp ult i16 %75, %76
  %82 = select i1 %78, i1 %81, i1 %80
  br i1 %82, label %71, label %.loopexit, !prof !5

; <label>:83:                                     ; preds = %32
  br i1 %38, label %84, label %127, !prof !6

; <label>:84:                                     ; preds = %83
  %.off83 = add i8 %33, -65
  %85 = icmp ugt i8 %.off83, 25
  br i1 %85, label %86, label %127, !prof !6

; <label>:86:                                     ; preds = %84
  %.off84 = add i8 %33, -97
  %87 = icmp ugt i8 %.off84, 25
  br i1 %87, label %.loopexit, label %127, !prof !6

; <label>:88:                                     ; preds = %125, %112
  %89 = phi %Ts5UInt8V* [ %126, %125 ], [ %115, %112 ]
  %90 = phi i64 [ %132, %125 ], [ %113, %112 ]
  %.sroa.0.1152 = phi i64 [ %35, %125 ], [ %.sroa.0.2, %112 ]
  %91 = inttoptr i64 %.sroa.0.1152 to i8*
  %92 = icmp eq i8* %14, %91
  %or.cond.i = and i1 %16, %92
  br i1 %or.cond.i, label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit, label %93

; <label>:93:                                     ; preds = %88
  %._value.i = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %89, i64 0, i32 0
  %94 = load i8, i8* %._value.i, align 1
  %95 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %89, i64 1, i32 0
  %96 = ptrtoint i8* %95 to i64
  %phitmp.i = zext i8 %94 to i16
  br label %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit

; <label>:97:                                     ; preds = %112
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit: ; preds = %88, %93
  %.sroa.0.2 = phi i64 [ %.sroa.0.1152, %88 ], [ %96, %93 ]
  %98 = phi i16 [ 0, %88 ], [ %phitmp.i, %93 ]
  %99 = phi i1 [ true, %88 ], [ false, %93 ]
  call void @llvm.lifetime.start.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i)
  store i1 %99, i1* %temp-coercion.coerced.sroa.4.i.0..sroa_cast, align 1
  %temp-coercion.coerced.sroa.4.i.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i = load i8, i8* %temp-coercion.coerced.sroa.4.i, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.i)
  %100 = and i8 %temp-coercion.coerced.sroa.4.i.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0.temp-coercion.coerced.sroa.4.0..i, 1
  %101 = icmp eq i8 %100, 0
  br i1 %101, label %102, label %.loopexit

; <label>:102:                                    ; preds = %_T0s27UnsafeBufferPointerIteratorV4nextxSgyFs5UInt8V_Tgq5.exit
  %.off80 = add nsw i16 %98, -48
  %103 = icmp ugt i16 %.off80, 9
  br i1 %103, label %104, label %116, !prof !6

; <label>:104:                                    ; preds = %102
  %.off81 = add nsw i16 %98, -65
  %105 = icmp ugt i16 %.off81, 25
  br i1 %105, label %106, label %116, !prof !6

; <label>:106:                                    ; preds = %104
  %.off82 = add nsw i16 %98, -97
  %107 = icmp ugt i16 %.off82, 25
  br i1 %107, label %.loopexit, label %116, !prof !6

; <label>:108:                                    ; preds = %122
  %109 = extractvalue { i64, i1 } %123, 0
  %110 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %109, i64 %118)
  %111 = extractvalue { i64, i1 } %110, 1
  br i1 %111, label %.loopexit, label %112, !prof !7

; <label>:112:                                    ; preds = %108
  %113 = extractvalue { i64, i1 } %110, 0
  %114 = icmp eq i64 %.sroa.0.2, 0
  %115 = inttoptr i64 %.sroa.0.2 to %Ts5UInt8V*
  br i1 %114, label %97, label %88

; <label>:116:                                    ; preds = %106, %102, %104
  %.sink50 = phi i16 [ -55, %104 ], [ -48, %102 ], [ -87, %106 ]
  %117 = add nsw i16 %.sink50, %98
  %118 = zext i16 %117 to i64
  %119 = icmp slt i64 %118, %3
  %120 = icmp ult i16 %117, %129
  %121 = select i1 %131, i1 %120, i1 %119
  br i1 %121, label %122, label %.loopexit, !prof !5

; <label>:122:                                    ; preds = %116
  %123 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %90, i64 %3)
  %124 = extractvalue { i64, i1 } %123, 1
  br i1 %124, label %.loopexit, label %108

; <label>:125:                                    ; preds = %127
  %temp-coercion.coerced.sroa.4.i.0..sroa_cast = bitcast i8* %temp-coercion.coerced.sroa.4.i to i1*
  %126 = bitcast i8* %34 to %Ts5UInt8V*
  br label %88

; <label>:127:                                    ; preds = %86, %83, %84
  %.sink54 = phi i16 [ -55, %84 ], [ -48, %83 ], [ -87, %86 ]
  %128 = add nsw i16 %.sink54, %37
  %129 = trunc i64 %3 to i16
  %130 = and i64 %3, 65535
  %131 = icmp eq i64 %130, %3
  %132 = zext i16 %128 to i64
  %133 = icmp slt i64 %132, %3
  %134 = icmp ult i16 %128, %129
  %135 = select i1 %131, i1 %134, i1 %133
  br i1 %135, label %125, label %.loopexit, !prof !5

; <label>:136:                                    ; preds = %26
  %.off90 = add i8 %27, -48
  %137 = icmp ugt i8 %.off90, 9
  br i1 %137, label %138, label %169, !prof !6

; <label>:138:                                    ; preds = %136
  %.off94 = add i8 %27, -65
  %139 = icmp ugt i8 %.off94, 25
  br i1 %139, label %140, label %169, !prof !6

; <label>:140:                                    ; preds = %138
  %.off95 = add i8 %27, -97
  %141 = icmp ugt i8 %.off95, 25
  br i1 %141, label %.loopexit, label %169, !prof !6

.lr.ph:                                           ; preds = %.lr.ph.preheader, %155
  %142 = phi i64 [ %156, %155 ], [ %174, %.lr.ph.preheader ]
  %.sroa.0.3.in103 = phi i8* [ %144, %155 ], [ %28, %.lr.ph.preheader ]
  %143 = load i8, i8* %.sroa.0.3.in103, align 1
  %144 = getelementptr inbounds i8, i8* %.sroa.0.3.in103, i64 1
  %145 = zext i8 %143 to i16
  %.off91 = add i8 %143, -48
  %146 = icmp ugt i8 %.off91, 9
  br i1 %146, label %147, label %158, !prof !6

; <label>:147:                                    ; preds = %.lr.ph
  %.off92 = add i8 %143, -65
  %148 = icmp ugt i8 %.off92, 25
  br i1 %148, label %149, label %158, !prof !6

; <label>:149:                                    ; preds = %147
  %.off93 = add i8 %143, -97
  %150 = icmp ugt i8 %.off93, 25
  br i1 %150, label %.loopexit, label %158, !prof !6

; <label>:151:                                    ; preds = %164
  %152 = extractvalue { i64, i1 } %165, 0
  %153 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %152, i64 %160)
  %154 = extractvalue { i64, i1 } %153, 1
  br i1 %154, label %.loopexit, label %155, !prof !7

; <label>:155:                                    ; preds = %151
  %156 = extractvalue { i64, i1 } %153, 0
  %157 = icmp eq i8* %144, %14
  %or.cond33 = and i1 %16, %157
  br i1 %or.cond33, label %.loopexit, label %.lr.ph

; <label>:158:                                    ; preds = %149, %.lr.ph, %147
  %.sink58 = phi i16 [ -55, %147 ], [ -48, %.lr.ph ], [ -87, %149 ]
  %159 = add nsw i16 %.sink58, %145
  %160 = zext i16 %159 to i64
  %161 = icmp slt i64 %160, %3
  %162 = icmp ult i16 %159, %171
  %163 = select i1 %173, i1 %162, i1 %161
  br i1 %163, label %164, label %.loopexit, !prof !5

; <label>:164:                                    ; preds = %158
  %165 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %142, i64 %3)
  %166 = extractvalue { i64, i1 } %165, 1
  br i1 %166, label %.loopexit, label %151

; <label>:167:                                    ; preds = %169
  %168 = icmp eq i64 %11, 1
  %or.cond33102 = and i1 %16, %168
  br i1 %or.cond33102, label %.loopexit, label %.lr.ph.preheader

.lr.ph.preheader:                                 ; preds = %167
  br label %.lr.ph

; <label>:169:                                    ; preds = %140, %136, %138
  %.sink62 = phi i16 [ -55, %138 ], [ -48, %136 ], [ -87, %140 ]
  %170 = add nsw i16 %.sink62, %29
  %171 = trunc i64 %3 to i16
  %172 = and i64 %3, 65535
  %173 = icmp eq i64 %172, %3
  %174 = zext i16 %170 to i64
  %175 = icmp slt i64 %174, %3
  %176 = icmp ult i16 %170, %171
  %177 = select i1 %173, i1 %176, i1 %175
  br i1 %177, label %167, label %.loopexit, !prof !5

; <label>:178:                                    ; preds = %9
  %179 = inttoptr i64 %0 to %Ts6UInt16V*
  %180 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %179, i64 %11
  %181 = ptrtoint %Ts6UInt16V* %180 to i64
  %182 = getelementptr inbounds %Ts27UnsafeBufferPointerIteratorVys6UInt16VG, %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %5, i64 0, i32 0, i32 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 16, i8* nonnull %182)
  %183 = bitcast %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %5 to i64*
  store i64 %0, i64* %183, align 8
  %._end = getelementptr inbounds %Ts27UnsafeBufferPointerIteratorVys6UInt16VG, %Ts27UnsafeBufferPointerIteratorVys6UInt16VG* %5, i64 0, i32 1
  %184 = bitcast %TSPys6UInt16VGSg* %._end to i64*
  store i64 %181, i64* %184, align 8
  %185 = call swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsE19_parseASCIISlowPathqd_0_Sgqd__z9codeUnits_qd_0_5radixts16IteratorProtocolRd__sAARd_0_s08UnsignedC07ElementRpd__r0_lFZSi_s019UnsafeBufferPointerJ0Vys6UInt16VGSiTg5Tf4nnd_n(%Ts27UnsafeBufferPointerIteratorVys6UInt16VG* nocapture nonnull dereferenceable(16) %5, i64 %3)
  %186 = extractvalue { i64, i8 } %185, 0
  %187 = extractvalue { i64, i8 } %185, 1
  %188 = and i8 %187, 1
  %189 = icmp ne i8 %188, 0
  call void @llvm.lifetime.end.p0i8(i64 16, i8* nonnull %182)
  %190 = inttoptr i64 %2 to %swift.refcounted*
  %191 = tail call %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned %190) #8
  br label %20

; <label>:192:                                    ; preds = %7
  %193 = bitcast %Ts16IndexingIteratorVySS9UTF16ViewVG* %4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 57, i8* nonnull %193)
  %194 = and i64 %1, 4611686018427387903
  %._elements._offset._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 0, i32 0, i32 0
  store i64 0, i64* %._elements._offset._value, align 8
  %._elements._length._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 0, i32 1, i32 0
  store i64 %194, i64* %._elements._length._value, align 8
  %._elements._core = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 0, i32 2
  %195 = bitcast %Ts11_StringCoreV* %._elements._core to i64*
  store i64 0, i64* %195, align 8
  %._elements._core._countAndFlags._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 0, i32 2, i32 1, i32 0
  store i64 %1, i64* %._elements._core._countAndFlags._value, align 8
  %._elements._core._owner = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 0, i32 2, i32 2
  %196 = bitcast %TyXlSg* %._elements._core._owner to i64*
  store i64 %2, i64* %196, align 8
  %._position._compoundOffset._value = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 1, i32 0, i32 0
  %197 = getelementptr inbounds %Ts16IndexingIteratorVySS9UTF16ViewVG, %Ts16IndexingIteratorVySS9UTF16ViewVG* %4, i64 0, i32 1, i32 1, i32 1, i64 0
  %198 = bitcast i64* %._position._compoundOffset._value to i8*
  call void @llvm.memset.p0i8.i64(i8* %198, i8 0, i64 16, i32 8, i1 false)
  store i8 3, i8* %197, align 8
  %199 = call swiftcc { i64, i8 } @_T0s17FixedWidthIntegerPsE19_parseASCIISlowPathqd_0_Sgqd__z9codeUnits_qd_0_5radixts16IteratorProtocolRd__sAARd_0_s08UnsignedC07ElementRpd__r0_lFZSi_s08IndexingJ0VySS9UTF16ViewVGSiTg5Tf4nnd_n(%Ts16IndexingIteratorVySS9UTF16ViewVG* nocapture nonnull dereferenceable(57) %4, i64 %3)
  %200 = extractvalue { i64, i8 } %199, 0
  %201 = extractvalue { i64, i8 } %199, 1
  %202 = and i8 %201, 1
  %203 = icmp ne i8 %202, 0
  %204 = bitcast %TyXlSg* %._elements._core._owner to %swift.refcounted**
  %205 = load %swift.refcounted*, %swift.refcounted** %204, align 8
  %206 = inttoptr i64 %2 to %swift.refcounted*
  %207 = tail call %swift.refcounted* @swift_rt_swift_retain_n(%swift.refcounted* %206, i32 2)
  tail call void @swift_rt_swift_release(%swift.refcounted* %205) #8
  call void @llvm.lifetime.end.p0i8(i64 57, i8* nonnull %193)
  br label %20

; <label>:208:                                    ; preds = %entry
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #6

; Function Attrs: noinline nounwind readonly
define linkonce_odr hidden %swift.type* @swift_rt_swift_getExistentialTypeMetadata(i1, %swift.type*, i64, %swift.protocol**) local_unnamed_addr #7 {
entry:
  %load = load %swift.type* (i1, %swift.type*, i64, %swift.protocol**)*, %swift.type* (i1, %swift.type*, i64, %swift.protocol**)** @_swift_getExistentialTypeMetadata, align 8
  %4 = tail call %swift.type* %load(i1 %0, %swift.type* %1, i64 %2, %swift.protocol** %3) #8
  ret %swift.type* %4
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #6

declare %swift.type* @_T0s23_ContiguousArrayStorageCMa(%swift.type*) local_unnamed_addr #1

; Function Attrs: noinline nounwind
define linkonce_odr hidden %swift.refcounted* @swift_rt_swift_allocObject(%swift.type*, i64, i64) local_unnamed_addr #0 {
entry:
  %load = load %swift.refcounted* (%swift.type*, i64, i64)*, %swift.refcounted* (%swift.type*, i64, i64)** @_swift_allocObject, align 8
  %3 = tail call %swift.refcounted* %load(%swift.type* %0, i64 %1, i64 %2) #8
  ret %swift.refcounted* %3
}

; Function Attrs: nounwind
declare %swift.refcounted* @swift_initStackObject(%swift.type*, %swift.refcounted*) local_unnamed_addr #8

; Function Attrs: noinline
define linkonce_odr hidden swiftcc i8 @_T0s7UnicodeO4UTF8O13ForwardParserV14_invalidLengths5UInt8VyFTf4x_n(i32) local_unnamed_addr #5 {
entry:
  %1 = and i32 %0, 49392
  %2 = icmp eq i32 %1, 32992
  br i1 %2, label %15, label %3

; <label>:3:                                      ; preds = %entry
  %4 = and i32 %0, 49400
  %5 = icmp eq i32 %4, 33008
  br i1 %5, label %6, label %17

; <label>:6:                                      ; preds = %3
  %7 = trunc i32 %0 to i16
  %8 = and i16 %7, 12295
  %9 = icmp eq i16 %8, 0
  %10 = tail call i16 @llvm.bswap.i16(i16 %8)
  %11 = icmp ugt i16 %10, 1024
  %or.cond2 = or i1 %9, %11
  br i1 %or.cond2, label %17, label %12

; <label>:12:                                     ; preds = %6
  %13 = and i32 %0, 12582912
  %14 = icmp eq i32 %13, 8388608
  %. = select i1 %14, i8 3, i8 2
  br label %18

; <label>:15:                                     ; preds = %entry
  %16 = and i32 %0, 8207
  %trunc = trunc i32 %16 to i14
  switch i14 %trunc, label %18 [
    i14 0, label %17
    i14 -8179, label %17
  ]

; <label>:17:                                     ; preds = %15, %15, %6, %3
  br label %18

; <label>:18:                                     ; preds = %15, %12, %17
  %19 = phi i8 [ 1, %17 ], [ %., %12 ], [ 2, %15 ]
  ret i8 %19
}

define linkonce_odr hidden swiftcc { i64, i16 } @_T0s7UnicodeO5UTF16O16transcodedLengthSi5count_Sb7isASCIItSgx2of_q_m9decodedAsSb27repairingIllFormedSequencests16IteratorProtocolRzs01_A8EncodingR_8CodeUnitQy_7ElementRtzr0_lFZs019UnsafeBufferPointerO0Vys5UInt8VG_AB4UTF8OTgq5Tf4xnnd_n(i64, i64, %swift.type*, i1) local_unnamed_addr #1 {
entry:
  %temp-coercion.coerced.sroa.4.sroa.4 = alloca i8, align 1
  %4 = icmp eq %swift.type* %2, @_T0s7UnicodeO4UTF8ON
  br i1 %4, label %5, label %entry._crit_edge

entry._crit_edge:                                 ; preds = %entry
  %.pre = inttoptr i64 %1 to i8*
  br label %26

; <label>:5:                                      ; preds = %entry
  %6 = icmp eq i64 %0, 0
  br i1 %6, label %._crit_edge119, label %.lr.ph118

.lr.ph118:                                        ; preds = %5
  %7 = icmp ne i64 %1, 0
  %8 = inttoptr i64 %1 to i8*
  %9 = inttoptr i64 %0 to i8*
  %10 = icmp eq i8* %9, %8
  %or.cond225 = and i1 %7, %10
  br i1 %or.cond225, label %.thread, label %.lr.ph227.preheader

.lr.ph227.preheader:                              ; preds = %.lr.ph118
  br label %.lr.ph227

._crit_edge119:                                   ; preds = %5
  %11 = icmp eq i64 %1, 0
  br i1 %11, label %.thread, label %206

.lr.ph227:                                        ; preds = %.lr.ph227.preheader, %21
  %.sroa.0.0116226 = phi i64 [ %16, %21 ], [ %0, %.lr.ph227.preheader ]
  %12 = phi i64 [ %22, %21 ], [ 0, %.lr.ph227.preheader ]
  %13 = inttoptr i64 %.sroa.0.0116226 to %Ts5UInt8V*
  %._value21 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %13, i64 0, i32 0
  %14 = load i8, i8* %._value21, align 1
  %15 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %13, i64 1, i32 0
  %16 = ptrtoint i8* %15 to i64
  %17 = icmp sgt i8 %14, -1
  br i1 %17, label %18, label %24, !prof !5

; <label>:18:                                     ; preds = %.lr.ph227
  %19 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %12, i64 1)
  %20 = extractvalue { i64, i1 } %19, 1
  br i1 %20, label %207, label %21

; <label>:21:                                     ; preds = %18
  %22 = extractvalue { i64, i1 } %19, 0
  %23 = icmp eq i8* %15, %8
  %or.cond = and i1 %7, %23
  br i1 %or.cond, label %.thread, label %.lr.ph227

; <label>:24:                                     ; preds = %.lr.ph227
  %25 = zext i8 %14 to i32
  br label %26

; <label>:26:                                     ; preds = %entry._crit_edge, %24
  %.pre-phi = phi i8* [ %.pre, %entry._crit_edge ], [ %8, %24 ]
  %.sroa.13.0 = phi i8 [ 0, %entry._crit_edge ], [ 8, %24 ]
  %.sroa.0.055 = phi i32 [ 0, %entry._crit_edge ], [ %25, %24 ]
  %.sroa.0.2 = phi i64 [ %0, %entry._crit_edge ], [ %16, %24 ]
  %27 = phi i64 [ 0, %entry._crit_edge ], [ %12, %24 ]
  %28 = icmp ne i64 %1, 0
  %29 = icmp eq i64 %1, 0
  br label %.backedge

.backedge:                                        ; preds = %.backedge.backedge, %26
  %.sroa.13.1 = phi i8 [ %.sroa.13.0, %26 ], [ %.sroa.13.1.be, %.backedge.backedge ]
  %.sroa.0.156 = phi i32 [ %.sroa.0.055, %26 ], [ %.sroa.0.156.be, %.backedge.backedge ]
  %.sroa.0.3 = phi i64 [ %.sroa.0.2, %26 ], [ %.sroa.0.3.be, %.backedge.backedge ]
  %30 = phi i64 [ %27, %26 ], [ %.be, %.backedge.backedge ]
  %31 = phi i16 [ 0, %26 ], [ %.be230, %.backedge.backedge ]
  %32 = icmp eq i8 %.sroa.13.1, 0
  br i1 %32, label %40, label %33, !prof !5

; <label>:33:                                     ; preds = %.backedge
  %34 = trunc i32 %.sroa.0.156 to i8
  %35 = icmp sgt i8 %34, -1
  br i1 %35, label %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit, label %.preheader

_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit: ; preds = %33
  %36 = lshr i32 %.sroa.0.156, 8
  %37 = add i8 %.sroa.13.1, -8
  %38 = add i32 %.sroa.0.156, 1
  %39 = and i32 %38, 255
  br label %122

; <label>:40:                                     ; preds = %.backedge
  %41 = icmp eq i64 %.sroa.0.3, 0
  br i1 %41, label %42, label %43

; <label>:42:                                     ; preds = %40
  br i1 %29, label %.loopexit74, label %205

; <label>:43:                                     ; preds = %40
  %44 = inttoptr i64 %.sroa.0.3 to i8*
  %45 = icmp eq i8* %.pre-phi, %44
  %or.cond28 = and i1 %28, %45
  br i1 %or.cond28, label %.loopexit74, label %46

; <label>:46:                                     ; preds = %43
  %47 = inttoptr i64 %.sroa.0.3 to %Ts5UInt8V*
  %._value12 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %47, i64 0, i32 0
  %48 = load i8, i8* %._value12, align 1
  %49 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %47, i64 1, i32 0
  %50 = ptrtoint i8* %49 to i64
  %51 = icmp sgt i8 %48, -1
  br i1 %51, label %103, label %.preheader.thread

.preheader.thread:                                ; preds = %46
  %52 = zext i8 %48 to i32
  %53 = or i32 %.sroa.0.156, %52
  br label %.lr.ph

.preheader:                                       ; preds = %33
  %54 = icmp eq i64 %.sroa.0.3, 0
  br i1 %54, label %._crit_edge, label %.lr.ph

.lr.ph:                                           ; preds = %.preheader.thread, %.preheader
  %.sroa.0.4.ph161 = phi i64 [ %50, %.preheader.thread ], [ %.sroa.0.3, %.preheader ]
  %.sroa.0.257.ph160 = phi i32 [ %53, %.preheader.thread ], [ %.sroa.0.156, %.preheader ]
  %.sroa.13.2.ph159 = phi i8 [ 8, %.preheader.thread ], [ %.sroa.13.1, %.preheader ]
  %55 = inttoptr i64 %.sroa.0.4.ph161 to i8*
  %56 = icmp eq i8* %.pre-phi, %55
  %or.cond30218 = and i1 %28, %56
  br i1 %or.cond30218, label %.loopexit, label %.lr.ph222

._crit_edge:                                      ; preds = %.preheader
  br i1 %29, label %.loopexit73, label %201

; <label>:57:                                     ; preds = %.lr.ph222
  %58 = icmp eq i8* %.pre-phi, %61
  %or.cond30 = and i1 %28, %58
  br i1 %or.cond30, label %.loopexit, label %.lr.ph222.1

.lr.ph222:                                        ; preds = %.lr.ph
  %59 = inttoptr i64 %.sroa.0.4.ph161 to %Ts5UInt8V*
  %._value = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %59, i64 0, i32 0
  %60 = load i8, i8* %._value, align 1
  %61 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %59, i64 1, i32 0
  %62 = ptrtoint i8* %61 to i64
  %63 = zext i8 %60 to i32
  %64 = and i8 %.sroa.13.2.ph159, 31
  %65 = zext i8 %64 to i32
  %66 = shl i32 %63, %65
  %67 = or i32 %66, %.sroa.0.257.ph160
  %68 = add i8 %.sroa.13.2.ph159, 8
  %69 = icmp ult i8 %68, 32
  br i1 %69, label %57, label %.loopexit73

.loopexit:                                        ; preds = %.lr.ph222.4, %57, %218, %230, %242, %.lr.ph
  %.sroa.0.4113.lcssa = phi i64 [ %.sroa.0.4.ph161, %.lr.ph ], [ %62, %57 ], [ %210, %218 ], [ %222, %230 ], [ %234, %242 ], [ %246, %.lr.ph222.4 ]
  %.sroa.0.257112.lcssa = phi i32 [ %.sroa.0.257.ph160, %.lr.ph ], [ %67, %57 ], [ %215, %218 ], [ %227, %230 ], [ %239, %242 ], [ %251, %.lr.ph222.4 ]
  %.sroa.13.2111.lcssa = phi i8 [ %.sroa.13.2.ph159, %.lr.ph ], [ %68, %57 ], [ %216, %218 ], [ %228, %230 ], [ %240, %242 ], [ %252, %.lr.ph222.4 ]
  %70 = icmp eq i8 %.sroa.13.2111.lcssa, 0
  br i1 %70, label %.loopexit74, label %.loopexit73

.loopexit73:                                      ; preds = %.lr.ph222, %.lr.ph222.1, %.lr.ph222.2, %.lr.ph222.3, %.lr.ph222.4, %._crit_edge, %.loopexit
  %.sroa.13.3 = phi i8 [ %.sroa.13.2111.lcssa, %.loopexit ], [ %.sroa.13.1, %._crit_edge ], [ %68, %.lr.ph222 ], [ %216, %.lr.ph222.1 ], [ %228, %.lr.ph222.2 ], [ %240, %.lr.ph222.3 ], [ %252, %.lr.ph222.4 ]
  %.sroa.0.358 = phi i32 [ %.sroa.0.257112.lcssa, %.loopexit ], [ %.sroa.0.156, %._crit_edge ], [ %67, %.lr.ph222 ], [ %215, %.lr.ph222.1 ], [ %227, %.lr.ph222.2 ], [ %239, %.lr.ph222.3 ], [ %251, %.lr.ph222.4 ]
  %.sroa.0.5 = phi i64 [ %.sroa.0.4113.lcssa, %.loopexit ], [ 0, %._crit_edge ], [ %62, %.lr.ph222 ], [ %210, %.lr.ph222.1 ], [ %222, %.lr.ph222.2 ], [ %234, %.lr.ph222.3 ], [ %246, %.lr.ph222.4 ]
  %71 = and i32 %.sroa.0.358, 49376
  %72 = icmp eq i32 %71, 32960
  br i1 %72, label %87, label %73

; <label>:73:                                     ; preds = %.loopexit73
  %74 = and i32 %.sroa.0.358, 12632304
  %75 = icmp eq i32 %74, 8421600
  br i1 %75, label %85, label %76

; <label>:76:                                     ; preds = %73
  %77 = and i32 %.sroa.0.358, -1061109512
  %78 = icmp eq i32 %77, -2139062032
  br i1 %78, label %79, label %107

; <label>:79:                                     ; preds = %76
  %80 = trunc i32 %.sroa.0.358 to i16
  %81 = and i16 %80, 12295
  %82 = icmp eq i16 %81, 0
  %83 = tail call i16 @llvm.bswap.i16(i16 %81)
  %84 = icmp ugt i16 %83, 1024
  %or.cond44 = or i1 %82, %84
  br i1 %or.cond44, label %107, label %90, !prof !6

; <label>:85:                                     ; preds = %73
  %86 = and i32 %.sroa.0.358, 8207
  %trunc = trunc i32 %86 to i14
  switch i14 %trunc, label %90 [
    i14 0, label %107
    i14 -8179, label %107
  ], !prof !4

; <label>:87:                                     ; preds = %.loopexit73
  %88 = and i32 %.sroa.0.358, 30
  %89 = icmp eq i32 %88, 0
  br i1 %89, label %107, label %90, !prof !7

; <label>:90:                                     ; preds = %87, %79, %85
  %.ph = phi i8 [ 24, %85 ], [ 32, %79 ], [ 16, %87 ]
  %91 = zext i32 %.sroa.0.358 to i64
  %92 = zext i8 %.ph to i64
  %93 = lshr i64 %91, %92
  %94 = trunc i64 %93 to i32
  %95 = sub i8 %.sroa.13.3, %.ph
  %96 = add i32 %.sroa.0.358, 16843009
  %97 = lshr exact i8 %.ph, 1
  %98 = zext i8 %97 to i32
  %99 = shl i32 -1, %98
  %100 = shl i32 %99, %98
  %101 = xor i32 %100, -1
  %102 = and i32 %96, %101
  br label %122

; <label>:103:                                    ; preds = %46
  %104 = add i8 %48, 1
  %105 = zext i8 %104 to i32
  br label %122

.loopexit74:                                      ; preds = %.loopexit, %43, %42
  %106 = icmp ult i16 %31, 128
  %phitmp = zext i1 %106 to i16
  br label %.thread

; <label>:107:                                    ; preds = %85, %85, %87, %79, %76
  %108 = tail call swiftcc i8 @_T0s7UnicodeO4UTF8O13ForwardParserV14_invalidLengths5UInt8VyFTf4x_n(i32 %.sroa.0.358)
  %109 = shl i8 %108, 3
  %110 = zext i32 %.sroa.0.358 to i64
  %111 = and i8 %109, 56
  %112 = zext i8 %111 to i64
  %113 = lshr i64 %110, %112
  %114 = trunc i64 %113 to i32
  %115 = sub i8 %.sroa.13.3, %109
  br i1 %3, label %116, label %.thread, !prof !5

; <label>:116:                                    ; preds = %107
  %117 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %30, i64 1)
  %118 = extractvalue { i64, i1 } %117, 1
  br i1 %118, label %202, label %119

; <label>:119:                                    ; preds = %116
  %120 = extractvalue { i64, i1 } %117, 0
  %121 = or i16 %31, -3
  br label %.backedge.backedge

.backedge.backedge:                               ; preds = %.prol.loopexit, %185, %119
  %.sroa.13.1.be = phi i8 [ %115, %119 ], [ %.sroa.13.4.ph65, %185 ], [ %.sroa.13.4.ph65, %.prol.loopexit ]
  %.sroa.0.156.be = phi i32 [ %114, %119 ], [ %.sroa.0.459.ph66, %185 ], [ %.sroa.0.459.ph66, %.prol.loopexit ]
  %.sroa.0.3.be = phi i64 [ %.sroa.0.5, %119 ], [ %.sroa.0.6.ph67, %185 ], [ %.sroa.0.6.ph67, %.prol.loopexit ]
  %.be = phi i64 [ %120, %119 ], [ %170, %185 ], [ %170, %.prol.loopexit ]
  %.be230 = phi i16 [ %121, %119 ], [ %.lcssa237.unr, %.prol.loopexit ], [ %193, %185 ]
  br label %.backedge

; <label>:122:                                    ; preds = %103, %90, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit
  %.sroa.13.4.ph65 = phi i8 [ %37, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ], [ %95, %90 ], [ 0, %103 ]
  %.sroa.0.459.ph66 = phi i32 [ %36, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ], [ %94, %90 ], [ %.sroa.0.156, %103 ]
  %.sroa.0.sroa.0.0.ph = phi i32 [ %39, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ], [ %102, %90 ], [ %105, %103 ]
  %.sroa.0.6.ph67 = phi i64 [ %.sroa.0.3, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ], [ %.sroa.0.5, %90 ], [ %50, %103 ]
  %123 = tail call i32 @llvm.ctlz.i32(i32 %.sroa.0.sroa.0.0.ph, i1 false), !range !8
  %124 = zext i32 %123 to i64
  %125 = lshr i64 %124, 3
  %126 = sub nsw i64 4, %125
  %127 = icmp eq i64 %126, 1
  br i1 %127, label %162, label %128, !prof !5

; <label>:128:                                    ; preds = %122
  %129 = add i32 %.sroa.0.sroa.0.0.ph, -16843009
  %130 = shl i32 %129, 6
  %131 = lshr i32 %129, 8
  %132 = and i32 %131, 63
  %133 = or i32 %132, %130
  %134 = icmp eq i64 %126, 2
  br i1 %134, label %160, label %135, !prof !5

; <label>:135:                                    ; preds = %128
  %136 = shl i32 %133, 6
  %137 = lshr i32 %129, 16
  %138 = and i32 %137, 63
  %139 = or i32 %136, %138
  %140 = icmp eq i64 %126, 3
  br i1 %140, label %158, label %141, !prof !5

; <label>:141:                                    ; preds = %135
  %142 = shl i32 %139, 6
  %143 = lshr i32 %129, 24
  %144 = and i32 %143, 63
  %.masked = and i32 %142, 2097088
  %145 = or i32 %.masked, %144
  %146 = icmp ult i32 %145, 65536
  br i1 %146, label %165, label %147, !prof !5

; <label>:147:                                    ; preds = %141
  %148 = tail call { i32, i1 } @llvm.usub.with.overflow.i32(i32 %145, i32 65536)
  %149 = extractvalue { i32, i1 } %148, 0
  %150 = extractvalue { i32, i1 } %148, 1
  br i1 %150, label %204, label %151

; <label>:151:                                    ; preds = %147
  %152 = lshr i32 %149, 10
  %153 = and i32 %152, 1023
  %154 = shl i32 %149, 16
  %155 = and i32 %154, 67043328
  %156 = or i32 %153, %155
  %157 = or i32 %156, -603924480
  br label %165

; <label>:158:                                    ; preds = %135
  %159 = and i32 %139, 65535
  br label %165

; <label>:160:                                    ; preds = %128
  %161 = and i32 %133, 2047
  br label %165

; <label>:162:                                    ; preds = %122
  %163 = add i32 %.sroa.0.sroa.0.0.ph, 127
  %164 = and i32 %163, 127
  br label %165

; <label>:165:                                    ; preds = %141, %162, %160, %158, %151
  %166 = phi i32 [ %157, %151 ], [ %159, %158 ], [ %161, %160 ], [ %164, %162 ], [ %145, %141 ]
  %167 = phi i8 [ 32, %151 ], [ 16, %158 ], [ 16, %160 ], [ 16, %162 ], [ 16, %141 ]
  %div = lshr exact i8 %167, 4
  %168 = zext i8 %div to i64
  %169 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %30, i64 %168)
  %170 = extractvalue { i64, i1 } %169, 0
  %171 = extractvalue { i64, i1 } %169, 1
  br i1 %171, label %203, label %.preheader229

.preheader229:                                    ; preds = %165
  %172 = mul i8 %167, -15
  %173 = add i8 %172, -16
  %174 = lshr exact i8 %173, 4
  %175 = add nuw nsw i8 %174, 1
  %xtraiter = and i8 %175, 7
  %lcmp.mod = icmp eq i8 %xtraiter, 0
  br i1 %lcmp.mod, label %.prol.loopexit, label %.prol.preheader

.prol.preheader:                                  ; preds = %.preheader229
  br label %176

; <label>:176:                                    ; preds = %176, %.prol.preheader
  %177 = phi i16 [ %182, %176 ], [ %31, %.prol.preheader ]
  %178 = phi i32 [ %183, %176 ], [ %166, %.prol.preheader ]
  %179 = phi i8 [ %181, %176 ], [ %167, %.prol.preheader ]
  %prol.iter = phi i8 [ %prol.iter.sub, %176 ], [ %xtraiter, %.prol.preheader ]
  %180 = trunc i32 %178 to i16
  %181 = add i8 %179, -16
  %182 = or i16 %177, %180
  %183 = lshr i32 %178, 16
  %prol.iter.sub = add i8 %prol.iter, -1
  %prol.iter.cmp = icmp eq i8 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %.prol.loopexit, label %176, !llvm.loop !9

.prol.loopexit:                                   ; preds = %176, %.preheader229
  %.lcssa237.unr = phi i16 [ undef, %.preheader229 ], [ %182, %176 ]
  %.unr = phi i16 [ %31, %.preheader229 ], [ %182, %176 ]
  %.unr262 = phi i32 [ %166, %.preheader229 ], [ %183, %176 ]
  %.unr263 = phi i8 [ %167, %.preheader229 ], [ %181, %176 ]
  %184 = icmp ult i8 %173, 112
  br i1 %184, label %.backedge.backedge, label %.preheader229.new

.preheader229.new:                                ; preds = %.prol.loopexit
  br label %185

; <label>:185:                                    ; preds = %185, %.preheader229.new
  %186 = phi i16 [ %.unr, %.preheader229.new ], [ %193, %185 ]
  %187 = phi i32 [ %.unr262, %.preheader229.new ], [ 0, %185 ]
  %188 = phi i8 [ %.unr263, %.preheader229.new ], [ %194, %185 ]
  %189 = trunc i32 %187 to i16
  %190 = or i16 %186, %189
  %191 = lshr i32 %187, 16
  %192 = trunc i32 %191 to i16
  %193 = or i16 %190, %192
  %194 = xor i8 %188, -128
  %195 = icmp eq i8 %194, 0
  br i1 %195, label %.backedge.backedge, label %185

.thread:                                          ; preds = %21, %107, %.lr.ph118, %._crit_edge119, %.loopexit74
  %196 = phi i64 [ %30, %.loopexit74 ], [ 0, %._crit_edge119 ], [ 0, %.lr.ph118 ], [ 0, %107 ], [ %22, %21 ]
  %197 = phi i16 [ %phitmp, %.loopexit74 ], [ 1, %._crit_edge119 ], [ 1, %.lr.ph118 ], [ 0, %107 ], [ 1, %21 ]
  %198 = phi i1 [ false, %.loopexit74 ], [ false, %._crit_edge119 ], [ false, %.lr.ph118 ], [ true, %107 ], [ false, %21 ]
  call void @llvm.lifetime.start.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.sroa.4)
  %temp-coercion.coerced.sroa.4.sroa.4.0..sroa_cast45 = bitcast i8* %temp-coercion.coerced.sroa.4.sroa.4 to i1*
  store i1 %198, i1* %temp-coercion.coerced.sroa.4.sroa.4.0..sroa_cast45, align 1
  %temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.8. = load i8, i8* %temp-coercion.coerced.sroa.4.sroa.4, align 1
  %temp-coercion.coerced.sroa.4.sroa.4.0.insert.ext = zext i8 %temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.sroa.4.0.temp-coercion.coerced.sroa.4.8. to i16
  %temp-coercion.coerced.sroa.4.sroa.4.0.insert.shift = shl nuw i16 %temp-coercion.coerced.sroa.4.sroa.4.0.insert.ext, 8
  %temp-coercion.coerced.sroa.4.sroa.0.0.insert.insert = or i16 %temp-coercion.coerced.sroa.4.sroa.4.0.insert.shift, %197
  call void @llvm.lifetime.end.p0i8(i64 1, i8* nonnull %temp-coercion.coerced.sroa.4.sroa.4)
  %199 = insertvalue { i64, i16 } undef, i64 %196, 0
  %200 = insertvalue { i64, i16 } %199, i16 %temp-coercion.coerced.sroa.4.sroa.0.0.insert.insert, 1
  ret { i64, i16 } %200

; <label>:201:                                    ; preds = %._crit_edge
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:202:                                    ; preds = %116
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable

; <label>:203:                                    ; preds = %165
  tail call void asm sideeffect "", "n"(i32 5) #8
  tail call void @llvm.trap()
  unreachable

; <label>:204:                                    ; preds = %147
  tail call void asm sideeffect "", "n"(i32 8) #8
  tail call void @llvm.trap()
  unreachable

; <label>:205:                                    ; preds = %42
  tail call void asm sideeffect "", "n"(i32 12) #8
  tail call void @llvm.trap()
  unreachable

; <label>:206:                                    ; preds = %._crit_edge119
  tail call void asm sideeffect "", "n"(i32 13) #8
  tail call void @llvm.trap()
  unreachable

; <label>:207:                                    ; preds = %18
  tail call void asm sideeffect "", "n"(i32 15) #8
  tail call void @llvm.trap()
  unreachable

.lr.ph222.1:                                      ; preds = %57
  %208 = load i8, i8* %61, align 1
  %209 = getelementptr inbounds i8, i8* %61, i64 1
  %210 = ptrtoint i8* %209 to i64
  %211 = zext i8 %208 to i32
  %212 = and i8 %68, 31
  %213 = zext i8 %212 to i32
  %214 = shl i32 %211, %213
  %215 = or i32 %214, %67
  %216 = add i8 %.sroa.13.2.ph159, 16
  %217 = icmp ult i8 %216, 32
  br i1 %217, label %218, label %.loopexit73

; <label>:218:                                    ; preds = %.lr.ph222.1
  %219 = icmp eq i8* %.pre-phi, %209
  %or.cond30.1 = and i1 %28, %219
  br i1 %or.cond30.1, label %.loopexit, label %.lr.ph222.2

.lr.ph222.2:                                      ; preds = %218
  %220 = load i8, i8* %209, align 1
  %221 = getelementptr inbounds i8, i8* %61, i64 2
  %222 = ptrtoint i8* %221 to i64
  %223 = zext i8 %220 to i32
  %224 = and i8 %216, 31
  %225 = zext i8 %224 to i32
  %226 = shl i32 %223, %225
  %227 = or i32 %226, %215
  %228 = add i8 %.sroa.13.2.ph159, 24
  %229 = icmp ult i8 %228, 32
  br i1 %229, label %230, label %.loopexit73

; <label>:230:                                    ; preds = %.lr.ph222.2
  %231 = icmp eq i8* %.pre-phi, %221
  %or.cond30.2 = and i1 %28, %231
  br i1 %or.cond30.2, label %.loopexit, label %.lr.ph222.3

.lr.ph222.3:                                      ; preds = %230
  %232 = load i8, i8* %221, align 1
  %233 = getelementptr inbounds i8, i8* %61, i64 3
  %234 = ptrtoint i8* %233 to i64
  %235 = zext i8 %232 to i32
  %236 = and i8 %228, 31
  %237 = zext i8 %236 to i32
  %238 = shl i32 %235, %237
  %239 = or i32 %238, %227
  %240 = add i8 %.sroa.13.2.ph159, 32
  %241 = icmp ugt i8 %.sroa.13.2.ph159, -33
  br i1 %241, label %242, label %.loopexit73

; <label>:242:                                    ; preds = %.lr.ph222.3
  %243 = icmp eq i8* %.pre-phi, %233
  %or.cond30.3 = and i1 %28, %243
  br i1 %or.cond30.3, label %.loopexit, label %.lr.ph222.4

.lr.ph222.4:                                      ; preds = %242
  %244 = load i8, i8* %233, align 1
  %245 = getelementptr inbounds i8, i8* %61, i64 4
  %246 = ptrtoint i8* %245 to i64
  %247 = zext i8 %244 to i32
  %248 = and i8 %240, 31
  %249 = zext i8 %248 to i32
  %250 = shl i32 %247, %249
  %251 = or i32 %250, %239
  %252 = add i8 %.sroa.13.2.ph159, 40
  %253 = icmp ult i8 %252, 32
  br i1 %253, label %.loopexit, label %.loopexit73
}

define linkonce_odr hidden swiftcc %swift.refcounted* @_T0s13_StringBufferVABSi8capacity_Si11initialSizeSi12elementWidthtcfCTf4nnnd_n(i64, i64, i64) local_unnamed_addr #1 {
entry:
  %3 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %2, i64 1)
  %4 = extractvalue { i64, i1 } %3, 0
  %5 = sub i64 1, %4
  %6 = extractvalue { i64, i1 } %3, 1
  br i1 %6, label %55, label %7

; <label>:7:                                      ; preds = %entry
  %8 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %0, i64 %5)
  %9 = extractvalue { i64, i1 } %8, 1
  br i1 %9, label %56, label %10

; <label>:10:                                     ; preds = %7
  %11 = extractvalue { i64, i1 } %8, 0
  %12 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %11, i64 %5)
  %13 = extractvalue { i64, i1 } %12, 1
  br i1 %13, label %57, label %14

; <label>:14:                                     ; preds = %10
  %15 = extractvalue { i64, i1 } %12, 0
  %16 = and i64 %5, 63
  %17 = ashr i64 %15, %16
  %18 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %17, i64 2)
  %19 = extractvalue { i64, i1 } %18, 1
  br i1 %19, label %58, label %20

; <label>:20:                                     ; preds = %14
  %21 = extractvalue { i64, i1 } %18, 0
  %22 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %21, i64 32)
  %23 = extractvalue { i64, i1 } %22, 1
  br i1 %23, label %59, label %.critedge4

.critedge4:                                       ; preds = %20
  %24 = extractvalue { i64, i1 } %22, 0
  %25 = load %swift.type*, %swift.type** @_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGML, align 8
  %26 = icmp eq %swift.type* %25, null
  br i1 %26, label %cacheIsNull.i, label %_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGMa.exit

cacheIsNull.i:                                    ; preds = %.critedge4
  %27 = load %swift.type*, %swift.type** @_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGML, align 8
  %28 = icmp eq %swift.type* %27, null
  br i1 %28, label %cacheIsNull.i.i, label %_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGMa.exit.i

cacheIsNull.i.i:                                  ; preds = %cacheIsNull.i
  %29 = tail call %swift.type* @_T0s17_HeapBufferHeaderVMa(%swift.type* nonnull @_T0s18_StringBufferIVarsVN) #11
  store atomic %swift.type* %29, %swift.type** @_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGML release, align 8
  br label %_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGMa.exit.i

_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGMa.exit.i: ; preds = %cacheIsNull.i.i, %cacheIsNull.i
  %30 = phi %swift.type* [ %27, %cacheIsNull.i ], [ %29, %cacheIsNull.i.i ]
  %31 = tail call %swift.type* @_T0s13ManagedBufferCMa(%swift.type* %30, %swift.type* nonnull @_T0s6UInt16VN) #11
  store atomic %swift.type* %31, %swift.type** @_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGML release, align 8
  br label %_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGMa.exit

_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGMa.exit: ; preds = %.critedge4, %_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGMa.exit.i
  %32 = phi %swift.type* [ %25, %.critedge4 ], [ %31, %_T0s17_HeapBufferHeaderVys07_StringB5IVarsVGMa.exit.i ]
  %33 = tail call swiftcc %swift.refcounted* @swift_bufferAllocate(%swift.type* %32, i64 %24, i64 7)
  %34 = bitcast %swift.refcounted* %33 to i8*
  %35 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %33, i64 1
  %36 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %33, i64 2
  %37 = bitcast %swift.refcounted* %36 to i8*
  %38 = bitcast %swift.refcounted* %35 to i64*
  store i64 0, i64* %38, align 8
  %.value.capacityAndElementShift = getelementptr inbounds %swift.refcounted, %swift.refcounted* %33, i64 1, i32 1
  store i64 %4, i64* %.value.capacityAndElementShift, align 8
  %39 = tail call i64 @_stdlib_malloc_size(i8* %34)
  %40 = add i64 %39, -32
  %41 = and i64 %4, 63
  %42 = shl i64 %1, %41
  %43 = getelementptr inbounds i8, i8* %37, i64 %42
  %44 = ptrtoint i8* %43 to i64
  %45 = sdiv i64 %40, 2
  %46 = tail call { i64, i1 } @llvm.ssub.with.overflow.i64(i64 %45, i64 %5)
  %47 = extractvalue { i64, i1 } %46, 1
  br i1 %47, label %60, label %48

; <label>:48:                                     ; preds = %_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGMa.exit
  %49 = extractvalue { i64, i1 } %46, 0
  %50 = shl i64 %49, 1
  %51 = tail call { i64, i1 } @llvm.sadd.with.overflow.i64(i64 %50, i64 %4)
  %52 = extractvalue { i64, i1 } %51, 1
  br i1 %52, label %61, label %53

; <label>:53:                                     ; preds = %48
  %54 = extractvalue { i64, i1 } %51, 0
  store i64 %44, i64* %38, align 8
  store i64 %54, i64* %.value.capacityAndElementShift, align 8
  ret %swift.refcounted* %33

; <label>:55:                                     ; preds = %entry
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

; <label>:56:                                     ; preds = %7
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable

; <label>:57:                                     ; preds = %10
  tail call void asm sideeffect "", "n"(i32 2) #8
  tail call void @llvm.trap()
  unreachable

; <label>:58:                                     ; preds = %14
  tail call void asm sideeffect "", "n"(i32 6) #8
  tail call void @llvm.trap()
  unreachable

; <label>:59:                                     ; preds = %20
  tail call void asm sideeffect "", "n"(i32 7) #8
  tail call void @llvm.trap()
  unreachable

; <label>:60:                                     ; preds = %_T0s13ManagedBufferCys05_HeapB6HeaderVys07_StringB5IVarsVGs6UInt16VGMa.exit
  tail call void asm sideeffect "", "n"(i32 9) #8
  tail call void @llvm.trap()
  unreachable

; <label>:61:                                     ; preds = %48
  tail call void asm sideeffect "", "n"(i32 10) #8
  tail call void @llvm.trap()
  unreachable
}

define linkonce_odr hidden swiftcc { i64, i64, i64, i8 } @_T0SS21_fromCodeUnitSequenceSSSgxm_q_5inputts16_UnicodeEncodingRzs10CollectionR_7ElementQy_0bC0Rtzr0_lFZs0F0O4UTF8O_SRys5UInt8VGTgq5Tf4nxd_n(%swift.type*, i64, i64) local_unnamed_addr #1 {
entry:
  %3 = tail call swiftcc { i64, i16 } @_T0s7UnicodeO5UTF16O16transcodedLengthSi5count_Sb7isASCIItSgx2of_q_m9decodedAsSb27repairingIllFormedSequencests16IteratorProtocolRzs01_A8EncodingR_8CodeUnitQy_7ElementRtzr0_lFZs019UnsafeBufferPointerO0Vys5UInt8VG_AB4UTF8OTgq5Tf4xnnd_n(i64 %1, i64 %2, %swift.type* %0, i1 false)
  %4 = extractvalue { i64, i16 } %3, 0
  %5 = extractvalue { i64, i16 } %3, 1
  %6 = and i16 %5, 256
  %7 = icmp eq i16 %6, 0
  br i1 %7, label %8, label %262

; <label>:8:                                      ; preds = %entry
  %9 = and i16 %5, 1
  %10 = icmp eq i16 %9, 0
  %11 = icmp sgt i64 %4, 0
  %. = select i1 %11, i64 %4, i64 0
  %12 = zext i16 %9 to i64
  %13 = sub nsw i64 2, %12
  %14 = tail call swiftcc %swift.refcounted* @_T0s13_StringBufferVABSi8capacity_Si11initialSizeSi12elementWidthtcfCTf4nnnd_n(i64 %., i64 %4, i64 %13)
  %15 = getelementptr inbounds %swift.refcounted, %swift.refcounted* %14, i64 2
  %16 = bitcast %swift.refcounted* %15 to i8*
  %17 = icmp ne i64 %2, 0
  %18 = inttoptr i64 %2 to i8*
  %19 = icmp eq i64 %2, 0
  br i1 %10, label %.preheader230, label %.preheader231

.preheader231:                                    ; preds = %8
  br label %178

.preheader230:                                    ; preds = %8
  br label %20

; <label>:20:                                     ; preds = %.preheader230, %.unr-lcssa
  %.sroa.12104.0 = phi i8 [ %.sroa.12104.3, %.unr-lcssa ], [ 0, %.preheader230 ]
  %.sroa.098.0 = phi i32 [ %.sroa.098.3, %.unr-lcssa ], [ 0, %.preheader230 ]
  %.sroa.079.0 = phi i64 [ %.sroa.079.3, %.unr-lcssa ], [ %1, %.preheader230 ]
  %21 = phi i8* [ %scevgep184, %.unr-lcssa ], [ %16, %.preheader230 ]
  %22 = icmp eq i8 %.sroa.12104.0, 0
  br i1 %22, label %30, label %23, !prof !5

; <label>:23:                                     ; preds = %20
  %24 = trunc i32 %.sroa.098.0 to i8
  %25 = icmp sgt i8 %24, -1
  br i1 %25, label %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit, label %.preheader

_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit: ; preds = %23
  %26 = lshr i32 %.sroa.098.0, 8
  %27 = add i8 %.sroa.12104.0, -8
  %28 = add i32 %.sroa.098.0, 1
  %29 = and i32 %28, 255
  br label %98

; <label>:30:                                     ; preds = %20
  %31 = icmp eq i64 %.sroa.079.0, 0
  br i1 %31, label %32, label %33

; <label>:32:                                     ; preds = %30
  br i1 %19, label %.loopexit117, label %318

; <label>:33:                                     ; preds = %30
  %34 = inttoptr i64 %.sroa.079.0 to i8*
  %35 = icmp eq i8* %34, %18
  %or.cond51 = and i1 %17, %35
  br i1 %or.cond51, label %.loopexit117, label %36

; <label>:36:                                     ; preds = %33
  %37 = inttoptr i64 %.sroa.079.0 to %Ts5UInt8V*
  %._value13 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %37, i64 0, i32 0
  %38 = load i8, i8* %._value13, align 1
  %39 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %37, i64 1, i32 0
  %40 = ptrtoint i8* %39 to i64
  %41 = icmp sgt i8 %38, -1
  br i1 %41, label %95, label %.preheader.thread

.preheader.thread:                                ; preds = %36
  %42 = zext i8 %38 to i32
  %43 = or i32 %.sroa.098.0, %42
  br label %.lr.ph

.preheader:                                       ; preds = %23
  %44 = icmp eq i64 %.sroa.079.0, 0
  br i1 %44, label %._crit_edge, label %.lr.ph

.lr.ph:                                           ; preds = %.preheader.thread, %.preheader
  %.sroa.079.1.ph187 = phi i64 [ %40, %.preheader.thread ], [ %.sroa.079.0, %.preheader ]
  %.sroa.098.1.ph186 = phi i32 [ %43, %.preheader.thread ], [ %.sroa.098.0, %.preheader ]
  %.sroa.12104.1.ph185 = phi i8 [ 8, %.preheader.thread ], [ %.sroa.12104.0, %.preheader ]
  %45 = inttoptr i64 %.sroa.079.1.ph187 to i8*
  %46 = icmp eq i8* %45, %18
  %or.cond53215 = and i1 %17, %46
  br i1 %or.cond53215, label %.loopexit, label %.lr.ph219

._crit_edge:                                      ; preds = %.preheader
  br i1 %19, label %.loopexit116, label %316

; <label>:47:                                     ; preds = %.lr.ph219
  %48 = icmp eq i8* %51, %18
  %or.cond53 = and i1 %17, %48
  br i1 %or.cond53, label %.loopexit, label %.lr.ph219.1

.lr.ph219:                                        ; preds = %.lr.ph
  %49 = inttoptr i64 %.sroa.079.1.ph187 to %Ts5UInt8V*
  %._value6 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %49, i64 0, i32 0
  %50 = load i8, i8* %._value6, align 1
  %51 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %49, i64 1, i32 0
  %52 = ptrtoint i8* %51 to i64
  %53 = zext i8 %50 to i32
  %54 = and i8 %.sroa.12104.1.ph185, 31
  %55 = zext i8 %54 to i32
  %56 = shl i32 %53, %55
  %57 = or i32 %56, %.sroa.098.1.ph186
  %58 = add i8 %.sroa.12104.1.ph185, 8
  %59 = icmp ult i8 %58, 32
  br i1 %59, label %47, label %.loopexit116

.loopexit:                                        ; preds = %.lr.ph219.4, %47, %332, %344, %356, %.lr.ph
  %.sroa.079.1148.lcssa = phi i64 [ %.sroa.079.1.ph187, %.lr.ph ], [ %52, %47 ], [ %324, %332 ], [ %336, %344 ], [ %348, %356 ], [ %360, %.lr.ph219.4 ]
  %.sroa.098.1147.lcssa = phi i32 [ %.sroa.098.1.ph186, %.lr.ph ], [ %57, %47 ], [ %329, %332 ], [ %341, %344 ], [ %353, %356 ], [ %365, %.lr.ph219.4 ]
  %.sroa.12104.1146.lcssa = phi i8 [ %.sroa.12104.1.ph185, %.lr.ph ], [ %58, %47 ], [ %330, %332 ], [ %342, %344 ], [ %354, %356 ], [ %366, %.lr.ph219.4 ]
  %60 = icmp eq i8 %.sroa.12104.1146.lcssa, 0
  br i1 %60, label %.loopexit117, label %.loopexit116

.loopexit116:                                     ; preds = %.lr.ph219, %.lr.ph219.1, %.lr.ph219.2, %.lr.ph219.3, %.lr.ph219.4, %._crit_edge, %.loopexit
  %.sroa.12104.2 = phi i8 [ %.sroa.12104.1146.lcssa, %.loopexit ], [ %.sroa.12104.0, %._crit_edge ], [ %58, %.lr.ph219 ], [ %330, %.lr.ph219.1 ], [ %342, %.lr.ph219.2 ], [ %354, %.lr.ph219.3 ], [ %366, %.lr.ph219.4 ]
  %.sroa.098.2 = phi i32 [ %.sroa.098.1147.lcssa, %.loopexit ], [ %.sroa.098.0, %._crit_edge ], [ %57, %.lr.ph219 ], [ %329, %.lr.ph219.1 ], [ %341, %.lr.ph219.2 ], [ %353, %.lr.ph219.3 ], [ %365, %.lr.ph219.4 ]
  %.sroa.079.2 = phi i64 [ %.sroa.079.1148.lcssa, %.loopexit ], [ 0, %._crit_edge ], [ %52, %.lr.ph219 ], [ %324, %.lr.ph219.1 ], [ %336, %.lr.ph219.2 ], [ %348, %.lr.ph219.3 ], [ %360, %.lr.ph219.4 ]
  %61 = and i32 %.sroa.098.2, 49376
  %62 = icmp eq i32 %61, 32960
  br i1 %62, label %77, label %63

; <label>:63:                                     ; preds = %.loopexit116
  %64 = and i32 %.sroa.098.2, 12632304
  %65 = icmp eq i32 %64, 8421600
  br i1 %65, label %75, label %66

; <label>:66:                                     ; preds = %63
  %67 = and i32 %.sroa.098.2, -1061109512
  %68 = icmp eq i32 %67, -2139062032
  br i1 %68, label %69, label %80

; <label>:69:                                     ; preds = %66
  %70 = trunc i32 %.sroa.098.2 to i16
  %71 = and i16 %70, 12295
  %72 = icmp eq i16 %71, 0
  %73 = tail call i16 @llvm.bswap.i16(i16 %71)
  %74 = icmp ugt i16 %73, 1024
  %or.cond = or i1 %72, %74
  br i1 %or.cond, label %80, label %82, !prof !6

; <label>:75:                                     ; preds = %63
  %76 = and i32 %.sroa.098.2, 8207
  %trunc115 = trunc i32 %76 to i14
  switch i14 %trunc115, label %82 [
    i14 0, label %80
    i14 -8179, label %80
  ], !prof !4

; <label>:77:                                     ; preds = %.loopexit116
  %78 = and i32 %.sroa.098.2, 30
  %79 = icmp eq i32 %78, 0
  br i1 %79, label %80, label %82, !prof !7

; <label>:80:                                     ; preds = %75, %75, %77, %69, %66
  %81 = tail call swiftcc i8 @_T0s7UnicodeO4UTF8O13ForwardParserV14_invalidLengths5UInt8VyFTf4x_n(i32 %.sroa.098.2)
  br label %.loopexit117

; <label>:82:                                     ; preds = %77, %69, %75
  %.ph = phi i8 [ 24, %75 ], [ 32, %69 ], [ 16, %77 ]
  %83 = zext i32 %.sroa.098.2 to i64
  %84 = zext i8 %.ph to i64
  %85 = lshr i64 %83, %84
  %86 = trunc i64 %85 to i32
  %87 = sub i8 %.sroa.12104.2, %.ph
  %88 = add i32 %.sroa.098.2, 16843009
  %89 = lshr exact i8 %.ph, 1
  %90 = zext i8 %89 to i32
  %91 = shl i32 -1, %90
  %92 = shl i32 %91, %90
  %93 = xor i32 %92, -1
  %94 = and i32 %88, %93
  br label %98

; <label>:95:                                     ; preds = %36
  %96 = add i8 %38, 1
  %97 = zext i8 %96 to i32
  br label %98

; <label>:98:                                     ; preds = %82, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit, %95
  %.sroa.12104.3 = phi i8 [ 0, %95 ], [ %87, %82 ], [ %27, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ]
  %.sroa.098.3 = phi i32 [ %.sroa.098.0, %95 ], [ %86, %82 ], [ %26, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ]
  %.sroa.074.sroa.0.0 = phi i32 [ %97, %95 ], [ %94, %82 ], [ %29, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ]
  %.sroa.079.3 = phi i64 [ %40, %95 ], [ %.sroa.079.2, %82 ], [ %.sroa.079.0, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit ]
  %99 = tail call i32 @llvm.ctlz.i32(i32 %.sroa.074.sroa.0.0, i1 false), !range !8
  %100 = zext i32 %99 to i64
  %101 = lshr i64 %100, 3
  %102 = sub nsw i64 4, %101
  %103 = icmp eq i64 %102, 1
  br i1 %103, label %138, label %104, !prof !5

; <label>:104:                                    ; preds = %98
  %105 = add i32 %.sroa.074.sroa.0.0, -16843009
  %106 = shl i32 %105, 6
  %107 = lshr i32 %105, 8
  %108 = and i32 %107, 63
  %109 = or i32 %108, %106
  %110 = icmp eq i64 %102, 2
  br i1 %110, label %136, label %111, !prof !5

; <label>:111:                                    ; preds = %104
  %112 = shl i32 %109, 6
  %113 = lshr i32 %105, 16
  %114 = and i32 %113, 63
  %115 = or i32 %112, %114
  %116 = icmp eq i64 %102, 3
  br i1 %116, label %134, label %117, !prof !5

; <label>:117:                                    ; preds = %111
  %118 = shl i32 %115, 6
  %119 = lshr i32 %105, 24
  %120 = and i32 %119, 63
  %.masked = and i32 %118, 2097088
  %121 = or i32 %.masked, %120
  %122 = icmp ult i32 %121, 65536
  br i1 %122, label %141, label %123, !prof !5

; <label>:123:                                    ; preds = %117
  %124 = tail call { i32, i1 } @llvm.usub.with.overflow.i32(i32 %121, i32 65536)
  %125 = extractvalue { i32, i1 } %124, 0
  %126 = extractvalue { i32, i1 } %124, 1
  br i1 %126, label %317, label %127

; <label>:127:                                    ; preds = %123
  %128 = lshr i32 %125, 10
  %129 = and i32 %128, 1023
  %130 = shl i32 %125, 16
  %131 = and i32 %130, 67043328
  %132 = or i32 %129, %131
  %133 = or i32 %132, -603924480
  br label %141

; <label>:134:                                    ; preds = %111
  %135 = and i32 %115, 65535
  br label %141

; <label>:136:                                    ; preds = %104
  %137 = and i32 %109, 2047
  br label %141

; <label>:138:                                    ; preds = %98
  %139 = add i32 %.sroa.074.sroa.0.0, 127
  %140 = and i32 %139, 127
  br label %141

; <label>:141:                                    ; preds = %127, %134, %136, %138, %117
  %142 = phi i32 [ %133, %127 ], [ %135, %134 ], [ %137, %136 ], [ %140, %138 ], [ %121, %117 ]
  %143 = phi i8 [ 32, %127 ], [ 16, %134 ], [ 16, %136 ], [ 16, %138 ], [ 16, %117 ]
  %144 = mul i8 %143, -15
  %145 = add i8 %144, -16
  %146 = lshr exact i8 %145, 3
  %147 = zext i8 %146 to i64
  %148 = mul i8 %143, -15
  %149 = add i8 %148, -16
  %150 = lshr exact i8 %149, 4
  %151 = add nuw nsw i8 %150, 1
  %xtraiter = and i8 %151, 7
  %lcmp.mod = icmp eq i8 %xtraiter, 0
  br i1 %lcmp.mod, label %.prol.loopexit, label %.prol.preheader

.prol.preheader:                                  ; preds = %141
  br label %152

; <label>:152:                                    ; preds = %152, %.prol.preheader
  %153 = phi i32 [ %142, %.prol.preheader ], [ %159, %152 ]
  %154 = phi i8 [ %143, %.prol.preheader ], [ %157, %152 ]
  %155 = phi i8* [ %21, %.prol.preheader ], [ %158, %152 ]
  %prol.iter = phi i8 [ %xtraiter, %.prol.preheader ], [ %prol.iter.sub, %152 ]
  %156 = trunc i32 %153 to i16
  %157 = add i8 %154, -16
  %._value.prol = bitcast i8* %155 to i16*
  store i16 %156, i16* %._value.prol, align 2
  %158 = getelementptr inbounds i8, i8* %155, i64 2
  %159 = lshr i32 %153, 16
  %prol.iter.sub = add i8 %prol.iter, -1
  %prol.iter.cmp = icmp eq i8 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %.prol.loopexit, label %152, !llvm.loop !11

.prol.loopexit:                                   ; preds = %152, %141
  %.unr = phi i32 [ %142, %141 ], [ %159, %152 ]
  %.unr244 = phi i8 [ %143, %141 ], [ %157, %152 ]
  %.unr245 = phi i8* [ %21, %141 ], [ %158, %152 ]
  %160 = icmp ult i8 %149, 112
  br i1 %160, label %.unr-lcssa, label %.new

.new:                                             ; preds = %.prol.loopexit
  br label %161

; <label>:161:                                    ; preds = %161, %.new
  %162 = phi i32 [ %.unr, %.new ], [ 0, %161 ]
  %163 = phi i8 [ %.unr244, %.new ], [ %175, %161 ]
  %164 = phi i8* [ %.unr245, %.new ], [ %176, %161 ]
  %165 = trunc i32 %162 to i16
  %._value = bitcast i8* %164 to i16*
  store i16 %165, i16* %._value, align 2
  %166 = getelementptr inbounds i8, i8* %164, i64 2
  %167 = lshr i32 %162, 16
  %168 = trunc i32 %167 to i16
  %._value.1 = bitcast i8* %166 to i16*
  store i16 %168, i16* %._value.1, align 2
  %169 = getelementptr inbounds i8, i8* %164, i64 4
  %._value.2 = bitcast i8* %169 to i16*
  store i16 0, i16* %._value.2, align 2
  %170 = getelementptr inbounds i8, i8* %164, i64 6
  %._value.3 = bitcast i8* %170 to i16*
  store i16 0, i16* %._value.3, align 2
  %171 = getelementptr inbounds i8, i8* %164, i64 8
  %._value.4 = bitcast i8* %171 to i16*
  store i16 0, i16* %._value.4, align 2
  %172 = getelementptr inbounds i8, i8* %164, i64 10
  %._value.5 = bitcast i8* %172 to i16*
  store i16 0, i16* %._value.5, align 2
  %173 = getelementptr inbounds i8, i8* %164, i64 12
  %._value.6 = bitcast i8* %173 to i16*
  store i16 0, i16* %._value.6, align 2
  %174 = getelementptr inbounds i8, i8* %164, i64 14
  %175 = xor i8 %163, -128
  %._value.7 = bitcast i8* %174 to i16*
  store i16 0, i16* %._value.7, align 2
  %176 = getelementptr inbounds i8, i8* %164, i64 16
  %177 = icmp eq i8 %175, 0
  br i1 %177, label %.unr-lcssa, label %161

.unr-lcssa:                                       ; preds = %161, %.prol.loopexit
  %scevgep = getelementptr i8, i8* %21, i64 2
  %scevgep184 = getelementptr i8, i8* %scevgep, i64 %147
  br label %20

; <label>:178:                                    ; preds = %.preheader231, %313
  %.sroa.12.0 = phi i8 [ %.sroa.12.3, %313 ], [ 0, %.preheader231 ]
  %.sroa.0.0110 = phi i32 [ %.sroa.0.3113, %313 ], [ 0, %.preheader231 ]
  %.sroa.0.0 = phi i64 [ %.sroa.0.3, %313 ], [ %1, %.preheader231 ]
  %179 = phi i8* [ %315, %313 ], [ %16, %.preheader231 ]
  %180 = icmp eq i8 %.sroa.12.0, 0
  br i1 %180, label %188, label %181, !prof !5

; <label>:181:                                    ; preds = %178
  %182 = trunc i32 %.sroa.0.0110 to i8
  %183 = icmp sgt i8 %182, -1
  br i1 %183, label %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88, label %.preheader118

_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88: ; preds = %181
  %184 = lshr i32 %.sroa.0.0110, 8
  %185 = add i8 %.sroa.12.0, -8
  %186 = add i32 %.sroa.0.0110, 1
  %187 = and i32 %186, 255
  br label %271

; <label>:188:                                    ; preds = %178
  %189 = icmp eq i64 %.sroa.0.0, 0
  br i1 %189, label %190, label %191

; <label>:190:                                    ; preds = %188
  br i1 %19, label %.loopexit117, label %321

; <label>:191:                                    ; preds = %188
  %192 = inttoptr i64 %.sroa.0.0 to i8*
  %193 = icmp eq i8* %192, %18
  %or.cond57 = and i1 %17, %193
  br i1 %or.cond57, label %.loopexit117, label %194

; <label>:194:                                    ; preds = %191
  %195 = inttoptr i64 %.sroa.0.0 to %Ts5UInt8V*
  %._value37 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %195, i64 0, i32 0
  %196 = load i8, i8* %._value37, align 1
  %197 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %195, i64 1, i32 0
  %198 = ptrtoint i8* %197 to i64
  %199 = icmp sgt i8 %196, -1
  br i1 %199, label %253, label %.preheader118.thread

.preheader118.thread:                             ; preds = %194
  %200 = zext i8 %196 to i32
  %201 = or i32 %.sroa.0.0110, %200
  br label %.lr.ph154

.preheader118:                                    ; preds = %181
  %202 = icmp eq i64 %.sroa.0.0, 0
  br i1 %202, label %._crit_edge155, label %.lr.ph154

.lr.ph154:                                        ; preds = %.preheader118.thread, %.preheader118
  %.sroa.0.1.ph190 = phi i64 [ %198, %.preheader118.thread ], [ %.sroa.0.0, %.preheader118 ]
  %.sroa.0.1111.ph189 = phi i32 [ %201, %.preheader118.thread ], [ %.sroa.0.0110, %.preheader118 ]
  %.sroa.12.1.ph188 = phi i8 [ 8, %.preheader118.thread ], [ %.sroa.12.0, %.preheader118 ]
  %203 = inttoptr i64 %.sroa.0.1.ph190 to i8*
  %204 = icmp eq i8* %203, %18
  %or.cond59222 = and i1 %17, %204
  br i1 %or.cond59222, label %.loopexit119, label %.lr.ph226

._crit_edge155:                                   ; preds = %.preheader118
  br i1 %19, label %.loopexit120, label %319

; <label>:205:                                    ; preds = %.lr.ph226
  %206 = icmp eq i8* %209, %18
  %or.cond59 = and i1 %17, %206
  br i1 %or.cond59, label %.loopexit119, label %.lr.ph226.1

.lr.ph226:                                        ; preds = %.lr.ph154
  %207 = inttoptr i64 %.sroa.0.1.ph190 to %Ts5UInt8V*
  %._value29 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %207, i64 0, i32 0
  %208 = load i8, i8* %._value29, align 1
  %209 = getelementptr inbounds %Ts5UInt8V, %Ts5UInt8V* %207, i64 1, i32 0
  %210 = ptrtoint i8* %209 to i64
  %211 = zext i8 %208 to i32
  %212 = and i8 %.sroa.12.1.ph188, 31
  %213 = zext i8 %212 to i32
  %214 = shl i32 %211, %213
  %215 = or i32 %214, %.sroa.0.1111.ph189
  %216 = add i8 %.sroa.12.1.ph188, 8
  %217 = icmp ult i8 %216, 32
  br i1 %217, label %205, label %.loopexit120

.loopexit119:                                     ; preds = %.lr.ph226.4, %205, %378, %390, %402, %.lr.ph154
  %.sroa.0.1153.lcssa = phi i64 [ %.sroa.0.1.ph190, %.lr.ph154 ], [ %210, %205 ], [ %370, %378 ], [ %382, %390 ], [ %394, %402 ], [ %406, %.lr.ph226.4 ]
  %.sroa.0.1111152.lcssa = phi i32 [ %.sroa.0.1111.ph189, %.lr.ph154 ], [ %215, %205 ], [ %375, %378 ], [ %387, %390 ], [ %399, %402 ], [ %411, %.lr.ph226.4 ]
  %.sroa.12.1151.lcssa = phi i8 [ %.sroa.12.1.ph188, %.lr.ph154 ], [ %216, %205 ], [ %376, %378 ], [ %388, %390 ], [ %400, %402 ], [ %412, %.lr.ph226.4 ]
  %218 = icmp eq i8 %.sroa.12.1151.lcssa, 0
  br i1 %218, label %.loopexit117, label %.loopexit120

.loopexit120:                                     ; preds = %.lr.ph226, %.lr.ph226.1, %.lr.ph226.2, %.lr.ph226.3, %.lr.ph226.4, %._crit_edge155, %.loopexit119
  %.sroa.12.2 = phi i8 [ %.sroa.12.1151.lcssa, %.loopexit119 ], [ %.sroa.12.0, %._crit_edge155 ], [ %216, %.lr.ph226 ], [ %376, %.lr.ph226.1 ], [ %388, %.lr.ph226.2 ], [ %400, %.lr.ph226.3 ], [ %412, %.lr.ph226.4 ]
  %.sroa.0.2112 = phi i32 [ %.sroa.0.1111152.lcssa, %.loopexit119 ], [ %.sroa.0.0110, %._crit_edge155 ], [ %215, %.lr.ph226 ], [ %375, %.lr.ph226.1 ], [ %387, %.lr.ph226.2 ], [ %399, %.lr.ph226.3 ], [ %411, %.lr.ph226.4 ]
  %.sroa.0.2 = phi i64 [ %.sroa.0.1153.lcssa, %.loopexit119 ], [ 0, %._crit_edge155 ], [ %210, %.lr.ph226 ], [ %370, %.lr.ph226.1 ], [ %382, %.lr.ph226.2 ], [ %394, %.lr.ph226.3 ], [ %406, %.lr.ph226.4 ]
  %219 = and i32 %.sroa.0.2112, 49376
  %220 = icmp eq i32 %219, 32960
  br i1 %220, label %235, label %221

; <label>:221:                                    ; preds = %.loopexit120
  %222 = and i32 %.sroa.0.2112, 12632304
  %223 = icmp eq i32 %222, 8421600
  br i1 %223, label %233, label %224

; <label>:224:                                    ; preds = %221
  %225 = and i32 %.sroa.0.2112, -1061109512
  %226 = icmp eq i32 %225, -2139062032
  br i1 %226, label %227, label %238

; <label>:227:                                    ; preds = %224
  %228 = trunc i32 %.sroa.0.2112 to i16
  %229 = and i16 %228, 12295
  %230 = icmp eq i16 %229, 0
  %231 = tail call i16 @llvm.bswap.i16(i16 %229)
  %232 = icmp ugt i16 %231, 1024
  %or.cond85 = or i1 %230, %232
  br i1 %or.cond85, label %238, label %240, !prof !6

; <label>:233:                                    ; preds = %221
  %234 = and i32 %.sroa.0.2112, 8207
  %trunc = trunc i32 %234 to i14
  switch i14 %trunc, label %240 [
    i14 0, label %238
    i14 -8179, label %238
  ], !prof !4

; <label>:235:                                    ; preds = %.loopexit120
  %236 = and i32 %.sroa.0.2112, 30
  %237 = icmp eq i32 %236, 0
  br i1 %237, label %238, label %240, !prof !7

; <label>:238:                                    ; preds = %233, %233, %235, %227, %224
  %239 = tail call swiftcc i8 @_T0s7UnicodeO4UTF8O13ForwardParserV14_invalidLengths5UInt8VyFTf4x_n(i32 %.sroa.0.2112)
  br label %.loopexit117

; <label>:240:                                    ; preds = %235, %227, %233
  %.ph114 = phi i8 [ 24, %233 ], [ 32, %227 ], [ 16, %235 ]
  %241 = zext i32 %.sroa.0.2112 to i64
  %242 = zext i8 %.ph114 to i64
  %243 = lshr i64 %241, %242
  %244 = trunc i64 %243 to i32
  %245 = sub i8 %.sroa.12.2, %.ph114
  %246 = add i32 %.sroa.0.2112, 16843009
  %247 = lshr exact i8 %.ph114, 1
  %248 = zext i8 %247 to i32
  %249 = shl i32 -1, %248
  %250 = shl i32 %249, %248
  %251 = xor i32 %250, -1
  %252 = and i32 %246, %251
  br label %271

; <label>:253:                                    ; preds = %194
  %254 = add i8 %196, 1
  %255 = zext i8 %254 to i32
  br label %271

.loopexit117:                                     ; preds = %191, %.loopexit119, %33, %.loopexit, %190, %238, %32, %80
  %256 = tail call swiftcc i64 @_T0s13_StringBufferV9usedCountSivg(%swift.refcounted* %14)
  %257 = ptrtoint %swift.refcounted* %15 to i64
  %.value.capacityAndElementShift = getelementptr inbounds %swift.refcounted, %swift.refcounted* %14, i64 1, i32 1
  %258 = load i64, i64* %.value.capacityAndElementShift, align 8
  %259 = ptrtoint %swift.refcounted* %14 to i64
  %260 = shl i64 %258, 63
  %261 = or i64 %260, %256
  br label %262

; <label>:262:                                    ; preds = %entry, %.loopexit117
  %263 = phi i64 [ %257, %.loopexit117 ], [ 0, %entry ]
  %264 = phi i64 [ %261, %.loopexit117 ], [ 0, %entry ]
  %265 = phi i64 [ %259, %.loopexit117 ], [ 0, %entry ]
  %266 = phi i8 [ 0, %.loopexit117 ], [ 1, %entry ]
  %267 = insertvalue { i64, i64, i64, i8 } undef, i64 %263, 0
  %268 = insertvalue { i64, i64, i64, i8 } %267, i64 %264, 1
  %269 = insertvalue { i64, i64, i64, i8 } %268, i64 %265, 2
  %270 = insertvalue { i64, i64, i64, i8 } %269, i8 %266, 3
  ret { i64, i64, i64, i8 } %270

; <label>:271:                                    ; preds = %240, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88, %253
  %.sroa.12.3 = phi i8 [ 0, %253 ], [ %245, %240 ], [ %185, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88 ]
  %.sroa.0.3113 = phi i32 [ %.sroa.0.0110, %253 ], [ %244, %240 ], [ %184, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88 ]
  %.sroa.0.3 = phi i64 [ %198, %253 ], [ %.sroa.0.2, %240 ], [ %.sroa.0.0, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88 ]
  %.sroa.0.sroa.0.0 = phi i32 [ %255, %253 ], [ %252, %240 ], [ %187, %_T0s26RangeReplaceableCollectionPsE6remove7ElementQz5IndexQz2at_tFs11_UIntBufferVys6UInt32Vs5UInt8VG_Tgq5.exit88 ]
  %272 = tail call i32 @llvm.ctlz.i32(i32 %.sroa.0.sroa.0.0, i1 false), !range !8
  %273 = zext i32 %272 to i64
  %274 = lshr i64 %273, 3
  %275 = sub nsw i64 4, %274
  switch i64 %275, label %276 [
    i64 1, label %306
    i64 2, label %299
    i64 3, label %289
  ]

; <label>:276:                                    ; preds = %271
  %277 = add i32 %.sroa.0.sroa.0.0, -16843009
  %278 = lshr i32 %277, 24
  %279 = and i32 %278, 63
  %280 = lshr i32 %277, 10
  %281 = and i32 %280, 4032
  %282 = or i32 %279, %281
  %283 = shl i32 %277, 4
  %284 = and i32 %283, 258048
  %285 = or i32 %282, %284
  %286 = shl i32 %277, 18
  %287 = and i32 %286, 1835008
  %288 = or i32 %285, %287
  br label %308

; <label>:289:                                    ; preds = %271
  %290 = add i32 %.sroa.0.sroa.0.0, -65793
  %291 = lshr i32 %290, 16
  %292 = and i32 %291, 63
  %293 = lshr i32 %290, 2
  %294 = and i32 %293, 4032
  %295 = or i32 %292, %294
  %296 = shl i32 %290, 12
  %297 = and i32 %296, 61440
  %298 = or i32 %295, %297
  br label %308

; <label>:299:                                    ; preds = %271
  %300 = add i32 %.sroa.0.sroa.0.0, -257
  %301 = lshr i32 %300, 8
  %302 = and i32 %301, 63
  %303 = shl i32 %300, 6
  %304 = and i32 %303, 1984
  %305 = or i32 %302, %304
  br label %308

; <label>:306:                                    ; preds = %271
  %307 = add i32 %.sroa.0.sroa.0.0, -1
  br label %308

; <label>:308:                                    ; preds = %306, %299, %289, %276
  %309 = phi i32 [ %288, %276 ], [ %298, %289 ], [ %305, %299 ], [ %307, %306 ]
  %310 = and i32 %309, 255
  %311 = icmp ne i32 %310, %309
  %312 = icmp ugt i32 %309, 255
  %or.cond65 = and i1 %312, %311
  br i1 %or.cond65, label %320, label %313

; <label>:313:                                    ; preds = %308
  %314 = trunc i32 %309 to i8
  store i8 %314, i8* %179, align 1
  %315 = getelementptr inbounds i8, i8* %179, i64 1
  br label %178

; <label>:316:                                    ; preds = %._crit_edge
  tail call void asm sideeffect "", "n"(i32 3) #8
  tail call void @llvm.trap()
  unreachable

; <label>:317:                                    ; preds = %123
  tail call void asm sideeffect "", "n"(i32 15) #8
  tail call void @llvm.trap()
  unreachable

; <label>:318:                                    ; preds = %32
  tail call void asm sideeffect "", "n"(i32 19) #8
  tail call void @llvm.trap()
  unreachable

; <label>:319:                                    ; preds = %._crit_edge155
  tail call void asm sideeffect "", "n"(i32 25) #8
  tail call void @llvm.trap()
  unreachable

; <label>:320:                                    ; preds = %308
  tail call void asm sideeffect "", "n"(i32 26) #8
  tail call void @llvm.trap()
  unreachable

; <label>:321:                                    ; preds = %190
  tail call void asm sideeffect "", "n"(i32 28) #8
  tail call void @llvm.trap()
  unreachable

.lr.ph219.1:                                      ; preds = %47
  %322 = load i8, i8* %51, align 1
  %323 = getelementptr inbounds i8, i8* %51, i64 1
  %324 = ptrtoint i8* %323 to i64
  %325 = zext i8 %322 to i32
  %326 = and i8 %58, 31
  %327 = zext i8 %326 to i32
  %328 = shl i32 %325, %327
  %329 = or i32 %328, %57
  %330 = add i8 %.sroa.12104.1.ph185, 16
  %331 = icmp ult i8 %330, 32
  br i1 %331, label %332, label %.loopexit116

; <label>:332:                                    ; preds = %.lr.ph219.1
  %333 = icmp eq i8* %323, %18
  %or.cond53.1 = and i1 %17, %333
  br i1 %or.cond53.1, label %.loopexit, label %.lr.ph219.2

.lr.ph219.2:                                      ; preds = %332
  %334 = load i8, i8* %323, align 1
  %335 = getelementptr inbounds i8, i8* %51, i64 2
  %336 = ptrtoint i8* %335 to i64
  %337 = zext i8 %334 to i32
  %338 = and i8 %330, 31
  %339 = zext i8 %338 to i32
  %340 = shl i32 %337, %339
  %341 = or i32 %340, %329
  %342 = add i8 %.sroa.12104.1.ph185, 24
  %343 = icmp ult i8 %342, 32
  br i1 %343, label %344, label %.loopexit116

; <label>:344:                                    ; preds = %.lr.ph219.2
  %345 = icmp eq i8* %335, %18
  %or.cond53.2 = and i1 %17, %345
  br i1 %or.cond53.2, label %.loopexit, label %.lr.ph219.3

.lr.ph219.3:                                      ; preds = %344
  %346 = load i8, i8* %335, align 1
  %347 = getelementptr inbounds i8, i8* %51, i64 3
  %348 = ptrtoint i8* %347 to i64
  %349 = zext i8 %346 to i32
  %350 = and i8 %342, 31
  %351 = zext i8 %350 to i32
  %352 = shl i32 %349, %351
  %353 = or i32 %352, %341
  %354 = add i8 %.sroa.12104.1.ph185, 32
  %355 = icmp ugt i8 %.sroa.12104.1.ph185, -33
  br i1 %355, label %356, label %.loopexit116

; <label>:356:                                    ; preds = %.lr.ph219.3
  %357 = icmp eq i8* %347, %18
  %or.cond53.3 = and i1 %17, %357
  br i1 %or.cond53.3, label %.loopexit, label %.lr.ph219.4

.lr.ph219.4:                                      ; preds = %356
  %358 = load i8, i8* %347, align 1
  %359 = getelementptr inbounds i8, i8* %51, i64 4
  %360 = ptrtoint i8* %359 to i64
  %361 = zext i8 %358 to i32
  %362 = and i8 %354, 31
  %363 = zext i8 %362 to i32
  %364 = shl i32 %361, %363
  %365 = or i32 %364, %353
  %366 = add i8 %.sroa.12104.1.ph185, 40
  %367 = icmp ult i8 %366, 32
  br i1 %367, label %.loopexit, label %.loopexit116

.lr.ph226.1:                                      ; preds = %205
  %368 = load i8, i8* %209, align 1
  %369 = getelementptr inbounds i8, i8* %209, i64 1
  %370 = ptrtoint i8* %369 to i64
  %371 = zext i8 %368 to i32
  %372 = and i8 %216, 31
  %373 = zext i8 %372 to i32
  %374 = shl i32 %371, %373
  %375 = or i32 %374, %215
  %376 = add i8 %.sroa.12.1.ph188, 16
  %377 = icmp ult i8 %376, 32
  br i1 %377, label %378, label %.loopexit120

; <label>:378:                                    ; preds = %.lr.ph226.1
  %379 = icmp eq i8* %369, %18
  %or.cond59.1 = and i1 %17, %379
  br i1 %or.cond59.1, label %.loopexit119, label %.lr.ph226.2

.lr.ph226.2:                                      ; preds = %378
  %380 = load i8, i8* %369, align 1
  %381 = getelementptr inbounds i8, i8* %209, i64 2
  %382 = ptrtoint i8* %381 to i64
  %383 = zext i8 %380 to i32
  %384 = and i8 %376, 31
  %385 = zext i8 %384 to i32
  %386 = shl i32 %383, %385
  %387 = or i32 %386, %375
  %388 = add i8 %.sroa.12.1.ph188, 24
  %389 = icmp ult i8 %388, 32
  br i1 %389, label %390, label %.loopexit120

; <label>:390:                                    ; preds = %.lr.ph226.2
  %391 = icmp eq i8* %381, %18
  %or.cond59.2 = and i1 %17, %391
  br i1 %or.cond59.2, label %.loopexit119, label %.lr.ph226.3

.lr.ph226.3:                                      ; preds = %390
  %392 = load i8, i8* %381, align 1
  %393 = getelementptr inbounds i8, i8* %209, i64 3
  %394 = ptrtoint i8* %393 to i64
  %395 = zext i8 %392 to i32
  %396 = and i8 %388, 31
  %397 = zext i8 %396 to i32
  %398 = shl i32 %395, %397
  %399 = or i32 %398, %387
  %400 = add i8 %.sroa.12.1.ph188, 32
  %401 = icmp ugt i8 %.sroa.12.1.ph188, -33
  br i1 %401, label %402, label %.loopexit120

; <label>:402:                                    ; preds = %.lr.ph226.3
  %403 = icmp eq i8* %393, %18
  %or.cond59.3 = and i1 %17, %403
  br i1 %or.cond59.3, label %.loopexit119, label %.lr.ph226.4

.lr.ph226.4:                                      ; preds = %402
  %404 = load i8, i8* %393, align 1
  %405 = getelementptr inbounds i8, i8* %209, i64 4
  %406 = ptrtoint i8* %405 to i64
  %407 = zext i8 %404 to i32
  %408 = and i8 %400, 31
  %409 = zext i8 %408 to i32
  %410 = shl i32 %407, %409
  %411 = or i32 %410, %399
  %412 = add i8 %.sroa.12.1.ph188, 40
  %413 = icmp ult i8 %412, 32
  br i1 %413, label %.loopexit119, label %.loopexit120
}

; Function Attrs: noinline
define linkonce_odr hidden swiftcc void @_T0s11_StringCoreV6appendyABFTf4gXn_n(i64, i64, %Ts11_StringCoreV* nocapture swiftself dereferenceable(24)) local_unnamed_addr #5 {
entry:
  %._countAndFlags._value = getelementptr inbounds %Ts11_StringCoreV, %Ts11_StringCoreV* %2, i64 0, i32 1, i32 0
  %3 = load i64, i64* %._countAndFlags._value, align 8
  %4 = lshr i64 %3, 63
  %5 = add nuw nsw i64 %4, 1
  %6 = lshr i64 %1, 63
  %7 = icmp ult i64 %4, %6
  br i1 %7, label %8, label %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread

; <label>:8:                                      ; preds = %entry
  %9 = icmp sgt i64 %1, -1
  br i1 %9, label %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread, label %10, !prof !5

; <label>:10:                                     ; preds = %8
  %11 = icmp eq i64 %0, 0
  br i1 %11, label %26, label %.lr.ph.i22

.lr.ph.i22:                                       ; preds = %10
  %12 = and i64 %1, 4611686018427387903
  %13 = inttoptr i64 %0 to %Ts6UInt16V*
  %14 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %13, i64 %12
  %15 = icmp ne %Ts6UInt16V* %14, null
  %16 = bitcast %Ts6UInt16V* %14 to i8*
  %17 = inttoptr i64 %0 to i8*
  %18 = icmp eq i8* %17, %16
  %or.cond.i2331 = and i1 %15, %18
  br i1 %or.cond.i2331, label %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread, label %.lr.ph.preheader

.lr.ph.preheader:                                 ; preds = %.lr.ph.i22
  br label %.lr.ph

; <label>:19:                                     ; preds = %.lr.ph
  %20 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %23, i64 1
  %21 = ptrtoint %Ts6UInt16V* %20 to i64
  %22 = icmp eq %Ts6UInt16V* %20, %14
  %or.cond.i23 = and i1 %15, %22
  br i1 %or.cond.i23, label %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph.preheader, %19
  %.sroa.0.05.i32 = phi i64 [ %21, %19 ], [ %0, %.lr.ph.preheader ]
  %23 = inttoptr i64 %.sroa.0.05.i32 to %Ts6UInt16V*
  %._value.i24 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %23, i64 0, i32 0
  %24 = load i16, i16* %._value.i24, align 2, !noalias !12
  %25 = icmp ugt i16 %24, 127
  br i1 %25, label %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread, label %19

; <label>:26:                                     ; preds = %10
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread: ; preds = %19, %.lr.ph, %.lr.ph.i22, %8, %entry
  %27 = phi i64 [ %5, %entry ], [ 1, %8 ], [ 1, %.lr.ph.i22 ], [ 1, %19 ], [ 2, %.lr.ph ]
  %28 = and i64 %3, 4611686018427387903
  %29 = and i64 %1, 4611686018427387903
  %._owner = getelementptr inbounds %Ts11_StringCoreV, %Ts11_StringCoreV* %2, i64 0, i32 2
  %30 = add nuw nsw i64 %28, %29
  %31 = bitcast %TyXlSg* %._owner to i64*
  %32 = load i64, i64* %31, align 8
  %.mask = and i64 %3, 4611686018427387904
  %33 = icmp ne i64 %.mask, 0
  %34 = icmp eq i64 %32, 0
  %or.cond7 = or i1 %33, %34
  %35 = icmp slt i64 %5, %27
  %or.cond = or i1 %35, %or.cond7
  br i1 %or.cond, label %71, label %36

; <label>:36:                                     ; preds = %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread
  %37 = inttoptr i64 %32 to %swift.refcounted*
  %38 = tail call i1 @swift_isUniquelyReferenced_native(%swift.refcounted* %37) #8
  br i1 %38, label %39, label %71, !prof !5

; <label>:39:                                     ; preds = %36
  %40 = bitcast %Ts11_StringCoreV* %2 to i64*
  %41 = load i64, i64* %40, align 8
  %42 = icmp eq i64 %41, 0
  br i1 %42, label %142, label %43

; <label>:43:                                     ; preds = %39
  %44 = inttoptr i64 %41 to i8*
  %45 = inttoptr i64 %32 to i8*
  %46 = getelementptr inbounds i8, i8* %45, i64 32
  %47 = icmp ne i8* %46, %44
  %.value2.capacityAndElementShift = getelementptr inbounds i8, i8* %45, i64 24
  %.value2.capacityAndElementShift._value = bitcast i8* %.value2.capacityAndElementShift to i64*
  %48 = load i64, i64* %.value2.capacityAndElementShift._value, align 8
  %49 = and i64 %48, -2
  %50 = and i64 %48, 1
  %51 = ashr i64 %49, %50
  %52 = icmp slt i64 %51, %30
  %or.cond46 = or i1 %47, %52
  br i1 %or.cond46, label %._crit_edge, label %53, !prof !6

; <label>:53:                                     ; preds = %43
  %54 = getelementptr inbounds i8, i8* %45, i64 16
  %55 = shl i64 %30, %4
  %56 = getelementptr inbounds i8, i8* %44, i64 %55
  %57 = ptrtoint i8* %56 to i64
  %58 = bitcast i8* %54 to i64*
  store i64 %57, i64* %58, align 8
  %59 = load i64, i64* %._countAndFlags._value, align 8
  %60 = and i64 %59, -4611686018427387904
  %61 = shl i64 %28, %4
  %62 = getelementptr inbounds i8, i8* %44, i64 %61
  %63 = or i64 %60, %30
  store i64 %63, i64* %._countAndFlags._value, align 8
  br label %82

._crit_edge:                                      ; preds = %43
  %64 = icmp slt i64 %51, %30
  br i1 %64, label %65, label %71

; <label>:65:                                     ; preds = %._crit_edge
  %66 = tail call { i64, i1 } @llvm.smul.with.overflow.i64(i64 %51, i64 2)
  %67 = extractvalue { i64, i1 } %66, 0
  %68 = extractvalue { i64, i1 } %66, 1
  br i1 %68, label %143, label %69

; <label>:69:                                     ; preds = %65
  %70 = icmp slt i64 %30, %67
  %.5 = select i1 %70, i64 %67, i64 %30
  br label %71

; <label>:71:                                     ; preds = %69, %._crit_edge, %36, %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread
  %72 = phi i64 [ %.5, %69 ], [ %30, %._crit_edge ], [ %30, %36 ], [ %30, %_T0s11_StringCoreV22isRepresentableAsASCIISbyFTf4x_n.exit.thread ]
  tail call swiftcc void @_T0s11_StringCoreV12_copyInPlaceySi7newSize_Si0F8CapacitySi15minElementWidthtF(i64 %30, i64 %72, i64 %27, %Ts11_StringCoreV* nocapture nonnull swiftself dereferenceable(24) %2)
  %73 = bitcast %Ts11_StringCoreV* %2 to i64*
  %74 = load i64, i64* %73, align 8
  %75 = load i64, i64* %._countAndFlags._value, align 8
  %76 = icmp eq i64 %74, 0
  br i1 %76, label %140, label %77

; <label>:77:                                     ; preds = %71
  %78 = inttoptr i64 %74 to i8*
  %79 = lshr i64 %75, 63
  %80 = shl i64 %28, %79
  %81 = getelementptr inbounds i8, i8* %78, i64 %80
  br label %82

; <label>:82:                                     ; preds = %53, %77
  %83 = phi i8* [ %81, %77 ], [ %62, %53 ]
  %84 = phi i64 [ %75, %77 ], [ %63, %53 ]
  %85 = icmp eq i64 %0, 0
  br i1 %85, label %141, label %86

; <label>:86:                                     ; preds = %82
  %87 = inttoptr i64 %0 to i8*
  %88 = lshr i64 %84, 63
  %89 = icmp eq i64 %6, %88
  br i1 %89, label %138, label %90, !prof !5

; <label>:90:                                     ; preds = %86
  %91 = icmp ult i64 %6, %88
  br i1 %91, label %114, label %92

; <label>:92:                                     ; preds = %90
  %93 = inttoptr i64 %0 to %Ts6UInt16V*
  %94 = getelementptr inbounds %Ts6UInt16V, %Ts6UInt16V* %93, i64 %29
  %95 = bitcast %Ts6UInt16V* %94 to i8*
  %96 = icmp eq i8* %95, %87
  br i1 %96, label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit, label %97

; <label>:97:                                     ; preds = %92
  %._value5.i = inttoptr i64 %0 to i16*
  %98 = load i16, i16* %._value5.i, align 2
  %99 = and i16 %98, 255
  %100 = icmp ne i16 %99, %98
  %101 = icmp ugt i16 %98, 255
  %or.cond6.i = and i1 %101, %100
  br i1 %or.cond6.i, label %._crit_edge.i, label %.lr.ph.i.preheader

.lr.ph.i.preheader:                               ; preds = %97
  br label %.lr.ph.i

; <label>:102:                                    ; preds = %.lr.ph.i
  %103 = getelementptr inbounds i8, i8* %110, i64 1
  %._value.i = bitcast i8* %112 to i16*
  %104 = load i16, i16* %._value.i, align 2
  %105 = and i16 %104, 255
  %106 = icmp ne i16 %105, %104
  %107 = icmp ugt i16 %104, 255
  %or.cond.i = and i1 %107, %106
  br i1 %or.cond.i, label %._crit_edge.i, label %.lr.ph.i

.lr.ph.i:                                         ; preds = %.lr.ph.i.preheader, %102
  %108 = phi i16 [ %104, %102 ], [ %98, %.lr.ph.i.preheader ]
  %109 = phi i8* [ %112, %102 ], [ %87, %.lr.ph.i.preheader ]
  %110 = phi i8* [ %103, %102 ], [ %83, %.lr.ph.i.preheader ]
  %111 = trunc i16 %108 to i8
  store i8 %111, i8* %110, align 1
  %112 = getelementptr inbounds i8, i8* %109, i64 2
  %113 = icmp eq i8* %112, %95
  br i1 %113, label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit, label %102

; <label>:114:                                    ; preds = %90
  %115 = getelementptr inbounds i8, i8* %87, i64 %29
  %116 = icmp eq i64 %29, 0
  br i1 %116, label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit, label %117

; <label>:117:                                    ; preds = %114
  %min.iters.check = icmp ult i64 %29, 16
  br i1 %min.iters.check, label %scalar.ph.preheader, label %vector.memcheck

vector.memcheck:                                  ; preds = %117
  %118 = shl nuw nsw i64 %29, 1
  %scevgep = getelementptr i8, i8* %83, i64 %118
  %bound0 = icmp ult i8* %83, %115
  %bound1 = icmp ugt i8* %scevgep, %87
  %memcheck.conflict = and i1 %bound0, %bound1
  br i1 %memcheck.conflict, label %scalar.ph.preheader, label %vector.ph

vector.ph:                                        ; preds = %vector.memcheck
  %n.mod.vf = and i64 %1, 15
  %n.vec = sub nsw i64 %29, %n.mod.vf
  %119 = sub nsw i64 %29, %n.mod.vf
  %120 = shl i64 %119, 1
  %ind.end = getelementptr i8, i8* %83, i64 %120
  %ind.end49 = getelementptr i8, i8* %87, i64 %n.vec
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %121 = shl i64 %index, 1
  %next.gep = getelementptr i8, i8* %83, i64 %121
  %next.gep51 = getelementptr i8, i8* %87, i64 %index
  %122 = bitcast i8* %next.gep51 to <8 x i8>*
  %wide.load = load <8 x i8>, <8 x i8>* %122, align 1, !alias.scope !15
  %123 = getelementptr i8, i8* %next.gep51, i64 8
  %124 = bitcast i8* %123 to <8 x i8>*
  %wide.load53 = load <8 x i8>, <8 x i8>* %124, align 1, !alias.scope !15
  %125 = zext <8 x i8> %wide.load to <8 x i16>
  %126 = zext <8 x i8> %wide.load53 to <8 x i16>
  %127 = bitcast i8* %next.gep to <8 x i16>*
  store <8 x i16> %125, <8 x i16>* %127, align 2, !alias.scope !18, !noalias !15
  %128 = getelementptr i8, i8* %next.gep, i64 16
  %129 = bitcast i8* %128 to <8 x i16>*
  store <8 x i16> %126, <8 x i16>* %129, align 2, !alias.scope !18, !noalias !15
  %index.next = add i64 %index, 16
  %130 = icmp eq i64 %index.next, %n.vec
  br i1 %130, label %middle.block, label %vector.body, !llvm.loop !20

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.mod.vf, 0
  br i1 %cmp.n, label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit, label %scalar.ph.preheader

scalar.ph.preheader:                              ; preds = %middle.block, %vector.memcheck, %117
  %.ph = phi i8* [ %83, %vector.memcheck ], [ %83, %117 ], [ %ind.end, %middle.block ]
  %.ph54 = phi i8* [ %87, %vector.memcheck ], [ %87, %117 ], [ %ind.end49, %middle.block ]
  br label %scalar.ph

scalar.ph:                                        ; preds = %scalar.ph.preheader, %scalar.ph
  %131 = phi i8* [ %137, %scalar.ph ], [ %.ph, %scalar.ph.preheader ]
  %132 = phi i8* [ %135, %scalar.ph ], [ %.ph54, %scalar.ph.preheader ]
  %133 = load i8, i8* %132, align 1
  %134 = zext i8 %133 to i16
  %._value3.i = bitcast i8* %131 to i16*
  store i16 %134, i16* %._value3.i, align 2
  %135 = getelementptr inbounds i8, i8* %132, i64 1
  %136 = icmp eq i8* %135, %115
  %137 = getelementptr inbounds i8, i8* %131, i64 2
  br i1 %136, label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit, label %scalar.ph, !llvm.loop !23

; <label>:138:                                    ; preds = %86
  %139 = shl i64 %29, %6
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %83, i8* %87, i64 %139, i32 1, i1 false)
  br label %_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit

._crit_edge.i:                                    ; preds = %102, %97
  tail call void asm sideeffect "", "n"(i32 0) #8
  tail call void @llvm.trap()
  unreachable

_T0s11_StringCoreV13_copyElementsySv_Si15srcElementWidthSv8dstStartSi0hfG0Si5counttFZTf4nnnnnd_n.exit: ; preds = %.lr.ph.i, %scalar.ph, %middle.block, %92, %114, %138
  ret void

; <label>:140:                                    ; preds = %71
  tail call void asm sideeffect "", "n"(i32 1) #8
  tail call void @llvm.trap()
  unreachable

; <label>:141:                                    ; preds = %82
  tail call void asm sideeffect "", "n"(i32 2) #8
  tail call void @llvm.trap()
  unreachable

; <label>:142:                                    ; preds = %39
  tail call void asm sideeffect "", "n"(i32 6) #8
  tail call void @llvm.trap()
  unreachable

; <label>:143:                                    ; preds = %65
  tail call void asm sideeffect "", "n"(i32 10) #8
  tail call void @llvm.trap()
  unreachable
}

define linkonce_odr hidden swiftcc { i64, i64, i64 } @_T0S3S19stringInterpolationd_tcfCTf4nd_n(%Ts27_ContiguousArrayStorageBaseC*) local_unnamed_addr #1 {
entry:
  %1 = alloca %Ts11_StringCoreV, align 8
  %2 = getelementptr inbounds %Ts11_StringCoreV, %Ts11_StringCoreV* %1, i64 0, i32 0, i32 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 24, i8* nonnull %2)
  %3 = tail call swiftcc i8* @_T0s19_emptyStringStorages6UInt32Vvau()
  %4 = ptrtoint i8* %3 to i64
  %5 = bitcast %Ts11_StringCoreV* %1 to i64*
  store i64 %4, i64* %5, align 8
  %._countAndFlags._value = getelementptr inbounds %Ts11_StringCoreV, %Ts11_StringCoreV* %1, i64 0, i32 1, i32 0
  %._owner = getelementptr inbounds %Ts11_StringCoreV, %Ts11_StringCoreV* %1, i64 0, i32 2
  %6 = bitcast %TyXlSg* %._owner to i64*
  %7 = bitcast i64* %._countAndFlags._value to i8*
  call void @llvm.memset.p0i8.i64(i8* %7, i8 0, i64 16, i32 8, i1 false)
  %offset = load i64, i64* @_T0s27_ContiguousArrayStorageBaseC16countAndCapacitys01_B4BodyVvpWvd, align 8
  %8 = bitcast %Ts27_ContiguousArrayStorageBaseC* %0 to i8*
  %9 = getelementptr inbounds i8, i8* %8, i64 %offset
  %.countAndCapacity._storage.count._value = bitcast i8* %9 to i64*
  %10 = load i64, i64* %.countAndCapacity._storage.count._value, align 8, !range !3
  %11 = icmp eq i64 %10, 0
  %.pre13 = getelementptr inbounds %Ts27_ContiguousArrayStorageBaseC, %Ts27_ContiguousArrayStorageBaseC* %0, i64 0, i32 0
  br i1 %11, label %entry._crit_edge, label %12

; <label>:12:                                     ; preds = %entry
  %13 = getelementptr inbounds %Ts27_ContiguousArrayStorageBaseC, %Ts27_ContiguousArrayStorageBaseC* %0, i64 1
  %tailaddr = bitcast %Ts27_ContiguousArrayStorageBaseC* %13 to %TSS*
  %14 = bitcast %TyXlSg* %._owner to %swift.refcounted**
  %15 = tail call %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned %.pre13) #8
  br label %16

; <label>:16:                                     ; preds = %._crit_edge, %12
  %17 = phi i64 [ 0, %12 ], [ %.pre, %._crit_edge ]
  %18 = phi i64 [ 0, %12 ], [ %32, %._crit_edge ]
  %._core = getelementptr inbounds %TSS, %TSS* %tailaddr, i64 %18, i32 0
  %19 = bitcast %Ts11_StringCoreV* %._core to i64*
  %20 = load i64, i64* %19, align 8
  %._core._countAndFlags._value = getelementptr inbounds %TSS, %TSS* %tailaddr, i64 %18, i32 0, i32 1, i32 0
  %21 = load i64, i64* %._core._countAndFlags._value, align 8
  %._core._owner = getelementptr inbounds %TSS, %TSS* %tailaddr, i64 %18, i32 0, i32 2
  %22 = bitcast %TyXlSg* %._core._owner to i64*
  %23 = load i64, i64* %22, align 8
  %24 = and i64 %17, 4611686018427387903
  %25 = icmp eq i64 %24, 0
  br i1 %25, label %27, label %26

; <label>:26:                                     ; preds = %16
  call swiftcc void @_T0s11_StringCoreV6appendyABFTf4gXn_n(i64 %20, i64 %21, %Ts11_StringCoreV* nocapture nonnull swiftself dereferenceable(24) %1)
  br label %31

; <label>:27:                                     ; preds = %16
  %28 = load %swift.refcounted*, %swift.refcounted** %14, align 8
  store i64 %20, i64* %5, align 8
  store i64 %21, i64* %._countAndFlags._value, align 8
  store i64 %23, i64* %6, align 8
  %29 = inttoptr i64 %23 to %swift.refcounted*
  %30 = tail call %swift.refcounted* @swift_rt_swift_retain(%swift.refcounted* returned %29) #8
  tail call void @swift_rt_swift_release(%swift.refcounted* %28) #8
  br label %31

; <label>:31:                                     ; preds = %27, %26
  %32 = add nuw nsw i64 %18, 1
  %33 = icmp eq i64 %32, %10
  br i1 %33, label %34, label %._crit_edge

._crit_edge:                                      ; preds = %31
  %.pre = load i64, i64* %._countAndFlags._value, align 8
  br label %16

; <label>:34:                                     ; preds = %31
  tail call void @swift_rt_swift_release(%swift.refcounted* nonnull %.pre13) #8
  %.pre10 = load i64, i64* %5, align 8
  %.pre11 = load i64, i64* %._countAndFlags._value, align 8
  %.pre12 = load i64, i64* %6, align 8
  br label %entry._crit_edge

entry._crit_edge:                                 ; preds = %entry, %34
  %35 = phi i64 [ %.pre12, %34 ], [ 0, %entry ]
  %36 = phi i64 [ %.pre11, %34 ], [ 0, %entry ]
  %37 = phi i64 [ %.pre10, %34 ], [ %4, %entry ]
  tail call void @swift_rt_swift_release(%swift.refcounted* %.pre13) #8
  call void @llvm.lifetime.end.p0i8(i64 24, i8* nonnull %2)
  %38 = insertvalue { i64, i64, i64 } undef, i64 %37, 0
  %39 = insertvalue { i64, i64, i64 } %38, i64 %36, 1
  %40 = insertvalue { i64, i64, i64 } %39, i64 %35, 2
  ret { i64, i64, i64 } %40
}

; Function Attrs: noinline
declare swiftcc { i64, i64, i64 } @_T0s5printyypd_SS9separatorSS10terminatortFfA0_() local_unnamed_addr #5

; Function Attrs: noinline
declare swiftcc { i64, i64, i64 } @_T0s5printyypd_SS9separatorSS10terminatortFfA1_() local_unnamed_addr #5

; Function Attrs: noinline
declare swiftcc void @_T0s5printyypd_SS9separatorSS10terminatortF(%Ts27_ContiguousArrayStorageBaseC*, i64, i64, i64, i64, i64, i64) local_unnamed_addr #5

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.ssub.with.overflow.i64(i64, i64) #9

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.sadd.with.overflow.i64(i64, i64) #9

declare swiftcc i8* @_T0s19_emptyStringStorages6UInt32Vvau() local_unnamed_addr #1

; Function Attrs: nounwind
declare zeroext i1 @swift_isUniquelyReferenced_native(%swift.refcounted*) local_unnamed_addr #8

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #6

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #6

declare %swift.type* @_T0s17_HeapBufferHeaderVMa(%swift.type*) local_unnamed_addr #1

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.smul.with.overflow.i64(i64, i64) #9

declare swiftcc i64 @swift_int64ToString(i8*, i64, i64, i64, i1) local_unnamed_addr #1

; Function Attrs: nounwind readnone speculatable
declare i32 @llvm.ctlz.i32(i32, i1) #9

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.usub.with.overflow.i32(i32, i32) #9

; Function Attrs: nounwind readnone speculatable
declare i16 @llvm.bswap.i16(i16) #9

declare %swift.type* @_T0s13ManagedBufferCMa(%swift.type*, %swift.type*) local_unnamed_addr #1

declare swiftcc %swift.refcounted* @swift_bufferAllocate(%swift.type*, i64, i64) local_unnamed_addr #1

; Function Attrs: nounwind optsize readnone
declare i64 @_stdlib_malloc_size(i8*) local_unnamed_addr #10

; Function Attrs: nounwind readnone speculatable
declare { i16, i1 } @llvm.uadd.with.overflow.i16(i16, i16) #9

; Function Attrs: noinline nounwind
define linkonce_odr hidden %swift.refcounted* @swift_rt_swift_retain_n(%swift.refcounted* returned, i32) #0 {
entry:
  %load = load %swift.refcounted* (%swift.refcounted*, i32)*, %swift.refcounted* (%swift.refcounted*, i32)** @_swift_retain_n
  %2 = tail call %swift.refcounted* %load(%swift.refcounted* returned %0, i32 %1)
  ret %swift.refcounted* %2
}

attributes #0 = { noinline nounwind }
attributes #1 = { "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" }
attributes #2 = { noreturn nounwind }
attributes #3 = { nounwind "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" }
attributes #4 = { noinline norecurse "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" }
attributes #5 = { noinline "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" }
attributes #6 = { argmemonly nounwind }
attributes #7 = { noinline nounwind readonly }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone speculatable }
attributes #10 = { nounwind optsize readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nounwind readnone }

!llvm.linker.options = !{}
!llvm.module.flags = !{!0, !1, !2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 4, !"Objective-C Garbage Collection", i32 1536}
!2 = !{i32 1, !"Swift Version", i32 6}
!3 = !{i64 0, i64 9223372036854775807}
!4 = !{!"branch_weights", i32 2000, i32 2001, i32 1}
!5 = !{!"branch_weights", i32 2000, i32 1}
!6 = !{!"branch_weights", i32 2002, i32 2000}
!7 = !{!"branch_weights", i32 1, i32 2000}
!8 = !{i32 0, i32 33}
!9 = distinct !{!9, !10}
!10 = !{!"llvm.loop.unroll.disable"}
!11 = distinct !{!11, !10}
!12 = !{!13}
!13 = distinct !{!13, !14, !"_T0s8SequencePsE8containsS2b7ElementQzKc5where_tKFSRys6UInt16VG_Tgq505_T0s6E37VSbs5Error_pIgydzo_ABSbsAC_pIgidzo_TRAHSbs0H0_pIgydzo_Tf1cn_nTf4ng_n: argument 0"}
!14 = distinct !{!14, !"_T0s8SequencePsE8containsS2b7ElementQzKc5where_tKFSRys6UInt16VG_Tgq505_T0s6E37VSbs5Error_pIgydzo_ABSbsAC_pIgidzo_TRAHSbs0H0_pIgydzo_Tf1cn_nTf4ng_n"}
!15 = !{!16}
!16 = distinct !{!16, !17}
!17 = distinct !{!17, !"LVerDomain"}
!18 = !{!19}
!19 = distinct !{!19, !17}
!20 = distinct !{!20, !21, !22}
!21 = !{!"llvm.loop.vectorize.width", i32 1}
!22 = !{!"llvm.loop.interleave.count", i32 1}
!23 = distinct !{!23, !21, !22}
