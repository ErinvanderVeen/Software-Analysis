use std::env;

#[inline(never)]
pub fn fib(n: i64) -> i64 {
	if n <= 1 {
		1
	} else {
		fib(n - 1) + fib(n - 2)
	}
}

fn main() {
	let args: Vec<String> = env::args().collect();

	let num: i64 = (&args[1]).parse::<i64>().unwrap();

	println!("{}", fib(num));
}
