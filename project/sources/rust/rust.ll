; ModuleID = 'rust.cgu-0.rs'
source_filename = "rust.cgu-0.rs"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%str_slice = type { i8*, i64 }
%"2.std::num::ParseIntError" = type { i8 }
%"2.std::fmt::Arguments" = type { { %str_slice*, i64 }, %"2.std::option::Option<&[std::fmt::rt::v1::Argument]>", { %"2.std::fmt::ArgumentV1"*, i64 } }
%"2.std::option::Option<&[std::fmt::rt::v1::Argument]>" = type { { %"2.std::fmt::rt::v1::Argument"*, i64 } }
%"2.std::fmt::rt::v1::Argument" = type { %"2.std::fmt::rt::v1::Position", %"2.std::fmt::rt::v1::FormatSpec" }
%"2.std::fmt::rt::v1::Position" = type { i64, [0 x i64], [1 x i64] }
%"2.std::fmt::rt::v1::FormatSpec" = type { i32, i8, i32, %"2.std::fmt::rt::v1::Count", %"2.std::fmt::rt::v1::Count" }
%"2.std::fmt::rt::v1::Count" = type { i64, [0 x i64], [1 x i64] }
%"2.std::fmt::ArgumentV1" = type { %"2.core::fmt::Void"*, i8 (%"2.core::fmt::Void"*, %"2.std::fmt::Formatter"*)* }
%"2.core::fmt::Void" = type {}
%"2.std::fmt::Formatter" = type { i32, i32, i8, %"2.std::option::Option<usize>", %"2.std::option::Option<usize>", { i8*, void (i8*)** }, %"2.std::slice::Iter<std::fmt::ArgumentV1>", { %"2.std::fmt::ArgumentV1"*, i64 } }
%"2.std::option::Option<usize>" = type { i64, [0 x i64], [1 x i64] }
%"2.std::slice::Iter<std::fmt::ArgumentV1>" = type { %"2.std::fmt::ArgumentV1"*, %"2.std::fmt::ArgumentV1"*, %"2.std::marker::PhantomData<&std::fmt::ArgumentV1>" }
%"2.std::marker::PhantomData<&std::fmt::ArgumentV1>" = type {}
%"1.std::env::Args" = type { %"1.std::env::ArgsOs" }
%"1.std::env::ArgsOs" = type { %"1.std::sys::imp::args::Args" }
%"1.std::sys::imp::args::Args" = type { %"3.std::vec::IntoIter<std::ffi::OsString>", %"2.std::marker::PhantomData<*mut ()>" }
%"3.std::vec::IntoIter<std::ffi::OsString>" = type { %"2.std::ptr::Shared<std::ffi::OsString>", i64, %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"* }
%"2.std::ptr::Shared<std::ffi::OsString>" = type { %"2.core::nonzero::NonZero<*const std::ffi::OsString>", %"2.std::marker::PhantomData<std::ffi::OsString>" }
%"2.core::nonzero::NonZero<*const std::ffi::OsString>" = type { %"1.std::ffi::OsString"* }
%"2.std::marker::PhantomData<std::ffi::OsString>" = type {}
%"1.std::ffi::OsString" = type { %"1.std::sys::imp::os_str::Buf" }
%"1.std::sys::imp::os_str::Buf" = type { %"3.std::vec::Vec<u8>" }
%"3.std::vec::Vec<u8>" = type { %"5.alloc::raw_vec::RawVec<u8>", i64 }
%"5.alloc::raw_vec::RawVec<u8>" = type { %"2.std::ptr::Unique<u8>", i64 }
%"2.std::ptr::Unique<u8>" = type { %"2.core::nonzero::NonZero<*const u8>", %"2.std::marker::PhantomData<u8>" }
%"2.core::nonzero::NonZero<*const u8>" = type { i8* }
%"2.std::marker::PhantomData<u8>" = type {}
%"2.std::marker::PhantomData<*mut ()>" = type {}
%"2.std::option::Option<std::string::String>" = type { %"3.std::string::String" }
%"3.std::string::String" = type { %"3.std::vec::Vec<u8>" }
%"2.std::result::Result<i64, std::num::ParseIntError>" = type { i8, [7 x i8], [1 x i64] }
%"8.unwind::libunwind::_Unwind_Exception" = type { i64, void (i32, %"8.unwind::libunwind::_Unwind_Exception"*)*, [6 x i64] }
%"8.unwind::libunwind::_Unwind_Context" = type {}

@str.0 = internal constant [17 x i8] c"capacity overflow"
@str.1 = internal constant [43 x i8] c"called `Result::unwrap()` on an `Err` value"
@_ZN4core6result13unwrap_failed15__STATIC_FMTSTR17h5907eda901f84b68E = external local_unnamed_addr global { %str_slice*, i64 }
@_ZN4core6result13unwrap_failed10_FILE_LINE17h7363461bd668782fE = external global { %str_slice, i32 }
@str.2 = internal constant [25 x i8] c"src/libcollections/vec.rs"
@panic_bounds_check_loc.3 = internal unnamed_addr constant { %str_slice, i32 } { %str_slice { i8* getelementptr inbounds ([25 x i8], [25 x i8]* @str.2, i32 0, i32 0), i64 25 }, i32 1362 }, align 8
@str.4 = internal constant [0 x i8] zeroinitializer
@str.5 = internal constant [1 x i8] c"\0A"
@ref.6 = internal unnamed_addr constant [2 x %str_slice] [%str_slice { i8* getelementptr inbounds ([0 x i8], [0 x i8]* @str.4, i32 0, i32 0), i64 0 }, %str_slice { i8* getelementptr inbounds ([1 x i8], [1 x i8]* @str.5, i32 0, i32 0), i64 1 }], align 8

; Function Attrs: cold noinline noreturn uwtable
define internal fastcc void @_ZN4core6result13unwrap_failed17h0be2fbb68ff466eeE(i8) unnamed_addr #0 personality i32 (i32, i32, i64, %"8.unwind::libunwind::_Unwind_Exception"*, %"8.unwind::libunwind::_Unwind_Context"*)* @rust_eh_personality {
entry-block:
  %msg = alloca %str_slice, align 8
  %error = alloca %"2.std::num::ParseIntError", align 8
  %_5 = alloca %"2.std::fmt::Arguments", align 8
  %_10 = alloca [2 x %"2.std::fmt::ArgumentV1"], align 8
  %1 = bitcast %str_slice* %msg to i8*
  call void @llvm.lifetime.start(i64 16, i8* %1)
  %2 = getelementptr inbounds %str_slice, %str_slice* %msg, i64 0, i32 0
  store i8* getelementptr inbounds ([43 x i8], [43 x i8]* @str.1, i64 0, i64 0), i8** %2, align 8
  %3 = getelementptr inbounds %str_slice, %str_slice* %msg, i64 0, i32 1
  store i64 43, i64* %3, align 8
  %4 = getelementptr inbounds %"2.std::num::ParseIntError", %"2.std::num::ParseIntError"* %error, i64 0, i32 0
  call void @llvm.lifetime.start(i64 1, i8* %4)
  store i8 %0, i8* %4, align 8
  %5 = bitcast %"2.std::fmt::Arguments"* %_5 to i8*
  call void @llvm.lifetime.start(i64 48, i8* %5)
  %6 = load i64, i64* bitcast ({ %str_slice*, i64 }* @_ZN4core6result13unwrap_failed15__STATIC_FMTSTR17h5907eda901f84b68E to i64*), align 8, !range !1
  %7 = load i64, i64* getelementptr inbounds ({ %str_slice*, i64 }, { %str_slice*, i64 }* @_ZN4core6result13unwrap_failed15__STATIC_FMTSTR17h5907eda901f84b68E, i64 0, i32 1), align 8
  %8 = bitcast [2 x %"2.std::fmt::ArgumentV1"]* %_10 to i8*
  call void @llvm.lifetime.start(i64 32, i8* %8)
  %9 = ptrtoint %str_slice* %msg to i64
  %10 = ptrtoint %"2.std::num::ParseIntError"* %error to i64
  %11 = bitcast [2 x %"2.std::fmt::ArgumentV1"]* %_10 to i64*
  store i64 %9, i64* %11, align 8
  %12 = getelementptr inbounds [2 x %"2.std::fmt::ArgumentV1"], [2 x %"2.std::fmt::ArgumentV1"]* %_10, i64 0, i64 0, i32 1
  %13 = bitcast i8 (%"2.core::fmt::Void"*, %"2.std::fmt::Formatter"*)** %12 to i64*
  store i64 ptrtoint (i8 (%str_slice*, %"2.std::fmt::Formatter"*)* @"_ZN55_$LT$$RF$$u27$a$u20$T$u20$as$u20$core..fmt..Display$GT$3fmt17h510ed05e72307174E" to i64), i64* %13, align 8
  %14 = getelementptr inbounds [2 x %"2.std::fmt::ArgumentV1"], [2 x %"2.std::fmt::ArgumentV1"]* %_10, i64 0, i64 1
  %15 = bitcast %"2.std::fmt::ArgumentV1"* %14 to i64*
  store i64 %10, i64* %15, align 8
  %16 = getelementptr inbounds [2 x %"2.std::fmt::ArgumentV1"], [2 x %"2.std::fmt::ArgumentV1"]* %_10, i64 0, i64 1, i32 1
  %17 = bitcast i8 (%"2.core::fmt::Void"*, %"2.std::fmt::Formatter"*)** %16 to i64*
  store i64 ptrtoint (i8 (%"2.std::num::ParseIntError"*, %"2.std::fmt::Formatter"*)* @"_ZN61_$LT$core..num..ParseIntError$u20$as$u20$core..fmt..Debug$GT$3fmt17hbb30da852c6908a5E" to i64), i64* %17, align 8
  %18 = getelementptr inbounds [2 x %"2.std::fmt::ArgumentV1"], [2 x %"2.std::fmt::ArgumentV1"]* %_10, i64 0, i64 0
  %19 = bitcast %"2.std::fmt::Arguments"* %_5 to i64*
  store i64 %6, i64* %19, align 8, !alias.scope !2, !noalias !5
  %20 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_5, i64 0, i32 0, i32 1
  store i64 %7, i64* %20, align 8, !alias.scope !2, !noalias !5
  %_6.sroa.0.0..sroa_idx.i = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_5, i64 0, i32 1, i32 0, i32 0
  store %"2.std::fmt::rt::v1::Argument"* null, %"2.std::fmt::rt::v1::Argument"** %_6.sroa.0.0..sroa_idx.i, align 8, !alias.scope !2, !noalias !5
  %21 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_5, i64 0, i32 2, i32 0
  store %"2.std::fmt::ArgumentV1"* %18, %"2.std::fmt::ArgumentV1"** %21, align 8, !alias.scope !2, !noalias !5
  %22 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_5, i64 0, i32 2, i32 1
  store i64 2, i64* %22, align 8, !alias.scope !2, !noalias !5
  call void @_ZN4core9panicking9panic_fmt17hcfbb59eeb7f27f75E(%"2.std::fmt::Arguments"* noalias nocapture nonnull dereferenceable(48) %_5, { %str_slice, i32 }* noalias nonnull readonly dereferenceable(24) @_ZN4core6result13unwrap_failed10_FILE_LINE17h7363461bd668782fE)
  unreachable
}

; Function Attrs: uwtable
define internal i8 @"_ZN55_$LT$$RF$$u27$a$u20$T$u20$as$u20$core..fmt..Display$GT$3fmt17h510ed05e72307174E"(%str_slice* noalias nocapture readonly dereferenceable(16), %"2.std::fmt::Formatter"* dereferenceable(96)) unnamed_addr #1 {
entry-block:
  %2 = getelementptr inbounds %str_slice, %str_slice* %0, i64 0, i32 0
  %3 = load i8*, i8** %2, align 8, !nonnull !8
  %4 = getelementptr inbounds %str_slice, %str_slice* %0, i64 0, i32 1
  %5 = load i64, i64* %4, align 8
  %6 = tail call i8 @"_ZN42_$LT$str$u20$as$u20$core..fmt..Display$GT$3fmt17hc1be2fa738bd7dbaE"(i8* noalias nonnull readonly %3, i64 %5, %"2.std::fmt::Formatter"* nonnull dereferenceable(96) %1)
  ret i8 %6
}

; Function Attrs: noinline nounwind readnone uwtable
define internal fastcc i64 @_ZN4rust3fib17h1dc2f854f5b08437E(i64) unnamed_addr #2 {
entry-block:
  %1 = icmp slt i64 %0, 2
  br i1 %1, label %bb5, label %bb2.preheader

bb2.preheader:                                    ; preds = %entry-block
  br label %bb2

bb2:                                              ; preds = %bb2.preheader, %bb2
  %.tr2 = phi i64 [ %4, %bb2 ], [ %0, %bb2.preheader ]
  %accumulator.tr1 = phi i64 [ %5, %bb2 ], [ 1, %bb2.preheader ]
  %2 = add i64 %.tr2, -1
  %3 = tail call fastcc i64 @_ZN4rust3fib17h1dc2f854f5b08437E(i64 %2)
  %4 = add nsw i64 %.tr2, -2
  %5 = add i64 %3, %accumulator.tr1
  %6 = icmp slt i64 %4, 2
  br i1 %6, label %bb5.loopexit, label %bb2

bb5.loopexit:                                     ; preds = %bb2
  br label %bb5

bb5:                                              ; preds = %bb5.loopexit, %entry-block
  %accumulator.tr.lcssa = phi i64 [ 1, %entry-block ], [ %5, %bb5.loopexit ]
  ret i64 %accumulator.tr.lcssa
}

; Function Attrs: uwtable
define internal void @_ZN4rust4main17h1daf7664e0d7324fE() unnamed_addr #1 personality i32 (i32, i32, i64, %"8.unwind::libunwind::_Unwind_Exception"*, %"8.unwind::libunwind::_Unwind_Context"*)* @rust_eh_personality {
entry-block:
  %iterator.i.i.i = alloca %"1.std::env::Args", align 8
  %_39.i.i.i = alloca %"2.std::option::Option<std::string::String>", align 8
  %_50.i.i.i = alloca { i64, %"2.std::option::Option<usize>" }, align 8
  %iterator.i.i = alloca %"1.std::env::Args", align 8
  %_6.i.i = alloca %"2.std::option::Option<std::string::String>", align 8
  %_10.i.i = alloca { i64, %"2.std::option::Option<usize>" }, align 8
  %_2 = alloca %"1.std::env::Args", align 8
  %_4 = alloca %"2.std::result::Result<i64, std::num::ParseIntError>", align 8
  %_12 = alloca %"2.std::fmt::Arguments", align 8
  %_17 = alloca [1 x %"2.std::fmt::ArgumentV1"], align 8
  %_20 = alloca i64, align 8
  %0 = bitcast %"1.std::env::Args"* %_2 to i8*
  call void @llvm.lifetime.start(i64 32, i8* %0)
  call void @_ZN3std3env4args17hff4915f936ee80ddE(%"1.std::env::Args"* noalias nocapture nonnull sret dereferenceable(32) %_2)
  %1 = bitcast %"1.std::env::Args"* %iterator.i.i to i8*
  call void @llvm.lifetime.start(i64 32, i8* %1), !noalias !9
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* %0, i64 32, i32 8, i1 false), !noalias !16
  %2 = bitcast %"2.std::option::Option<std::string::String>"* %_6.i.i to i8*
  call void @llvm.lifetime.start(i64 24, i8* %2), !noalias !9
  invoke void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$4next17hb559a6d0c64a0febE"(%"2.std::option::Option<std::string::String>"* noalias nocapture nonnull sret dereferenceable(24) %_6.i.i, %"1.std::env::Args"* nonnull dereferenceable(32) %iterator.i.i)
          to label %bb4.i.i unwind label %bb3.thread163.i.i, !noalias !9

bb1:                                              ; preds = %cleanup.body.thread, %bb6.i.i.i.i23, %normal-return.i22
  %eh.lpad-body40 = phi { i8*, i32 } [ %.fca.1.insert.i.i, %cleanup.body.thread ], [ %lpad.phi55, %bb6.i.i.i.i23 ], [ %lpad.phi55, %normal-return.i22 ]
  resume { i8*, i32 } %eh.lpad-body40

cleanup.body.thread:                              ; preds = %bb9.i.i.i.i.i62.i.i, %bb6.i.i.i.i.i.i.i63.i.i, %normal-return.i70.i.i, %bb6.i.i.i.i71.i.i
  %personalityslot.sroa.8.1161177.i.i = phi i32 [ %personalityslot.sroa.8.1161.ph.i.i, %bb6.i.i.i.i.i.i.i63.i.i ], [ %personalityslot.sroa.8.1161.ph.i.i, %bb9.i.i.i.i.i62.i.i ], [ %personalityslot.sroa.9.2.ph.i.i.i, %bb6.i.i.i.i71.i.i ], [ %personalityslot.sroa.9.2.ph.i.i.i, %normal-return.i70.i.i ]
  %personalityslot.sroa.0.1162175.i.i = phi i8* [ %personalityslot.sroa.0.1162.ph.i.i, %bb6.i.i.i.i.i.i.i63.i.i ], [ %personalityslot.sroa.0.1162.ph.i.i, %bb9.i.i.i.i.i62.i.i ], [ %personalityslot.sroa.0.2.ph.i.i.i, %bb6.i.i.i.i71.i.i ], [ %personalityslot.sroa.0.2.ph.i.i.i, %normal-return.i70.i.i ]
  %.fca.0.insert.i.i = insertvalue { i8*, i32 } undef, i8* %personalityslot.sroa.0.1162175.i.i, 0
  %.fca.1.insert.i.i = insertvalue { i8*, i32 } %.fca.0.insert.i.i, i32 %personalityslot.sroa.8.1161177.i.i, 1
  br label %bb1

bb4.i.i:                                          ; preds = %entry-block
  %3 = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_6.i.i, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %4 = load i8*, i8** %3, align 8, !noalias !9
  %switchtmp.i.i = icmp eq i8* %4, null
  br i1 %switchtmp.i.i, label %bb10.i.i, label %bb7.i.i

bb7.i.i:                                          ; preds = %bb4.i.i
  %element.sroa.6.0..sroa_idx118.i.i = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_6.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %element.sroa.6.0.copyload.i.i = load i64, i64* %element.sroa.6.0..sroa_idx118.i.i, align 8, !noalias !9
  %element.sroa.7.0..sroa_idx123.i.i = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_6.i.i, i64 0, i32 0, i32 0, i32 1
  %element.sroa.7.0.copyload.i.i = load i64, i64* %element.sroa.7.0..sroa_idx123.i.i, align 8, !noalias !9
  %5 = bitcast { i64, %"2.std::option::Option<usize>" }* %_10.i.i to i8*
  call void @llvm.lifetime.start(i64 24, i8* %5), !noalias !9
  invoke void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$9size_hint17h23736c8fea892246E"({ i64, %"2.std::option::Option<usize>" }* noalias nocapture nonnull sret dereferenceable(24) %_10.i.i, %"1.std::env::Args"* noalias nonnull readonly dereferenceable(32) %iterator.i.i)
          to label %bb13.i.i unwind label %cleanup3.i.i, !noalias !9

bb10.i.i:                                         ; preds = %bb4.i.i
  call void @llvm.lifetime.end(i64 24, i8* nonnull %2), !noalias !9
  %6 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 2
  %7 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %6, align 8, !noalias !17
  %8 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 3
  %9 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %8, align 8, !noalias !17
  %10 = icmp eq %"1.std::ffi::OsString"* %7, %9
  br i1 %10, label %bb9.i.i.i.i.i.i.i, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.preheader"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.preheader": ; preds = %bb10.i.i
  br label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i": ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.preheader", %bb3.backedge.i.i.i.i.i.i.i
  %11 = phi %"1.std::ffi::OsString"* [ %14, %bb3.backedge.i.i.i.i.i.i.i ], [ %9, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.preheader" ]
  %12 = phi %"1.std::ffi::OsString"* [ %15, %bb3.backedge.i.i.i.i.i.i.i ], [ %7, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.preheader" ]
  %13 = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %12, i64 1
  store %"1.std::ffi::OsString"* %13, %"1.std::ffi::OsString"** %6, align 8, !noalias !17
  %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %12, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %_10.sroa.0.0.copyload.i.i.i.i.i.i.i = load i8*, i8** %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i.i.i, align 8, !noalias !9
  %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %12, i64 0, i32 0, i32 0, i32 0, i32 1
  %_10.sroa.11.0.copyload.i.i.i.i.i.i.i = load i64, i64* %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i.i.i, align 8, !noalias !9
  %switch3tmp.i.i.i.i.i.i.i = icmp eq i8* %_10.sroa.0.0.copyload.i.i.i.i.i.i.i, null
  br i1 %switch3tmp.i.i.i.i.i.i.i, label %bb9.i.i.i.i.i.i.i.loopexit, label %bb8.i.i.i.i.i.i.i

bb8.i.i.i.i.i.i.i:                                ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i"
  %not..i.i.i.i.i.i.i.i.i.i.i.i.i = icmp eq i64 %_10.sroa.11.0.copyload.i.i.i.i.i.i.i, 0
  br i1 %not..i.i.i.i.i.i.i.i.i.i.i.i.i, label %bb3.backedge.i.i.i.i.i.i.i, label %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i

bb3.backedge.i.i.i.i.i.i.i:                       ; preds = %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i, %bb8.i.i.i.i.i.i.i
  %14 = phi %"1.std::ffi::OsString"* [ %11, %bb8.i.i.i.i.i.i.i ], [ %.pre49.i.i.i.i.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i ]
  %15 = phi %"1.std::ffi::OsString"* [ %13, %bb8.i.i.i.i.i.i.i ], [ %.pre.i.i.i.i.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i ]
  %16 = icmp eq %"1.std::ffi::OsString"* %15, %14
  br i1 %16, label %bb9.i.i.i.i.i.i.i.loopexit, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i"

bb6.i.i.i.i.i.i.i.i.i.i.i.i.i:                    ; preds = %bb8.i.i.i.i.i.i.i
  call void @__rust_deallocate(i8* nonnull %_10.sroa.0.0.copyload.i.i.i.i.i.i.i, i64 %_10.sroa.11.0.copyload.i.i.i.i.i.i.i, i64 1) #5, !noalias !9
  %.pre.i.i.i.i.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %6, align 8, !noalias !17
  %.pre49.i.i.i.i.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %8, align 8, !noalias !17
  br label %bb3.backedge.i.i.i.i.i.i.i

bb9.i.i.i.i.i.i.i.loopexit:                       ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i", %bb3.backedge.i.i.i.i.i.i.i
  br label %bb9.i.i.i.i.i.i.i

bb9.i.i.i.i.i.i.i:                                ; preds = %bb9.i.i.i.i.i.i.i.loopexit, %bb10.i.i
  %17 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %18 = load i64, i64* %17, align 8, !noalias !9
  %not..i.i.i.i.i.i.i.i.i = icmp eq i64 %18, 0
  br i1 %not..i.i.i.i.i.i.i.i.i, label %bb3.thread, label %bb6.i.i.i.i.i.i.i.i.i

bb6.i.i.i.i.i.i.i.i.i:                            ; preds = %bb9.i.i.i.i.i.i.i
  %19 = bitcast %"1.std::env::Args"* %iterator.i.i to i8**
  %20 = load i8*, i8** %19, align 8, !noalias !9
  %21 = mul i64 %18, 24
  call void @__rust_deallocate(i8* %20, i64 %21, i64 8) #5, !noalias !9
  br label %bb3.thread

bb3.thread:                                       ; preds = %bb9.i.i.i.i.i.i.i, %bb6.i.i.i.i.i.i.i.i.i
  call void @llvm.lifetime.end(i64 32, i8* nonnull %1), !noalias !9
  call void @llvm.lifetime.end(i64 32, i8* %0)
  %22 = getelementptr inbounds %"2.std::result::Result<i64, std::num::ParseIntError>", %"2.std::result::Result<i64, std::num::ParseIntError>"* %_4, i64 0, i32 0
  call void @llvm.lifetime.start(i64 16, i8* %22)
  br label %panic.i

bb13.i.i:                                         ; preds = %bb7.i.i
  %23 = getelementptr inbounds { i64, %"2.std::option::Option<usize>" }, { i64, %"2.std::option::Option<usize>" }* %_10.i.i, i64 0, i32 0
  %24 = load i64, i64* %23, align 8, !noalias !9
  call void @llvm.lifetime.end(i64 24, i8* %5), !noalias !9
  %25 = call { i64, i1 } @llvm.uadd.with.overflow.i64(i64 %24, i64 1) #5
  %26 = extractvalue { i64, i1 } %25, 1
  %27 = extractvalue { i64, i1 } %25, 0
  %..i.i.i = select i1 %26, i64 -1, i64 %27
  %28 = call { i64, i1 } @llvm.umul.with.overflow.i64(i64 %..i.i.i, i64 24) #5
  %29 = extractvalue { i64, i1 } %28, 1
  br i1 %29, label %bb3.i3.i.i.i.i, label %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit.i.i.i.i"

bb3.i3.i.i.i.i:                                   ; preds = %bb13.i.i
  invoke void @_ZN4core6option13expect_failed17h530ede3d41450938E(i8* noalias nonnull readonly getelementptr inbounds ([17 x i8], [17 x i8]* @str.0, i64 0, i64 0), i64 17)
          to label %.noexc.i.i unwind label %cleanup4.i.i, !noalias !9

.noexc.i.i:                                       ; preds = %bb3.i3.i.i.i.i
  unreachable

"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit.i.i.i.i": ; preds = %bb13.i.i
  %30 = extractvalue { i64, i1 } %28, 0
  %31 = icmp eq i64 %30, 0
  br i1 %31, label %bb18.i.i, label %bb6.i.i.i.i

bb6.i.i.i.i:                                      ; preds = %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit.i.i.i.i"
  %32 = call i8* @__rust_allocate(i64 %30, i64 8) #5, !noalias !22
  %33 = icmp eq i8* %32, null
  br i1 %33, label %bb10.i.i.i.i, label %bb18.i.i

bb10.i.i.i.i:                                     ; preds = %bb6.i.i.i.i
  invoke void @_ZN5alloc3oom3oom17hcac7549e1d9cf2ccE()
          to label %.noexc44.i.i unwind label %cleanup4.i.i, !noalias !9

.noexc44.i.i:                                     ; preds = %bb10.i.i.i.i
  unreachable

bb18.i.i:                                         ; preds = %bb6.i.i.i.i, %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit.i.i.i.i"
  %ptr.0.i.i.i.i = phi i8* [ inttoptr (i64 1 to i8*), %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit.i.i.i.i" ], [ %32, %bb6.i.i.i.i ]
  %34 = ptrtoint i8* %ptr.0.i.i.i.i to i64
  %_23.sroa.0.sroa.0.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx.i.i = bitcast i8* %ptr.0.i.i.i.i to i8**
  store i8* %4, i8** %_23.sroa.0.sroa.0.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx.i.i, align 8, !noalias !9
  %_23.sroa.0.sroa.4.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx129.i.i = getelementptr inbounds i8, i8* %ptr.0.i.i.i.i, i64 8
  %35 = bitcast i8* %_23.sroa.0.sroa.4.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx129.i.i to i64*
  store i64 %element.sroa.6.0.copyload.i.i, i64* %35, align 8, !noalias !9
  %_23.sroa.0.sroa.5.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx131.i.i = getelementptr inbounds i8, i8* %ptr.0.i.i.i.i, i64 16
  %36 = bitcast i8* %_23.sroa.0.sroa.5.0._23.sroa.0.0._8.sroa.0.0..sroa_cast2.i.sroa_cast.sroa_idx131.i.i to i64*
  store i64 %element.sroa.7.0.copyload.i.i, i64* %36, align 8, !noalias !9
  call void @llvm.lifetime.end(i64 24, i8* nonnull %2), !noalias !9
  %37 = bitcast %"1.std::env::Args"* %iterator.i.i.i to i8*
  call void @llvm.lifetime.start(i64 32, i8* %37), !noalias !27
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %37, i8* %1, i64 32, i32 8, i1 false), !noalias !9
  %38 = bitcast %"2.std::option::Option<std::string::String>"* %_39.i.i.i to i8*
  %39 = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_39.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %element1.sroa.6.0..sroa_idx133.i.i.i = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_39.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %element1.sroa.7.0..sroa_idx138.i.i.i = getelementptr inbounds %"2.std::option::Option<std::string::String>", %"2.std::option::Option<std::string::String>"* %_39.i.i.i, i64 0, i32 0, i32 0, i32 1
  %40 = bitcast { i64, %"2.std::option::Option<usize>" }* %_50.i.i.i to i8*
  %41 = getelementptr inbounds { i64, %"2.std::option::Option<usize>" }, { i64, %"2.std::option::Option<usize>" }* %_50.i.i.i, i64 0, i32 0
  br label %bb25.i.i.i

bb25.i.i.i:                                       ; preds = %bb41.i.i.i, %bb18.i.i
  %vector.sroa.0.1.i.i = phi i64 [ %34, %bb18.i.i ], [ %vector.sroa.0.3.i.i, %bb41.i.i.i ]
  %vector.sroa.10.1.i.i = phi i64 [ %..i.i.i, %bb18.i.i ], [ %vector.sroa.10.3.i.i, %bb41.i.i.i ]
  %vector.sroa.15.1.i.i = phi i64 [ 1, %bb18.i.i ], [ %65, %bb41.i.i.i ]
  call void @llvm.lifetime.start(i64 24, i8* %38), !noalias !27
  invoke void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$4next17hb559a6d0c64a0febE"(%"2.std::option::Option<std::string::String>"* noalias nocapture nonnull sret dereferenceable(24) %_39.i.i.i, %"1.std::env::Args"* nonnull dereferenceable(32) %iterator.i.i.i)
          to label %bb26.i.i.i unwind label %cleanup.i.i.i, !noalias !27

bb26.i.i.i:                                       ; preds = %bb25.i.i.i
  %42 = load i8*, i8** %39, align 8, !noalias !27
  %switch9tmp.i.i.i = icmp eq i8* %42, null
  br i1 %switch9tmp.i.i.i, label %bb42.i.i.i, label %bb31.i.i.i

bb31.i.i.i:                                       ; preds = %bb26.i.i.i
  %element1.sroa.6.0.copyload.i.i.i = load i64, i64* %element1.sroa.6.0..sroa_idx133.i.i.i, align 8, !noalias !27
  %element1.sroa.7.0.copyload.i.i.i = load i64, i64* %element1.sroa.7.0..sroa_idx138.i.i.i, align 8, !noalias !27
  %43 = icmp eq i64 %vector.sroa.10.1.i.i, %vector.sroa.15.1.i.i
  br i1 %43, label %bb32.i.i.i, label %bb41.i.i.i

bb32.i.i.i:                                       ; preds = %bb31.i.i.i
  call void @llvm.lifetime.start(i64 24, i8* %40), !noalias !27
  invoke void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$9size_hint17h23736c8fea892246E"({ i64, %"2.std::option::Option<usize>" }* noalias nocapture nonnull sret dereferenceable(24) %_50.i.i.i, %"1.std::env::Args"* noalias nonnull readonly dereferenceable(32) %iterator.i.i.i)
          to label %bb35.i.i.i unwind label %bb56.i.loopexit.i.i, !noalias !27

bb35.i.i.i:                                       ; preds = %bb32.i.i.i
  %44 = load i64, i64* %41, align 8, !noalias !27
  call void @llvm.lifetime.end(i64 24, i8* %40), !noalias !27
  %45 = call { i64, i1 } @llvm.uadd.with.overflow.i64(i64 %44, i64 1) #5
  %46 = extractvalue { i64, i1 } %45, 1
  %47 = extractvalue { i64, i1 } %45, 0
  %..i.i.i.i = select i1 %46, i64 -1, i64 %47
  %48 = icmp eq i64 %..i.i.i.i, 0
  br i1 %48, label %bb41.i.i.i, label %bb6.i.i81.i.i

bb6.i.i81.i.i:                                    ; preds = %bb35.i.i.i
  %49 = call { i64, i1 } @llvm.uadd.with.overflow.i64(i64 %vector.sroa.10.1.i.i, i64 %..i.i.i.i) #5
  %50 = extractvalue { i64, i1 } %49, 1
  br i1 %50, label %bb3.i7.i.i.i.i.i, label %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit10.i.i.i.i.i"

bb3.i7.i.i.i.i.i:                                 ; preds = %bb6.i.i81.i.i
  invoke void @_ZN4core6option13expect_failed17h530ede3d41450938E(i8* noalias nonnull readonly getelementptr inbounds ([17 x i8], [17 x i8]* @str.0, i64 0, i64 0), i64 17)
          to label %.noexc84.i.i unwind label %bb56.i.loopexit.split-lp.i.i, !noalias !9

.noexc84.i.i:                                     ; preds = %bb3.i7.i.i.i.i.i
  unreachable

"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit10.i.i.i.i.i": ; preds = %bb6.i.i81.i.i
  %51 = extractvalue { i64, i1 } %49, 0
  %52 = shl i64 %vector.sroa.10.1.i.i, 1
  %53 = icmp uge i64 %51, %52
  %_0.0.sroa.speculated.i.i.i.i.i.i = select i1 %53, i64 %51, i64 %52
  %54 = call { i64, i1 } @llvm.umul.with.overflow.i64(i64 %_0.0.sroa.speculated.i.i.i.i.i.i, i64 24) #5
  %55 = extractvalue { i64, i1 } %54, 1
  br i1 %55, label %bb3.i.i.i.i.i.i, label %"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$18amortized_new_size17he0667a697ebd04e1E.exit.i.i.i.i"

bb3.i.i.i.i.i.i:                                  ; preds = %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit10.i.i.i.i.i"
  invoke void @_ZN4core6option13expect_failed17h530ede3d41450938E(i8* noalias nonnull readonly getelementptr inbounds ([17 x i8], [17 x i8]* @str.0, i64 0, i64 0), i64 17)
          to label %.noexc85.i.i unwind label %bb56.i.loopexit.split-lp.i.i, !noalias !9

.noexc85.i.i:                                     ; preds = %bb3.i.i.i.i.i.i
  unreachable

"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$18amortized_new_size17he0667a697ebd04e1E.exit.i.i.i.i": ; preds = %"_ZN38_$LT$core..option..Option$LT$T$GT$$GT$6expect17h6984dbe86b5dbc8aE.exit10.i.i.i.i.i"
  %56 = extractvalue { i64, i1 } %54, 0
  %57 = icmp eq i64 %vector.sroa.10.1.i.i, 0
  br i1 %57, label %bb10.i.i82.i.i, label %bb11.i.i.i.i

bb10.i.i82.i.i:                                   ; preds = %"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$18amortized_new_size17he0667a697ebd04e1E.exit.i.i.i.i"
  %58 = call i8* @__rust_allocate(i64 %56, i64 8) #5, !noalias !27
  br label %bb15.i.i.i.i

bb11.i.i.i.i:                                     ; preds = %"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$18amortized_new_size17he0667a697ebd04e1E.exit.i.i.i.i"
  %59 = inttoptr i64 %vector.sroa.0.1.i.i to i8*
  %60 = mul i64 %vector.sroa.10.1.i.i, 24
  %61 = call i8* @__rust_reallocate(i8* %59, i64 %60, i64 %56, i64 8) #5, !noalias !27
  br label %bb15.i.i.i.i

bb15.i.i.i.i:                                     ; preds = %bb11.i.i.i.i, %bb10.i.i82.i.i
  %ptr.0.i.i83.i.i = phi i8* [ %58, %bb10.i.i82.i.i ], [ %61, %bb11.i.i.i.i ]
  %62 = icmp eq i8* %ptr.0.i.i83.i.i, null
  br i1 %62, label %bb17.i.i.i.i, label %bb18.i.i.i.i

bb17.i.i.i.i:                                     ; preds = %bb15.i.i.i.i
  invoke void @_ZN5alloc3oom3oom17hcac7549e1d9cf2ccE()
          to label %.noexc86.i.i unwind label %bb56.i.loopexit.split-lp.i.i, !noalias !9

.noexc86.i.i:                                     ; preds = %bb17.i.i.i.i
  unreachable

bb18.i.i.i.i:                                     ; preds = %bb15.i.i.i.i
  %63 = ptrtoint i8* %ptr.0.i.i83.i.i to i64
  br label %bb41.i.i.i

bb41.i.i.i:                                       ; preds = %bb18.i.i.i.i, %bb35.i.i.i, %bb31.i.i.i
  %vector.sroa.0.3.i.i = phi i64 [ %vector.sroa.0.1.i.i, %bb31.i.i.i ], [ %63, %bb18.i.i.i.i ], [ %vector.sroa.0.1.i.i, %bb35.i.i.i ]
  %vector.sroa.10.3.i.i = phi i64 [ %vector.sroa.10.1.i.i, %bb31.i.i.i ], [ %_0.0.sroa.speculated.i.i.i.i.i.i, %bb18.i.i.i.i ], [ %vector.sroa.10.1.i.i, %bb35.i.i.i ]
  %64 = inttoptr i64 %vector.sroa.0.3.i.i to %"3.std::string::String"*
  %_64.sroa.0.sroa.0.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx.i.i.i = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %64, i64 %vector.sroa.15.1.i.i, i32 0, i32 0, i32 0, i32 0, i32 0
  store i8* %42, i8** %_64.sroa.0.sroa.0.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx.i.i.i, align 8, !noalias !27
  %_64.sroa.0.sroa.4.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx143.i.i.i = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %64, i64 %vector.sroa.15.1.i.i, i32 0, i32 0, i32 1
  store i64 %element1.sroa.6.0.copyload.i.i.i, i64* %_64.sroa.0.sroa.4.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx143.i.i.i, align 8, !noalias !27
  %_64.sroa.0.sroa.5.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx145.i.i.i = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %64, i64 %vector.sroa.15.1.i.i, i32 0, i32 1
  store i64 %element1.sroa.7.0.copyload.i.i.i, i64* %_64.sroa.0.sroa.5.0._64.sroa.0.0._8.sroa.0.0..sroa_cast2.i123.sroa_cast.sroa_idx145.i.i.i, align 8, !noalias !27
  %65 = add i64 %vector.sroa.15.1.i.i, 1
  call void @llvm.lifetime.end(i64 24, i8* nonnull %38), !noalias !27
  br label %bb25.i.i.i

bb42.i.i.i:                                       ; preds = %bb26.i.i.i
  call void @llvm.lifetime.end(i64 24, i8* nonnull %38), !noalias !27
  %66 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 2
  %67 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %66, align 8, !noalias !30
  %68 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 3
  %69 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %68, align 8, !noalias !30
  %70 = icmp eq %"1.std::ffi::OsString"* %67, %69
  br i1 %70, label %bb9.i.i.i.i.i98.i.i.i, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i.preheader"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i.preheader": ; preds = %bb42.i.i.i
  br label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i": ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i.preheader", %bb3.backedge.i.i.i.i.i93.i.i.i
  %71 = phi %"1.std::ffi::OsString"* [ %74, %bb3.backedge.i.i.i.i.i93.i.i.i ], [ %69, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i.preheader" ]
  %72 = phi %"1.std::ffi::OsString"* [ %75, %bb3.backedge.i.i.i.i.i93.i.i.i ], [ %67, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i.preheader" ]
  %73 = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %72, i64 1
  store %"1.std::ffi::OsString"* %73, %"1.std::ffi::OsString"** %66, align 8, !noalias !30
  %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i85.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %72, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %_10.sroa.0.0.copyload.i.i.i.i.i86.i.i.i = load i8*, i8** %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i85.i.i.i, align 8, !noalias !27
  %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i87.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %72, i64 0, i32 0, i32 0, i32 0, i32 1
  %_10.sroa.11.0.copyload.i.i.i.i.i88.i.i.i = load i64, i64* %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i87.i.i.i, align 8, !noalias !27
  %switch3tmp.i.i.i.i.i89.i.i.i = icmp eq i8* %_10.sroa.0.0.copyload.i.i.i.i.i86.i.i.i, null
  br i1 %switch3tmp.i.i.i.i.i89.i.i.i, label %bb9.i.i.i.i.i98.i.i.i.loopexit, label %bb8.i.i.i.i.i92.i.i.i

bb8.i.i.i.i.i92.i.i.i:                            ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i"
  %not..i.i.i.i.i.i.i.i.i.i.i91.i.i.i = icmp eq i64 %_10.sroa.11.0.copyload.i.i.i.i.i88.i.i.i, 0
  br i1 %not..i.i.i.i.i.i.i.i.i.i.i91.i.i.i, label %bb3.backedge.i.i.i.i.i93.i.i.i, label %bb6.i.i.i.i.i.i.i.i.i.i.i96.i.i.i

bb3.backedge.i.i.i.i.i93.i.i.i:                   ; preds = %bb6.i.i.i.i.i.i.i.i.i.i.i96.i.i.i, %bb8.i.i.i.i.i92.i.i.i
  %74 = phi %"1.std::ffi::OsString"* [ %71, %bb8.i.i.i.i.i92.i.i.i ], [ %.pre49.i.i.i.i.i95.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i96.i.i.i ]
  %75 = phi %"1.std::ffi::OsString"* [ %73, %bb8.i.i.i.i.i92.i.i.i ], [ %.pre.i.i.i.i.i94.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i96.i.i.i ]
  %76 = icmp eq %"1.std::ffi::OsString"* %75, %74
  br i1 %76, label %bb9.i.i.i.i.i98.i.i.i.loopexit, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i"

bb6.i.i.i.i.i.i.i.i.i.i.i96.i.i.i:                ; preds = %bb8.i.i.i.i.i92.i.i.i
  call void @__rust_deallocate(i8* nonnull %_10.sroa.0.0.copyload.i.i.i.i.i86.i.i.i, i64 %_10.sroa.11.0.copyload.i.i.i.i.i88.i.i.i, i64 1) #5, !noalias !27
  %.pre.i.i.i.i.i94.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %66, align 8, !noalias !30
  %.pre49.i.i.i.i.i95.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %68, align 8, !noalias !30
  br label %bb3.backedge.i.i.i.i.i93.i.i.i

bb9.i.i.i.i.i98.i.i.i.loopexit:                   ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i90.i.i.i", %bb3.backedge.i.i.i.i.i93.i.i.i
  br label %bb9.i.i.i.i.i98.i.i.i

bb9.i.i.i.i.i98.i.i.i:                            ; preds = %bb9.i.i.i.i.i98.i.i.i.loopexit, %bb42.i.i.i
  %77 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %78 = load i64, i64* %77, align 8, !noalias !27
  %not..i.i.i.i.i.i.i97.i.i.i = icmp eq i64 %78, 0
  br i1 %not..i.i.i.i.i.i.i97.i.i.i, label %bb3, label %bb6.i.i.i.i.i.i.i99.i.i.i

bb6.i.i.i.i.i.i.i99.i.i.i:                        ; preds = %bb9.i.i.i.i.i98.i.i.i
  %79 = bitcast %"1.std::env::Args"* %iterator.i.i.i to i8**
  %80 = load i8*, i8** %79, align 8, !noalias !27
  %81 = mul i64 %78, 24
  call void @__rust_deallocate(i8* %80, i64 %81, i64 8) #5, !noalias !27
  br label %bb3

bb45.i.i.i:                                       ; preds = %cleanup.i.i.i, %bb6.i.i.i.i.i.i48.i.i, %bb56.i.i.i
  %vector.sroa.15.1227.i.i = phi i64 [ %vector.sroa.15.1.i.i, %cleanup.i.i.i ], [ %vector.sroa.10.1.i.i, %bb6.i.i.i.i.i.i48.i.i ], [ %vector.sroa.10.1.i.i, %bb56.i.i.i ]
  %personalityslot.sroa.9.2.ph.i.i.i = phi i32 [ %.fca.1.extract.i.i.i, %cleanup.i.i.i ], [ %.fca.1.extract31.i.i.i, %bb6.i.i.i.i.i.i48.i.i ], [ %.fca.1.extract31.i.i.i, %bb56.i.i.i ]
  %personalityslot.sroa.0.2.ph.i.i.i = phi i8* [ %.fca.0.extract.i.i.i, %cleanup.i.i.i ], [ %.fca.0.extract29.i.i.i, %bb6.i.i.i.i.i.i48.i.i ], [ %.fca.0.extract29.i.i.i, %bb56.i.i.i ]
  %82 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 2
  %83 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %82, align 8, !noalias !35
  %84 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 3
  %85 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %84, align 8, !noalias !35
  %86 = icmp eq %"1.std::ffi::OsString"* %83, %85
  br i1 %86, label %bb9.i.i.i.i.i.i.i.i, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i.preheader"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i.preheader": ; preds = %bb45.i.i.i
  br label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i": ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i.preheader", %bb3.backedge.i.i.i.i.i.i.i.i
  %87 = phi %"1.std::ffi::OsString"* [ %90, %bb3.backedge.i.i.i.i.i.i.i.i ], [ %85, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i.preheader" ]
  %88 = phi %"1.std::ffi::OsString"* [ %91, %bb3.backedge.i.i.i.i.i.i.i.i ], [ %83, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i.preheader" ]
  %89 = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %88, i64 1
  store %"1.std::ffi::OsString"* %89, %"1.std::ffi::OsString"** %82, align 8, !noalias !35
  %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %88, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %_10.sroa.0.0.copyload.i.i.i.i.i.i.i.i = load i8*, i8** %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i.i.i.i, align 8, !noalias !27
  %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i.i.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %88, i64 0, i32 0, i32 0, i32 0, i32 1
  %_10.sroa.11.0.copyload.i.i.i.i.i.i.i.i = load i64, i64* %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i.i.i.i, align 8, !noalias !27
  %switch3tmp.i.i.i.i.i.i.i.i = icmp eq i8* %_10.sroa.0.0.copyload.i.i.i.i.i.i.i.i, null
  br i1 %switch3tmp.i.i.i.i.i.i.i.i, label %bb9.i.i.i.i.i.i.i.i.loopexit, label %bb8.i.i.i.i.i.i.i.i

bb8.i.i.i.i.i.i.i.i:                              ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i"
  %not..i.i.i.i.i.i.i.i.i.i.i.i.i.i = icmp eq i64 %_10.sroa.11.0.copyload.i.i.i.i.i.i.i.i, 0
  br i1 %not..i.i.i.i.i.i.i.i.i.i.i.i.i.i, label %bb3.backedge.i.i.i.i.i.i.i.i, label %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i.i

bb3.backedge.i.i.i.i.i.i.i.i:                     ; preds = %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i.i, %bb8.i.i.i.i.i.i.i.i
  %90 = phi %"1.std::ffi::OsString"* [ %87, %bb8.i.i.i.i.i.i.i.i ], [ %.pre49.i.i.i.i.i.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i.i ]
  %91 = phi %"1.std::ffi::OsString"* [ %89, %bb8.i.i.i.i.i.i.i.i ], [ %.pre.i.i.i.i.i.i.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i.i.i.i ]
  %92 = icmp eq %"1.std::ffi::OsString"* %91, %90
  br i1 %92, label %bb9.i.i.i.i.i.i.i.i.loopexit, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i"

bb6.i.i.i.i.i.i.i.i.i.i.i.i.i.i:                  ; preds = %bb8.i.i.i.i.i.i.i.i
  call void @__rust_deallocate(i8* nonnull %_10.sroa.0.0.copyload.i.i.i.i.i.i.i.i, i64 %_10.sroa.11.0.copyload.i.i.i.i.i.i.i.i, i64 1) #5, !noalias !27
  %.pre.i.i.i.i.i.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %82, align 8, !noalias !35
  %.pre49.i.i.i.i.i.i.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %84, align 8, !noalias !35
  br label %bb3.backedge.i.i.i.i.i.i.i.i

bb9.i.i.i.i.i.i.i.i.loopexit:                     ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i.i.i.i", %bb3.backedge.i.i.i.i.i.i.i.i
  br label %bb9.i.i.i.i.i.i.i.i

bb9.i.i.i.i.i.i.i.i:                              ; preds = %bb9.i.i.i.i.i.i.i.i.loopexit, %bb45.i.i.i
  %93 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %94 = load i64, i64* %93, align 8, !noalias !27
  %not..i.i.i.i.i.i.i.i45.i.i = icmp eq i64 %94, 0
  br i1 %not..i.i.i.i.i.i.i.i45.i.i, label %bb23.i.i, label %bb6.i.i.i.i.i.i.i.i46.i.i

bb6.i.i.i.i.i.i.i.i46.i.i:                        ; preds = %bb9.i.i.i.i.i.i.i.i
  %95 = bitcast %"1.std::env::Args"* %iterator.i.i.i to i8**
  %96 = load i8*, i8** %95, align 8, !noalias !27
  %97 = mul i64 %94, 24
  call void @__rust_deallocate(i8* %96, i64 %97, i64 8) #5, !noalias !27
  br label %bb23.i.i

bb56.i.loopexit.i.i:                              ; preds = %bb32.i.i.i
  %lpad.loopexit.i.i = landingpad { i8*, i32 }
          cleanup
  br label %bb56.i.i.i

bb56.i.loopexit.split-lp.i.i:                     ; preds = %bb17.i.i.i.i, %bb3.i.i.i.i.i.i, %bb3.i7.i.i.i.i.i
  %lpad.loopexit.split-lp.i.i = landingpad { i8*, i32 }
          cleanup
  br label %bb56.i.i.i

bb56.i.i.i:                                       ; preds = %bb56.i.loopexit.split-lp.i.i, %bb56.i.loopexit.i.i
  %lpad.phi.i.i = phi { i8*, i32 } [ %lpad.loopexit.i.i, %bb56.i.loopexit.i.i ], [ %lpad.loopexit.split-lp.i.i, %bb56.i.loopexit.split-lp.i.i ]
  %.fca.0.extract29.i.i.i = extractvalue { i8*, i32 } %lpad.phi.i.i, 0
  %.fca.1.extract31.i.i.i = extractvalue { i8*, i32 } %lpad.phi.i.i, 1
  %not..i.i.i.i.i.i47.i.i = icmp eq i64 %element1.sroa.6.0.copyload.i.i.i, 0
  br i1 %not..i.i.i.i.i.i47.i.i, label %bb45.i.i.i, label %bb6.i.i.i.i.i.i48.i.i

bb6.i.i.i.i.i.i48.i.i:                            ; preds = %bb56.i.i.i
  call void @__rust_deallocate(i8* nonnull %42, i64 %element1.sroa.6.0.copyload.i.i.i, i64 1) #5, !noalias !27
  br label %bb45.i.i.i

cleanup.i.i.i:                                    ; preds = %bb25.i.i.i
  %98 = landingpad { i8*, i32 }
          cleanup
  %.fca.0.extract.i.i.i = extractvalue { i8*, i32 } %98, 0
  %.fca.1.extract.i.i.i = extractvalue { i8*, i32 } %98, 1
  br label %bb45.i.i.i

bb21.i.i:                                         ; preds = %bb3.thread163.i.i, %bb6.i.i.i.i.i.i.i, %bb27.i.i
  %personalityslot.sroa.0.1162.ph.i.i = phi i8* [ %.fca.0.extract8164.i.i, %bb3.thread163.i.i ], [ %.fca.0.extract12.i.i, %bb6.i.i.i.i.i.i.i ], [ %.fca.0.extract12.i.i, %bb27.i.i ]
  %personalityslot.sroa.8.1161.ph.i.i = phi i32 [ %.fca.1.extract10165.i.i, %bb3.thread163.i.i ], [ %.fca.1.extract14.i.i, %bb6.i.i.i.i.i.i.i ], [ %.fca.1.extract14.i.i, %bb27.i.i ]
  %99 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 2
  %100 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %99, align 8, !noalias !40
  %101 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 3
  %102 = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %101, align 8, !noalias !40
  %103 = icmp eq %"1.std::ffi::OsString"* %100, %102
  br i1 %103, label %bb9.i.i.i.i.i62.i.i, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i.preheader"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i.preheader": ; preds = %bb21.i.i
  br label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i"

"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i": ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i.preheader", %bb3.backedge.i.i.i.i.i57.i.i
  %104 = phi %"1.std::ffi::OsString"* [ %107, %bb3.backedge.i.i.i.i.i57.i.i ], [ %102, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i.preheader" ]
  %105 = phi %"1.std::ffi::OsString"* [ %108, %bb3.backedge.i.i.i.i.i57.i.i ], [ %100, %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i.preheader" ]
  %106 = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %105, i64 1
  store %"1.std::ffi::OsString"* %106, %"1.std::ffi::OsString"** %99, align 8, !noalias !40
  %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i49.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %105, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %_10.sroa.0.0.copyload.i.i.i.i.i50.i.i = load i8*, i8** %_10.sroa.0.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx.i.i.i.i.i49.i.i, align 8, !noalias !9
  %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i51.i.i = getelementptr inbounds %"1.std::ffi::OsString", %"1.std::ffi::OsString"* %105, i64 0, i32 0, i32 0, i32 0, i32 1
  %_10.sroa.11.0.copyload.i.i.i.i.i52.i.i = load i64, i64* %_10.sroa.11.0._20.sroa.0.0.tmp.sroa.0.0..sroa_cast.i.sroa_cast.i.i.sroa_idx41.i.i.i.i.i51.i.i, align 8, !noalias !9
  %switch3tmp.i.i.i.i.i53.i.i = icmp eq i8* %_10.sroa.0.0.copyload.i.i.i.i.i50.i.i, null
  br i1 %switch3tmp.i.i.i.i.i53.i.i, label %bb9.i.i.i.i.i62.i.i.loopexit, label %bb8.i.i.i.i.i56.i.i

bb8.i.i.i.i.i56.i.i:                              ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i"
  %not..i.i.i.i.i.i.i.i.i.i.i55.i.i = icmp eq i64 %_10.sroa.11.0.copyload.i.i.i.i.i52.i.i, 0
  br i1 %not..i.i.i.i.i.i.i.i.i.i.i55.i.i, label %bb3.backedge.i.i.i.i.i57.i.i, label %bb6.i.i.i.i.i.i.i.i.i.i.i60.i.i

bb3.backedge.i.i.i.i.i57.i.i:                     ; preds = %bb6.i.i.i.i.i.i.i.i.i.i.i60.i.i, %bb8.i.i.i.i.i56.i.i
  %107 = phi %"1.std::ffi::OsString"* [ %104, %bb8.i.i.i.i.i56.i.i ], [ %.pre49.i.i.i.i.i59.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i60.i.i ]
  %108 = phi %"1.std::ffi::OsString"* [ %106, %bb8.i.i.i.i.i56.i.i ], [ %.pre.i.i.i.i.i58.i.i, %bb6.i.i.i.i.i.i.i.i.i.i.i60.i.i ]
  %109 = icmp eq %"1.std::ffi::OsString"* %108, %107
  br i1 %109, label %bb9.i.i.i.i.i62.i.i.loopexit, label %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i"

bb6.i.i.i.i.i.i.i.i.i.i.i60.i.i:                  ; preds = %bb8.i.i.i.i.i56.i.i
  call void @__rust_deallocate(i8* nonnull %_10.sroa.0.0.copyload.i.i.i.i.i50.i.i, i64 %_10.sroa.11.0.copyload.i.i.i.i.i52.i.i, i64 1) #5, !noalias !9
  %.pre.i.i.i.i.i58.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %99, align 8, !noalias !40
  %.pre49.i.i.i.i.i59.i.i = load %"1.std::ffi::OsString"*, %"1.std::ffi::OsString"** %101, align 8, !noalias !40
  br label %bb3.backedge.i.i.i.i.i57.i.i

bb9.i.i.i.i.i62.i.i.loopexit:                     ; preds = %"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE.exit.i.i.i.i.i54.i.i", %bb3.backedge.i.i.i.i.i57.i.i
  br label %bb9.i.i.i.i.i62.i.i

bb9.i.i.i.i.i62.i.i:                              ; preds = %bb9.i.i.i.i.i62.i.i.loopexit, %bb21.i.i
  %110 = getelementptr inbounds %"1.std::env::Args", %"1.std::env::Args"* %iterator.i.i, i64 0, i32 0, i32 0, i32 0, i32 1
  %111 = load i64, i64* %110, align 8, !noalias !9
  %not..i.i.i.i.i.i.i61.i.i = icmp eq i64 %111, 0
  br i1 %not..i.i.i.i.i.i.i61.i.i, label %cleanup.body.thread, label %bb6.i.i.i.i.i.i.i63.i.i

bb6.i.i.i.i.i.i.i63.i.i:                          ; preds = %bb9.i.i.i.i.i62.i.i
  %112 = bitcast %"1.std::env::Args"* %iterator.i.i to i8**
  %113 = load i8*, i8** %112, align 8, !noalias !9
  %114 = mul i64 %111, 24
  call void @__rust_deallocate(i8* %113, i64 %114, i64 8) #5, !noalias !9
  br label %cleanup.body.thread

bb23.i.i:                                         ; preds = %bb6.i.i.i.i.i.i.i.i46.i.i, %bb9.i.i.i.i.i.i.i.i
  %115 = inttoptr i64 %vector.sroa.0.1.i.i to %"3.std::string::String"*
  %116 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %115, i64 %vector.sroa.15.1227.i.i
  %117 = icmp eq i64 %vector.sroa.15.1227.i.i, 0
  br i1 %117, label %normal-return.i70.i.i, label %slice_loop_body.i.i.i66.i.i.preheader

slice_loop_body.i.i.i66.i.i.preheader:            ; preds = %bb23.i.i
  br label %slice_loop_body.i.i.i66.i.i

slice_loop_body.i.i.i66.i.i:                      ; preds = %slice_loop_body.i.i.i66.i.i.preheader, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i
  %118 = phi %"3.std::string::String"* [ %123, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i ], [ %115, %slice_loop_body.i.i.i66.i.i.preheader ]
  %119 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %118, i64 0, i32 0, i32 0, i32 1
  %120 = load i64, i64* %119, align 8, !noalias !9
  %not..i.i.i.i.i.i.i.i65.i.i = icmp eq i64 %120, 0
  br i1 %not..i.i.i.i.i.i.i.i65.i.i, label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i, label %bb6.i.i.i.i.i.i.i.i67.i.i

bb6.i.i.i.i.i.i.i.i67.i.i:                        ; preds = %slice_loop_body.i.i.i66.i.i
  %121 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %118, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %122 = load i8*, i8** %121, align 8, !alias.scope !45, !noalias !9, !nonnull !8
  call void @__rust_deallocate(i8* nonnull %122, i64 %120, i64 1) #5, !noalias !9
  br label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i

_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i:    ; preds = %bb6.i.i.i.i.i.i.i.i67.i.i, %slice_loop_body.i.i.i66.i.i
  %123 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %118, i64 1
  %124 = icmp eq %"3.std::string::String"* %123, %116
  br i1 %124, label %normal-return.i70.i.i.loopexit, label %slice_loop_body.i.i.i66.i.i

normal-return.i70.i.i.loopexit:                   ; preds = %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i68.i.i
  br label %normal-return.i70.i.i

normal-return.i70.i.i:                            ; preds = %normal-return.i70.i.i.loopexit, %bb23.i.i
  %not..i.i.i.i69.i.i = icmp eq i64 %vector.sroa.10.1.i.i, 0
  br i1 %not..i.i.i.i69.i.i, label %cleanup.body.thread, label %bb6.i.i.i.i71.i.i

bb6.i.i.i.i71.i.i:                                ; preds = %normal-return.i70.i.i
  %125 = mul i64 %vector.sroa.10.1.i.i, 24
  %126 = inttoptr i64 %vector.sroa.0.1.i.i to i8*
  call void @__rust_deallocate(i8* %126, i64 %125, i64 8) #5, !noalias !9
  br label %cleanup.body.thread

bb27.i.i:                                         ; preds = %cleanup4.i.i, %cleanup3.i.i
  %.sink.i.i = phi { i8*, i32 } [ %128, %cleanup3.i.i ], [ %129, %cleanup4.i.i ]
  %.fca.0.extract12.i.i = extractvalue { i8*, i32 } %.sink.i.i, 0
  %.fca.1.extract14.i.i = extractvalue { i8*, i32 } %.sink.i.i, 1
  %not..i.i.i.i.i.i.i = icmp eq i64 %element.sroa.6.0.copyload.i.i, 0
  br i1 %not..i.i.i.i.i.i.i, label %bb21.i.i, label %bb6.i.i.i.i.i.i.i

bb6.i.i.i.i.i.i.i:                                ; preds = %bb27.i.i
  call void @__rust_deallocate(i8* nonnull %4, i64 %element.sroa.6.0.copyload.i.i, i64 1) #5, !noalias !9
  br label %bb21.i.i

bb3.thread163.i.i:                                ; preds = %entry-block
  %127 = landingpad { i8*, i32 }
          cleanup
  %.fca.0.extract8164.i.i = extractvalue { i8*, i32 } %127, 0
  %.fca.1.extract10165.i.i = extractvalue { i8*, i32 } %127, 1
  br label %bb21.i.i

cleanup3.i.i:                                     ; preds = %bb7.i.i
  %128 = landingpad { i8*, i32 }
          cleanup
  br label %bb27.i.i

cleanup4.i.i:                                     ; preds = %bb10.i.i.i.i, %bb3.i3.i.i.i.i
  %129 = landingpad { i8*, i32 }
          cleanup
  br label %bb27.i.i

bb3:                                              ; preds = %bb9.i.i.i.i.i98.i.i.i, %bb6.i.i.i.i.i.i.i99.i.i.i
  call void @llvm.lifetime.end(i64 32, i8* nonnull %37), !noalias !27
  call void @llvm.lifetime.end(i64 32, i8* %1), !noalias !9
  call void @llvm.lifetime.end(i64 32, i8* %0)
  %130 = getelementptr inbounds %"2.std::result::Result<i64, std::num::ParseIntError>", %"2.std::result::Result<i64, std::num::ParseIntError>"* %_4, i64 0, i32 0
  call void @llvm.lifetime.start(i64 16, i8* %130)
  %131 = icmp ugt i64 %vector.sroa.15.1.i.i, 1
  br i1 %131, label %bb5, label %panic.i, !prof !48

panic.i:                                          ; preds = %bb3.thread, %bb3
  %args.sroa.13.037 = phi i64 [ 0, %bb3.thread ], [ %vector.sroa.15.1.i.i, %bb3 ]
  %args.sroa.9.036 = phi i64 [ 0, %bb3.thread ], [ %vector.sroa.10.1.i.i, %bb3 ]
  %args.sroa.0.035 = phi i64 [ 1, %bb3.thread ], [ %vector.sroa.0.1.i.i, %bb3 ]
  invoke void @_ZN4core9panicking18panic_bounds_check17h155238769b791565E({ %str_slice, i32 }* nonnull @panic_bounds_check_loc.3, i64 1, i64 %args.sroa.13.037)
          to label %.noexc unwind label %bb13

.noexc:                                           ; preds = %panic.i
  unreachable

bb5:                                              ; preds = %bb3
  %132 = inttoptr i64 %vector.sroa.0.1.i.i to %"3.std::string::String"*
  %133 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %132, i64 1, i32 0, i32 0, i32 0, i32 0, i32 0
  %134 = load i8*, i8** %133, align 8, !alias.scope !49, !nonnull !8
  %135 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %132, i64 1, i32 0, i32 1
  %136 = load i64, i64* %135, align 8, !alias.scope !56
  invoke void @"_ZN4core3num52_$LT$impl$u20$core..str..FromStr$u20$for$u20$i64$GT$8from_str17hc997481aa17af5deE"(%"2.std::result::Result<i64, std::num::ParseIntError>"* noalias nocapture nonnull sret dereferenceable(16) %_4, i8* noalias nonnull readonly %134, i64 %136)
          to label %bb6 unwind label %bb13.thread

bb6:                                              ; preds = %bb5
  %137 = bitcast %"2.std::result::Result<i64, std::num::ParseIntError>"* %_4 to i16*
  %self.sroa.0.0.copyload.i = load i16, i16* %137, align 8, !alias.scope !57
  %138 = trunc i16 %self.sroa.0.0.copyload.i to i8
  %switch2.i = icmp eq i8 %138, 0
  br i1 %switch2.i, label %bb10, label %bb4.i

bb4.i:                                            ; preds = %bb6
  %139 = lshr i16 %self.sroa.0.0.copyload.i, 8
  %140 = trunc i16 %139 to i8
  invoke fastcc void @_ZN4core6result13unwrap_failed17h0be2fbb68ff466eeE(i8 %140)
          to label %.noexc15 unwind label %bb13.thread

.noexc15:                                         ; preds = %bb4.i
  unreachable

bb10:                                             ; preds = %bb6
  %self.sroa.720.0..sroa_idx21.i = getelementptr inbounds %"2.std::result::Result<i64, std::num::ParseIntError>", %"2.std::result::Result<i64, std::num::ParseIntError>"* %_4, i64 0, i32 2, i64 0
  %self.sroa.720.0.copyload.i = load i64, i64* %self.sroa.720.0..sroa_idx21.i, align 8, !alias.scope !57
  call void @llvm.lifetime.end(i64 16, i8* nonnull %130)
  %141 = bitcast %"2.std::fmt::Arguments"* %_12 to i8*
  call void @llvm.lifetime.start(i64 48, i8* %141)
  %142 = bitcast [1 x %"2.std::fmt::ArgumentV1"]* %_17 to i8*
  call void @llvm.lifetime.start(i64 16, i8* %142)
  %143 = bitcast i64* %_20 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %143)
  %144 = call fastcc i64 @_ZN4rust3fib17h1dc2f854f5b08437E(i64 %self.sroa.720.0.copyload.i)
  store i64 %144, i64* %_20, align 8
  %145 = ptrtoint i64* %_20 to i64
  %146 = bitcast [1 x %"2.std::fmt::ArgumentV1"]* %_17 to i64*
  store i64 %145, i64* %146, align 8
  %147 = getelementptr inbounds [1 x %"2.std::fmt::ArgumentV1"], [1 x %"2.std::fmt::ArgumentV1"]* %_17, i64 0, i64 0, i32 1
  %148 = bitcast i8 (%"2.core::fmt::Void"*, %"2.std::fmt::Formatter"*)** %147 to i64*
  store i64 ptrtoint (i8 (i64*, %"2.std::fmt::Formatter"*)* @"_ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Display$u20$for$u20$i64$GT$3fmt17h4496ef8f06724037E" to i64), i64* %148, align 8
  %149 = getelementptr inbounds [1 x %"2.std::fmt::ArgumentV1"], [1 x %"2.std::fmt::ArgumentV1"]* %_17, i64 0, i64 0
  %150 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_12, i64 0, i32 0, i32 0
  store %str_slice* getelementptr inbounds ([2 x %str_slice], [2 x %str_slice]* @ref.6, i64 0, i64 0), %str_slice** %150, align 8, !alias.scope !60, !noalias !63
  %151 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_12, i64 0, i32 0, i32 1
  store i64 2, i64* %151, align 8, !alias.scope !60, !noalias !63
  %_6.sroa.0.0..sroa_idx.i = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_12, i64 0, i32 1, i32 0, i32 0
  store %"2.std::fmt::rt::v1::Argument"* null, %"2.std::fmt::rt::v1::Argument"** %_6.sroa.0.0..sroa_idx.i, align 8, !alias.scope !60, !noalias !63
  %152 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_12, i64 0, i32 2, i32 0
  store %"2.std::fmt::ArgumentV1"* %149, %"2.std::fmt::ArgumentV1"** %152, align 8, !alias.scope !60, !noalias !63
  %153 = getelementptr inbounds %"2.std::fmt::Arguments", %"2.std::fmt::Arguments"* %_12, i64 0, i32 2, i32 1
  store i64 1, i64* %153, align 8, !alias.scope !60, !noalias !63
  invoke void @_ZN3std2io5stdio6_print17he48522be5b0a80d9E(%"2.std::fmt::Arguments"* noalias nocapture nonnull dereferenceable(48) %_12)
          to label %bb11 unwind label %bb13.thread

bb11:                                             ; preds = %bb10
  call void @llvm.lifetime.end(i64 48, i8* %141)
  call void @llvm.lifetime.end(i64 16, i8* %142)
  call void @llvm.lifetime.end(i64 8, i8* %143)
  %154 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %132, i64 %vector.sroa.15.1.i.i
  br label %slice_loop_body.i.i.i

slice_loop_body.i.i.i:                            ; preds = %bb11, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i
  %155 = phi %"3.std::string::String"* [ %160, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i ], [ %132, %bb11 ]
  %156 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %155, i64 0, i32 0, i32 0, i32 1
  %157 = load i64, i64* %156, align 8
  %not..i.i.i.i.i.i.i.i = icmp eq i64 %157, 0
  br i1 %not..i.i.i.i.i.i.i.i, label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i, label %bb6.i.i.i.i.i.i.i.i

bb6.i.i.i.i.i.i.i.i:                              ; preds = %slice_loop_body.i.i.i
  %158 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %155, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %159 = load i8*, i8** %158, align 8, !alias.scope !66, !nonnull !8
  call void @__rust_deallocate(i8* nonnull %159, i64 %157, i64 1) #5
  br label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i

_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i:          ; preds = %bb6.i.i.i.i.i.i.i.i, %slice_loop_body.i.i.i
  %160 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %155, i64 1
  %161 = icmp eq %"3.std::string::String"* %160, %154
  br i1 %161, label %normal-return.i, label %slice_loop_body.i.i.i

normal-return.i:                                  ; preds = %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i
  %not..i.i.i.i = icmp eq i64 %vector.sroa.10.1.i.i, 0
  br i1 %not..i.i.i.i, label %bb12, label %bb6.i.i.i.i16

bb6.i.i.i.i16:                                    ; preds = %normal-return.i
  %162 = mul i64 %vector.sroa.10.1.i.i, 24
  %163 = inttoptr i64 %vector.sroa.0.1.i.i to i8*
  call void @__rust_deallocate(i8* %163, i64 %162, i64 8) #5
  br label %bb12

bb12:                                             ; preds = %bb6.i.i.i.i16, %normal-return.i
  ret void

bb13.thread:                                      ; preds = %bb5, %bb4.i, %bb10
  %lpad.thr_comm48 = landingpad { i8*, i32 }
          cleanup
  %164 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %132, i64 %vector.sroa.15.1.i.i
  br label %slice_loop_body.i.i.i18.preheader

bb13:                                             ; preds = %panic.i
  %lpad.thr_comm.split-lp49 = landingpad { i8*, i32 }
          cleanup
  %165 = inttoptr i64 %args.sroa.0.035 to %"3.std::string::String"*
  %166 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %165, i64 %args.sroa.13.037
  %167 = icmp eq i64 %args.sroa.13.037, 0
  br i1 %167, label %normal-return.i22, label %slice_loop_body.i.i.i18.preheader

slice_loop_body.i.i.i18.preheader:                ; preds = %bb13, %bb13.thread
  %.ph = phi %"3.std::string::String"* [ %164, %bb13.thread ], [ %166, %bb13 ]
  %lpad.phi54.ph = phi { i8*, i32 } [ %lpad.thr_comm48, %bb13.thread ], [ %lpad.thr_comm.split-lp49, %bb13 ]
  %args.sroa.9.1.ph52.ph = phi i64 [ %vector.sroa.10.1.i.i, %bb13.thread ], [ %args.sroa.9.036, %bb13 ]
  %args.sroa.0.1.ph50.ph = phi i64 [ %vector.sroa.0.1.i.i, %bb13.thread ], [ %args.sroa.0.035, %bb13 ]
  %.ph56 = phi %"3.std::string::String"* [ %132, %bb13.thread ], [ %165, %bb13 ]
  br label %slice_loop_body.i.i.i18

slice_loop_body.i.i.i18:                          ; preds = %slice_loop_body.i.i.i18.preheader, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20
  %168 = phi %"3.std::string::String"* [ %173, %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20 ], [ %.ph56, %slice_loop_body.i.i.i18.preheader ]
  %169 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %168, i64 0, i32 0, i32 0, i32 1
  %170 = load i64, i64* %169, align 8
  %not..i.i.i.i.i.i.i.i17 = icmp eq i64 %170, 0
  br i1 %not..i.i.i.i.i.i.i.i17, label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20, label %bb6.i.i.i.i.i.i.i.i19

bb6.i.i.i.i.i.i.i.i19:                            ; preds = %slice_loop_body.i.i.i18
  %171 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %168, i64 0, i32 0, i32 0, i32 0, i32 0, i32 0
  %172 = load i8*, i8** %171, align 8, !alias.scope !69, !nonnull !8
  call void @__rust_deallocate(i8* nonnull %172, i64 %170, i64 1) #5
  br label %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20

_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20:        ; preds = %bb6.i.i.i.i.i.i.i.i19, %slice_loop_body.i.i.i18
  %173 = getelementptr inbounds %"3.std::string::String", %"3.std::string::String"* %168, i64 1
  %174 = icmp eq %"3.std::string::String"* %173, %.ph
  br i1 %174, label %normal-return.i22.loopexit, label %slice_loop_body.i.i.i18

normal-return.i22.loopexit:                       ; preds = %_ZN4drop17h0db4ca8bb6d95bc1E.exit.i.i.i20
  br label %normal-return.i22

normal-return.i22:                                ; preds = %normal-return.i22.loopexit, %bb13
  %lpad.phi55 = phi { i8*, i32 } [ %lpad.thr_comm.split-lp49, %bb13 ], [ %lpad.phi54.ph, %normal-return.i22.loopexit ]
  %args.sroa.9.1.ph53 = phi i64 [ %args.sroa.9.036, %bb13 ], [ %args.sroa.9.1.ph52.ph, %normal-return.i22.loopexit ]
  %args.sroa.0.1.ph51 = phi i64 [ %args.sroa.0.035, %bb13 ], [ %args.sroa.0.1.ph50.ph, %normal-return.i22.loopexit ]
  %not..i.i.i.i21 = icmp eq i64 %args.sroa.9.1.ph53, 0
  br i1 %not..i.i.i.i21, label %bb1, label %bb6.i.i.i.i23

bb6.i.i.i.i23:                                    ; preds = %normal-return.i22
  %175 = mul i64 %args.sroa.9.1.ph53, 24
  %176 = inttoptr i64 %args.sroa.0.1.ph51 to i8*
  call void @__rust_deallocate(i8* %176, i64 %175, i64 8) #5
  br label %bb1
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #3

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #3

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #3

; Function Attrs: cold noinline noreturn
declare void @_ZN4core6option13expect_failed17h530ede3d41450938E(i8* noalias nonnull readonly, i64) unnamed_addr #4

; Function Attrs: nounwind
declare i32 @rust_eh_personality(i32, i32, i64, %"8.unwind::libunwind::_Unwind_Exception"*, %"8.unwind::libunwind::_Unwind_Context"*) unnamed_addr #5

declare void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$4next17hb559a6d0c64a0febE"(%"2.std::option::Option<std::string::String>"* noalias nocapture sret dereferenceable(24), %"1.std::env::Args"* dereferenceable(32)) unnamed_addr

declare void @"_ZN65_$LT$std..env..Args$u20$as$u20$core..iter..iterator..Iterator$GT$9size_hint17h23736c8fea892246E"({ i64, %"2.std::option::Option<usize>" }* noalias nocapture sret dereferenceable(24), %"1.std::env::Args"* noalias readonly dereferenceable(32)) unnamed_addr

; Function Attrs: cold noinline noreturn
declare void @_ZN5alloc3oom3oom17hcac7549e1d9cf2ccE() unnamed_addr #4

declare void @"_ZN4core3num52_$LT$impl$u20$core..str..FromStr$u20$for$u20$i64$GT$8from_str17hc997481aa17af5deE"(%"2.std::result::Result<i64, std::num::ParseIntError>"* noalias nocapture sret dereferenceable(16), i8* noalias nonnull readonly, i64) unnamed_addr

; Function Attrs: nounwind readnone
declare { i64, i1 } @llvm.uadd.with.overflow.i64(i64, i64) #6

; Function Attrs: nounwind readnone
declare { i64, i1 } @llvm.umul.with.overflow.i64(i64, i64) #6

declare i8 @"_ZN61_$LT$core..num..ParseIntError$u20$as$u20$core..fmt..Debug$GT$3fmt17hbb30da852c6908a5E"(%"2.std::num::ParseIntError"* noalias readonly dereferenceable(1), %"2.std::fmt::Formatter"* dereferenceable(96)) unnamed_addr

; Function Attrs: cold noinline noreturn
declare void @_ZN4core9panicking9panic_fmt17hcfbb59eeb7f27f75E(%"2.std::fmt::Arguments"* noalias nocapture dereferenceable(48), { %str_slice, i32 }* noalias readonly dereferenceable(24)) unnamed_addr #4

declare i8 @"_ZN42_$LT$str$u20$as$u20$core..fmt..Display$GT$3fmt17hc1be2fa738bd7dbaE"(i8* noalias nonnull readonly, i64, %"2.std::fmt::Formatter"* dereferenceable(96)) unnamed_addr

; Function Attrs: nounwind
declare void @__rust_deallocate(i8*, i64, i64) unnamed_addr #5

; Function Attrs: nounwind
declare i8* @__rust_reallocate(i8*, i64, i64, i64) unnamed_addr #5

; Function Attrs: nounwind
declare noalias i8* @__rust_allocate(i64, i64) unnamed_addr #5

; Function Attrs: cold noinline noreturn
declare void @_ZN4core9panicking18panic_bounds_check17h155238769b791565E({ %str_slice, i32 }* noalias readonly dereferenceable(24), i64, i64) unnamed_addr #4

declare void @_ZN3std3env4args17hff4915f936ee80ddE(%"1.std::env::Args"* noalias nocapture sret dereferenceable(32)) unnamed_addr

declare i8 @"_ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Display$u20$for$u20$i64$GT$3fmt17h4496ef8f06724037E"(i64* noalias readonly dereferenceable(8), %"2.std::fmt::Formatter"* dereferenceable(96)) unnamed_addr

declare void @_ZN3std2io5stdio6_print17he48522be5b0a80d9E(%"2.std::fmt::Arguments"* noalias nocapture dereferenceable(48)) unnamed_addr

define i64 @main(i64, i8**) unnamed_addr {
top:
  %2 = tail call i64 @_ZN3std2rt10lang_start17hd661476ce2fc2931E(i8* bitcast (void ()* @_ZN4rust4main17h1daf7664e0d7324fE to i8*), i64 %0, i8** %1)
  ret i64 %2
}

declare i64 @_ZN3std2rt10lang_start17hd661476ce2fc2931E(i8*, i64, i8**) unnamed_addr

attributes #0 = { cold noinline noreturn uwtable }
attributes #1 = { uwtable }
attributes #2 = { noinline nounwind readnone uwtable }
attributes #3 = { argmemonly nounwind }
attributes #4 = { cold noinline noreturn }
attributes #5 = { nounwind }
attributes #6 = { nounwind readnone }

!llvm.module.flags = !{!0}

!0 = !{i32 1, !"PIE Level", i32 2}
!1 = !{i64 1, i64 0}
!2 = !{!3}
!3 = distinct !{!3, !4, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 0"}
!4 = distinct !{!4, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E"}
!5 = !{!6, !7}
!6 = distinct !{!6, !4, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 1"}
!7 = distinct !{!7, !4, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 2"}
!8 = !{}
!9 = !{!10, !12, !13, !15}
!10 = distinct !{!10, !11, !"_ZN92_$LT$collections..vec..Vec$LT$T$GT$$u20$as$u20$core..iter..traits..FromIterator$LT$T$GT$$GT$9from_iter17h6750b9d9886da313E: argument 0"}
!11 = distinct !{!11, !"_ZN92_$LT$collections..vec..Vec$LT$T$GT$$u20$as$u20$core..iter..traits..FromIterator$LT$T$GT$$GT$9from_iter17h6750b9d9886da313E"}
!12 = distinct !{!12, !11, !"_ZN92_$LT$collections..vec..Vec$LT$T$GT$$u20$as$u20$core..iter..traits..FromIterator$LT$T$GT$$GT$9from_iter17h6750b9d9886da313E: argument 1"}
!13 = distinct !{!13, !14, !"_ZN4core4iter8iterator8Iterator7collect17h1725e3d813e722b4E: argument 0"}
!14 = distinct !{!14, !"_ZN4core4iter8iterator8Iterator7collect17h1725e3d813e722b4E"}
!15 = distinct !{!15, !14, !"_ZN4core4iter8iterator8Iterator7collect17h1725e3d813e722b4E: argument 1"}
!16 = !{!13}
!17 = !{!18, !20, !10, !12, !13, !15}
!18 = distinct !{!18, !19, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE: argument 0"}
!19 = distinct !{!19, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE"}
!20 = distinct !{!20, !21, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE: argument 0"}
!21 = distinct !{!21, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE"}
!22 = !{!23, !25, !10, !12, !13, !15}
!23 = distinct !{!23, !24, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$13with_capacity17h331b788e24a850f2E: argument 0"}
!24 = distinct !{!24, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$13with_capacity17h331b788e24a850f2E"}
!25 = distinct !{!25, !26, !"_ZN39_$LT$collections..vec..Vec$LT$T$GT$$GT$13with_capacity17h405d208efb2b8d64E: argument 0"}
!26 = distinct !{!26, !"_ZN39_$LT$collections..vec..Vec$LT$T$GT$$GT$13with_capacity17h405d208efb2b8d64E"}
!27 = !{!28, !10, !12, !13, !15}
!28 = distinct !{!28, !29, !"_ZN39_$LT$collections..vec..Vec$LT$T$GT$$GT$16extend_desugared17h5384a33cbb712d5bE: argument 0"}
!29 = distinct !{!29, !"_ZN39_$LT$collections..vec..Vec$LT$T$GT$$GT$16extend_desugared17h5384a33cbb712d5bE"}
!30 = !{!31, !33, !28, !10, !12, !13, !15}
!31 = distinct !{!31, !32, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE: argument 0"}
!32 = distinct !{!32, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE"}
!33 = distinct !{!33, !34, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE: argument 0"}
!34 = distinct !{!34, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE"}
!35 = !{!36, !38, !28, !10, !12, !13, !15}
!36 = distinct !{!36, !37, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE: argument 0"}
!37 = distinct !{!37, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE"}
!38 = distinct !{!38, !39, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE: argument 0"}
!39 = distinct !{!39, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE"}
!40 = !{!41, !43, !10, !12, !13, !15}
!41 = distinct !{!41, !42, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE: argument 0"}
!42 = distinct !{!42, !"_ZN86_$LT$collections..vec..IntoIter$LT$T$GT$$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h40188afc5cf0fbbcE"}
!43 = distinct !{!43, !44, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE: argument 0"}
!44 = distinct !{!44, !"_ZN75_$LT$$RF$$u27$a$u20$mut$u20$I$u20$as$u20$core..iter..iterator..Iterator$GT$4next17h43e8667d72b26a0bE"}
!45 = !{!46}
!46 = distinct !{!46, !47, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E: argument 0"}
!47 = distinct !{!47, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E"}
!48 = !{!"branch_weights", i32 2000, i32 1}
!49 = !{!50, !52, !54}
!50 = distinct !{!50, !51, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E: argument 0"}
!51 = distinct !{!51, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E"}
!52 = distinct !{!52, !53, !"_ZN67_$LT$collections..vec..Vec$LT$T$GT$$u20$as$u20$core..ops..Deref$GT$5deref17h9b56f5d54a9107ccE: argument 0"}
!53 = distinct !{!53, !"_ZN67_$LT$collections..vec..Vec$LT$T$GT$$u20$as$u20$core..ops..Deref$GT$5deref17h9b56f5d54a9107ccE"}
!54 = distinct !{!54, !55, !"_ZN64_$LT$collections..string..String$u20$as$u20$core..ops..Deref$GT$5deref17h000e9c2d8c0e04f5E: argument 0"}
!55 = distinct !{!55, !"_ZN64_$LT$collections..string..String$u20$as$u20$core..ops..Deref$GT$5deref17h000e9c2d8c0e04f5E"}
!56 = !{!52, !54}
!57 = !{!58}
!58 = distinct !{!58, !59, !"_ZN47_$LT$core..result..Result$LT$T$C$$u20$E$GT$$GT$6unwrap17h45dab051c56758feE: argument 0"}
!59 = distinct !{!59, !"_ZN47_$LT$core..result..Result$LT$T$C$$u20$E$GT$$GT$6unwrap17h45dab051c56758feE"}
!60 = !{!61}
!61 = distinct !{!61, !62, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 0"}
!62 = distinct !{!62, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E"}
!63 = !{!64, !65}
!64 = distinct !{!64, !62, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 1"}
!65 = distinct !{!65, !62, !"_ZN4core3fmt9Arguments6new_v117hf82ebeeaebdcd474E: argument 2"}
!66 = !{!67}
!67 = distinct !{!67, !68, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E: argument 0"}
!68 = distinct !{!68, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E"}
!69 = !{!70}
!70 = distinct !{!70, !71, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E: argument 0"}
!71 = distinct !{!71, !"_ZN40_$LT$alloc..raw_vec..RawVec$LT$T$GT$$GT$3ptr17h8f31e2209b06ee34E"}
