#include <stdlib.h>
#include <stdio.h>

int fib(int n) {
	if (n <= 1) {
		return 1;
	} else {
		return (fib(n - 1) + fib(n - 2));
	}
}

int main(int argc,  char** argv) {
    int num = atoi(argv[1]);

    printf("%d\n", fib(num));
}
