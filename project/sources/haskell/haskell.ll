target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux"
declare ccc i8* @memcpy$def(i8*, i8*, i64)
declare ccc i8* @memmove$def(i8*, i8*, i64)
declare ccc i8* @memset$def(i8*, i64, i64)
declare ccc i64 @newSpark$def(i8*, i8*)
!0 = !{!"root"}
!1 = !{!"top", !0}
!2 = !{!"stack", !1}
!3 = !{!"heap", !1}
!4 = !{!"rx", !3}
!5 = !{!"base", !1}

%Main_zdtrModule4_bytes_struct = type <{[5 x i8]}>
@Main_zdtrModule4_bytes$def = internal constant %Main_zdtrModule4_bytes_struct<{[5 x i8] [i8 109, i8 97, i8 105, i8 110, i8 0]}>, align 1
@Main_zdtrModule4_bytes = alias i8, bitcast (%Main_zdtrModule4_bytes_struct* @Main_zdtrModule4_bytes$def to i8*)
%Main_zdtrModule3_closure_struct = type <{i64, i64}>
@Main_zdtrModule3_closure$def = internal global %Main_zdtrModule3_closure_struct<{i64 ptrtoint (i8* @ghczmprim_GHCziTypes_TrNameS_con_info to i64), i64 ptrtoint (%Main_zdtrModule4_bytes_struct* @Main_zdtrModule4_bytes$def to i64)}>
@Main_zdtrModule3_closure = alias i8, bitcast (%Main_zdtrModule3_closure_struct* @Main_zdtrModule3_closure$def to i8*)
%Main_zdtrModule2_bytes_struct = type <{[5 x i8]}>
@Main_zdtrModule2_bytes$def = internal constant %Main_zdtrModule2_bytes_struct<{[5 x i8] [i8 77, i8 97, i8 105, i8 110, i8 0]}>, align 1
@Main_zdtrModule2_bytes = alias i8, bitcast (%Main_zdtrModule2_bytes_struct* @Main_zdtrModule2_bytes$def to i8*)
%Main_zdtrModule1_closure_struct = type <{i64, i64}>
@Main_zdtrModule1_closure$def = internal global %Main_zdtrModule1_closure_struct<{i64 ptrtoint (i8* @ghczmprim_GHCziTypes_TrNameS_con_info to i64), i64 ptrtoint (%Main_zdtrModule2_bytes_struct* @Main_zdtrModule2_bytes$def to i64)}>
@Main_zdtrModule1_closure = alias i8, bitcast (%Main_zdtrModule1_closure_struct* @Main_zdtrModule1_closure$def to i8*)
%Main_zdtrModule_closure_struct = type <{i64, i64, i64, i64}>
@Main_zdtrModule_closure$def = internal global %Main_zdtrModule_closure_struct<{i64 ptrtoint (i8* @ghczmprim_GHCziTypes_Module_con_info to i64), i64 add (i64 ptrtoint (%Main_zdtrModule3_closure_struct* @Main_zdtrModule3_closure$def to i64),i64 1), i64 add (i64 ptrtoint (%Main_zdtrModule1_closure_struct* @Main_zdtrModule1_closure$def to i64),i64 1), i64 3}>
@Main_zdtrModule_closure = alias i8, bitcast (%Main_zdtrModule_closure_struct* @Main_zdtrModule_closure$def to i8*)
%Main_zdwfib_closure_struct = type <{i64}>
@Main_zdwfib_closure$def = internal global %Main_zdwfib_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to i64)}>
@Main_zdwfib_closure = alias i8, bitcast (%Main_zdwfib_closure_struct* @Main_zdwfib_closure$def to i8*)
@Main_zdwfib_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to i8*)
define ghccc void @Main_zdwfib_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 4294967300, i64 0, i64 14}>
{
c673:
  %ls66i = alloca i64, i32 1
  %R2_Var = alloca i64, i32 1
  store i64 %R2_Arg, i64* %R2_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln67o = load i64*, i64** %Sp_Var
  %ln67p = getelementptr inbounds i64, i64* %ln67o, i32 -2
  %ln67q = ptrtoint i64* %ln67p to i64
  %ln67r = icmp ult i64 %ln67q, %SpLim_Arg
  %ln67t = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln67r, i1 0 )
  br i1 %ln67t, label %c674, label %c675
c675:
  %ln67u = load i64, i64* %R2_Var
  %ln67v = icmp sle i64 %ln67u, 1
  %ln67w = zext i1 %ln67v to i64
  switch i64 %ln67w, label %c671 [i64 1, label %c672]
c671:
  %ln67y = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c67a_info$def to i64
  %ln67x = load i64*, i64** %Sp_Var
  %ln67z = getelementptr inbounds i64, i64* %ln67x, i32 -2
  store i64 %ln67y, i64* %ln67z, !tbaa !2
  %ln67A = load i64, i64* %R2_Var
  store i64 %ln67A, i64* %ls66i
  %ln67C = load i64, i64* %R2_Var
  %ln67D = add i64 %ln67C, -1
  store i64 %ln67D, i64* %R2_Var
  %ln67F = load i64, i64* %ls66i
  %ln67E = load i64*, i64** %Sp_Var
  %ln67G = getelementptr inbounds i64, i64* %ln67E, i32 -1
  store i64 %ln67F, i64* %ln67G, !tbaa !2
  %ln67H = load i64*, i64** %Sp_Var
  %ln67I = getelementptr inbounds i64, i64* %ln67H, i32 -2
  %ln67J = ptrtoint i64* %ln67I to i64
  %ln67K = inttoptr i64 %ln67J to i64*
  store i64* %ln67K, i64** %Sp_Var
  %ln67L = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln67M = load i64*, i64** %Sp_Var
  %ln67N = load i64, i64* %R1_Var
  %ln67O = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln67L( i64* %Base_Arg, i64* %ln67M, i64* %Hp_Arg, i64 %ln67N, i64 %ln67O, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c672:
  store i64 1, i64* %R1_Var
  %ln67P = load i64*, i64** %Sp_Var
  %ln67Q = getelementptr inbounds i64, i64* %ln67P, i32 0
  %ln67R = bitcast i64* %ln67Q to i64*
  %ln67S = load i64, i64* %ln67R, !tbaa !2
  %ln67T = inttoptr i64 %ln67S to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln67U = load i64*, i64** %Sp_Var
  %ln67V = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln67T( i64* %Base_Arg, i64* %ln67U, i64* %Hp_Arg, i64 %ln67V, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c674:
  %ln67W = load i64, i64* %R2_Var
  store i64 %ln67W, i64* %R2_Var
  %ln67X = ptrtoint %Main_zdwfib_closure_struct* @Main_zdwfib_closure$def to i64
  store i64 %ln67X, i64* %R1_Var
  %ln67Y = getelementptr inbounds i64, i64* %Base_Arg, i32 -1
  %ln67Z = bitcast i64* %ln67Y to i64*
  %ln680 = load i64, i64* %ln67Z, !tbaa !5
  %ln681 = inttoptr i64 %ln680 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln682 = load i64*, i64** %Sp_Var
  %ln683 = load i64, i64* %R1_Var
  %ln684 = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln681( i64* %Base_Arg, i64* %ln682, i64* %Hp_Arg, i64 %ln683, i64 %ln684, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
declare ccc i1 @llvm.expect.i1(i1, i1)
@c67a_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c67a_info$def to i8*)
define internal ghccc void @c67a_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 65, i64 30}>
{
c67a:
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln685 = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c67h_info$def to i64
  %ln686 = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln685, i64* %ln686, !tbaa !2
  %ln687 = getelementptr inbounds i64, i64* %Sp_Arg, i32 1
  %ln688 = bitcast i64* %ln687 to i64*
  %ln689 = load i64, i64* %ln688, !tbaa !2
  %ln68a = add i64 %ln689, -2
  store i64 %ln68a, i64* %R2_Var
  %ln68b = getelementptr inbounds i64, i64* %Sp_Arg, i32 1
  store i64 %R1_Arg, i64* %ln68b, !tbaa !2
  %ln68c = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln68d = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln68c( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln68d, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c67h_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c67h_info$def to i8*)
define internal ghccc void @c67h_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 65, i64 30}>
{
c67h:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln68e = load i64*, i64** %Sp_Var
  %ln68f = getelementptr inbounds i64, i64* %ln68e, i32 1
  %ln68g = bitcast i64* %ln68f to i64*
  %ln68h = load i64, i64* %ln68g, !tbaa !2
  %ln68i = load i64, i64* %R1_Var
  %ln68j = add i64 %ln68h, %ln68i
  store i64 %ln68j, i64* %R1_Var
  %ln68k = load i64*, i64** %Sp_Var
  %ln68l = getelementptr inbounds i64, i64* %ln68k, i32 2
  %ln68m = ptrtoint i64* %ln68l to i64
  %ln68n = inttoptr i64 %ln68m to i64*
  store i64* %ln68n, i64** %Sp_Var
  %ln68o = load i64*, i64** %Sp_Var
  %ln68p = getelementptr inbounds i64, i64* %ln68o, i32 0
  %ln68q = bitcast i64* %ln68p to i64*
  %ln68r = load i64, i64* %ln68q, !tbaa !2
  %ln68s = inttoptr i64 %ln68r to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln68t = load i64*, i64** %Sp_Var
  %ln68u = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln68s( i64* %Base_Arg, i64* %ln68t, i64* %Hp_Arg, i64 %ln68u, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_fib_closure_struct = type <{i64}>
@Main_fib_closure$def = internal global %Main_fib_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_fib_info$def to i64)}>
@Main_fib_closure = alias i8, bitcast (%Main_fib_closure_struct* @Main_fib_closure$def to i8*)
@Main_fib_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_fib_info$def to i8*)
define ghccc void @Main_fib_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 4294967301, i64 0, i64 14}>
{
c68C:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R2_Var = alloca i64, i32 1
  store i64 %R2_Arg, i64* %R2_Var
  %ln68R = load i64*, i64** %Sp_Var
  %ln68S = getelementptr inbounds i64, i64* %ln68R, i32 -1
  %ln68T = ptrtoint i64* %ln68S to i64
  %ln68U = icmp ult i64 %ln68T, %SpLim_Arg
  %ln68V = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln68U, i1 0 )
  br i1 %ln68V, label %c68K, label %c68L
c68L:
  %ln68X = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c68z_info$def to i64
  %ln68W = load i64*, i64** %Sp_Var
  %ln68Y = getelementptr inbounds i64, i64* %ln68W, i32 -1
  store i64 %ln68X, i64* %ln68Y, !tbaa !2
  %ln68Z = load i64, i64* %R2_Var
  store i64 %ln68Z, i64* %R1_Var
  %ln690 = load i64*, i64** %Sp_Var
  %ln691 = getelementptr inbounds i64, i64* %ln690, i32 -1
  %ln692 = ptrtoint i64* %ln691 to i64
  %ln693 = inttoptr i64 %ln692 to i64*
  store i64* %ln693, i64** %Sp_Var
  %ln694 = load i64, i64* %R1_Var
  %ln695 = and i64 %ln694, 7
  %ln696 = icmp ne i64 %ln695, 0
  br i1 %ln696, label %u68Q, label %c68A
c68A:
  %ln698 = load i64, i64* %R1_Var
  %ln699 = inttoptr i64 %ln698 to i64*
  %ln69a = load i64, i64* %ln699, !tbaa !4
  %ln69b = inttoptr i64 %ln69a to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln69c = load i64*, i64** %Sp_Var
  %ln69d = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln69b( i64* %Base_Arg, i64* %ln69c, i64* %Hp_Arg, i64 %ln69d, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
u68Q:
  %ln69e = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c68z_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln69f = load i64*, i64** %Sp_Var
  %ln69g = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln69e( i64* %Base_Arg, i64* %ln69f, i64* %Hp_Arg, i64 %ln69g, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c68K:
  %ln69h = load i64, i64* %R2_Var
  store i64 %ln69h, i64* %R2_Var
  %ln69i = ptrtoint %Main_fib_closure_struct* @Main_fib_closure$def to i64
  store i64 %ln69i, i64* %R1_Var
  %ln69j = getelementptr inbounds i64, i64* %Base_Arg, i32 -1
  %ln69k = bitcast i64* %ln69j to i64*
  %ln69l = load i64, i64* %ln69k, !tbaa !5
  %ln69m = inttoptr i64 %ln69l to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln69n = load i64*, i64** %Sp_Var
  %ln69o = load i64, i64* %R1_Var
  %ln69p = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln69m( i64* %Base_Arg, i64* %ln69n, i64* %Hp_Arg, i64 %ln69o, i64 %ln69p, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c68z_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c68z_info$def to i8*)
define internal ghccc void @c68z_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c68z:
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln69q = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c68F_info$def to i64
  %ln69r = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln69q, i64* %ln69r, !tbaa !2
  %ln69s = add i64 %R1_Arg, 7
  %ln69t = inttoptr i64 %ln69s to i64*
  %ln69u = load i64, i64* %ln69t, !tbaa !4
  store i64 %ln69u, i64* %R2_Var
  %ln69v = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln69w = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln69v( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln69w, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c68F_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c68F_info$def to i8*)
define internal ghccc void @c68F_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c68F:
  %Hp_Var = alloca i64*, i32 1
  store i64* %Hp_Arg, i64** %Hp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln69x = load i64*, i64** %Hp_Var
  %ln69y = getelementptr inbounds i64, i64* %ln69x, i32 2
  %ln69z = ptrtoint i64* %ln69y to i64
  %ln69A = inttoptr i64 %ln69z to i64*
  store i64* %ln69A, i64** %Hp_Var
  %ln69B = load i64*, i64** %Hp_Var
  %ln69C = ptrtoint i64* %ln69B to i64
  %ln69D = getelementptr inbounds i64, i64* %Base_Arg, i32 107
  %ln69E = bitcast i64* %ln69D to i64*
  %ln69F = load i64, i64* %ln69E, !tbaa !5
  %ln69G = icmp ugt i64 %ln69C, %ln69F
  %ln69H = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln69G, i1 0 )
  br i1 %ln69H, label %c68P, label %c68O
c68O:
  %ln69J = ptrtoint i8* @ghczmprim_GHCziTypes_Izh_con_info to i64
  %ln69I = load i64*, i64** %Hp_Var
  %ln69K = getelementptr inbounds i64, i64* %ln69I, i32 -1
  store i64 %ln69J, i64* %ln69K, !tbaa !3
  %ln69M = load i64, i64* %R1_Var
  %ln69L = load i64*, i64** %Hp_Var
  %ln69N = getelementptr inbounds i64, i64* %ln69L, i32 0
  store i64 %ln69M, i64* %ln69N, !tbaa !3
  %ln69P = load i64*, i64** %Hp_Var
  %ln69Q = ptrtoint i64* %ln69P to i64
  %ln69R = add i64 %ln69Q, -7
  store i64 %ln69R, i64* %R1_Var
  %ln69S = load i64*, i64** %Sp_Var
  %ln69T = getelementptr inbounds i64, i64* %ln69S, i32 1
  %ln69U = ptrtoint i64* %ln69T to i64
  %ln69V = inttoptr i64 %ln69U to i64*
  store i64* %ln69V, i64** %Sp_Var
  %ln69W = load i64*, i64** %Sp_Var
  %ln69X = getelementptr inbounds i64, i64* %ln69W, i32 0
  %ln69Y = bitcast i64* %ln69X to i64*
  %ln69Z = load i64, i64* %ln69Y, !tbaa !2
  %ln6a0 = inttoptr i64 %ln69Z to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6a1 = load i64*, i64** %Sp_Var
  %ln6a2 = load i64*, i64** %Hp_Var
  %ln6a3 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6a0( i64* %Base_Arg, i64* %ln6a1, i64* %ln6a2, i64 %ln6a3, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c68P:
  %ln6a4 = getelementptr inbounds i64, i64* %Base_Arg, i32 113
  store i64 16, i64* %ln6a4, !tbaa !5
  %ln6a5 = load i64, i64* %R1_Var
  store i64 %ln6a5, i64* %R1_Var
  %ln6a6 = bitcast i8* @stg_gc_unbx_r1 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6a7 = load i64*, i64** %Sp_Var
  %ln6a8 = load i64*, i64** %Hp_Var
  %ln6a9 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6a6( i64* %Base_Arg, i64* %ln6a7, i64* %ln6a8, i64 %ln6a9, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_main3_closure_struct = type <{i64, i64, i64, i64}>
@Main_main3_closure$def = internal global %Main_main3_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main3_info$def to i64), i64 0, i64 0, i64 0}>
@Main_main3_closure = alias i8, bitcast (%Main_main3_closure_struct* @Main_main3_closure$def to i8*)
@Main_main3_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main3_info$def to i8*)
define ghccc void @Main_main3_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main3_info$def to i64)),i64 0), i64 0, i64 12884901909}>
{
c6ag:
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R4_Var = alloca i64, i32 1
  store i64 undef, i64* %R4_Var
  %R5_Var = alloca i64, i32 1
  store i64 undef, i64* %R5_Var
  %R6_Var = alloca i64, i32 1
  store i64 undef, i64* %R6_Var
  %F1_Var = alloca float, i32 1
  store float undef, float* %F1_Var
  %D1_Var = alloca double, i32 1
  store double undef, double* %D1_Var
  %F2_Var = alloca float, i32 1
  store float undef, float* %F2_Var
  %D2_Var = alloca double, i32 1
  store double undef, double* %D2_Var
  %F3_Var = alloca float, i32 1
  store float undef, float* %F3_Var
  %D3_Var = alloca double, i32 1
  store double undef, double* %D3_Var
  %F4_Var = alloca float, i32 1
  store float undef, float* %F4_Var
  %D4_Var = alloca double, i32 1
  store double undef, double* %D4_Var
  %F5_Var = alloca float, i32 1
  store float undef, float* %F5_Var
  %D5_Var = alloca double, i32 1
  store double undef, double* %D5_Var
  %F6_Var = alloca float, i32 1
  store float undef, float* %F6_Var
  %D6_Var = alloca double, i32 1
  store double undef, double* %D6_Var
  %lc6ad = alloca i64, i32 1
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln6ak = load i64*, i64** %Sp_Var
  %ln6al = getelementptr inbounds i64, i64* %ln6ak, i32 -2
  %ln6am = ptrtoint i64* %ln6al to i64
  %ln6an = icmp ult i64 %ln6am, %SpLim_Arg
  %ln6ao = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6an, i1 0 )
  br i1 %ln6ao, label %c6ah, label %c6ai
c6ai:
  %ln6ap = ptrtoint i64* %Base_Arg to i64
  %ln6aq = inttoptr i64 %ln6ap to i8*
  %ln6ar = load i64, i64* %R1_Var
  %ln6as = inttoptr i64 %ln6ar to i8*
  %ln6at = bitcast i8* @newCAF to i8* (i8*, i8*)*
  store i64 undef, i64* %R3_Var
  store i64 undef, i64* %R4_Var
  store i64 undef, i64* %R5_Var
  store i64 undef, i64* %R6_Var
  store float undef, float* %F1_Var
  store double undef, double* %D1_Var
  store float undef, float* %F2_Var
  store double undef, double* %D2_Var
  store float undef, float* %F3_Var
  store double undef, double* %D3_Var
  store float undef, float* %F4_Var
  store double undef, double* %D4_Var
  store float undef, float* %F5_Var
  store double undef, double* %D5_Var
  store float undef, float* %F6_Var
  store double undef, double* %D6_Var
  %ln6au = call ccc i8* (i8*, i8*) %ln6at( i8* %ln6aq, i8* %ln6as ) nounwind
  %ln6av = ptrtoint i8* %ln6au to i64
  store i64 %ln6av, i64* %lc6ad
  %ln6aw = load i64, i64* %lc6ad
  %ln6ax = icmp eq i64 %ln6aw, 0
  br i1 %ln6ax, label %c6af, label %c6ae
c6ae:
  %ln6az = ptrtoint i8* @stg_bh_upd_frame_info to i64
  %ln6ay = load i64*, i64** %Sp_Var
  %ln6aA = getelementptr inbounds i64, i64* %ln6ay, i32 -2
  store i64 %ln6az, i64* %ln6aA, !tbaa !2
  %ln6aC = load i64, i64* %lc6ad
  %ln6aB = load i64*, i64** %Sp_Var
  %ln6aD = getelementptr inbounds i64, i64* %ln6aB, i32 -1
  store i64 %ln6aC, i64* %ln6aD, !tbaa !2
  %ln6aE = ptrtoint i8* @base_TextziRead_readEither5_closure to i64
  store i64 %ln6aE, i64* %R2_Var
  %ln6aF = load i64*, i64** %Sp_Var
  %ln6aG = getelementptr inbounds i64, i64* %ln6aF, i32 -2
  %ln6aH = ptrtoint i64* %ln6aG to i64
  %ln6aI = inttoptr i64 %ln6aH to i64*
  store i64* %ln6aI, i64** %Sp_Var
  %ln6aJ = bitcast i8* @base_GHCziErr_errorWithoutStackTrace_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6aK = load i64*, i64** %Sp_Var
  %ln6aL = load i64, i64* %R1_Var
  %ln6aM = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6aJ( i64* %Base_Arg, i64* %ln6aK, i64* %Hp_Arg, i64 %ln6aL, i64 %ln6aM, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6af:
  %ln6aO = load i64, i64* %R1_Var
  %ln6aP = inttoptr i64 %ln6aO to i64*
  %ln6aQ = load i64, i64* %ln6aP, !tbaa !4
  %ln6aR = inttoptr i64 %ln6aQ to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6aS = load i64*, i64** %Sp_Var
  %ln6aT = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6aR( i64* %Base_Arg, i64* %ln6aS, i64* %Hp_Arg, i64 %ln6aT, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6ah:
  %ln6aU = load i64, i64* %R1_Var
  store i64 %ln6aU, i64* %R1_Var
  %ln6aV = getelementptr inbounds i64, i64* %Base_Arg, i32 -2
  %ln6aW = bitcast i64* %ln6aV to i64*
  %ln6aX = load i64, i64* %ln6aW, !tbaa !5
  %ln6aY = inttoptr i64 %ln6aX to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6aZ = load i64*, i64** %Sp_Var
  %ln6b0 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6aY( i64* %Base_Arg, i64* %ln6aZ, i64* %Hp_Arg, i64 %ln6b0, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_main2_closure_struct = type <{i64, i64, i64, i64}>
@Main_main2_closure$def = internal global %Main_main2_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main2_info$def to i64), i64 0, i64 0, i64 0}>
@Main_main2_closure = alias i8, bitcast (%Main_main2_closure_struct* @Main_main2_closure$def to i8*)
@Main_main2_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main2_info$def to i8*)
define ghccc void @Main_main2_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main2_info$def to i64)),i64 0), i64 0, i64 21474836501}>
{
c6b7:
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R4_Var = alloca i64, i32 1
  store i64 undef, i64* %R4_Var
  %R5_Var = alloca i64, i32 1
  store i64 undef, i64* %R5_Var
  %R6_Var = alloca i64, i32 1
  store i64 undef, i64* %R6_Var
  %F1_Var = alloca float, i32 1
  store float undef, float* %F1_Var
  %D1_Var = alloca double, i32 1
  store double undef, double* %D1_Var
  %F2_Var = alloca float, i32 1
  store float undef, float* %F2_Var
  %D2_Var = alloca double, i32 1
  store double undef, double* %D2_Var
  %F3_Var = alloca float, i32 1
  store float undef, float* %F3_Var
  %D3_Var = alloca double, i32 1
  store double undef, double* %D3_Var
  %F4_Var = alloca float, i32 1
  store float undef, float* %F4_Var
  %D4_Var = alloca double, i32 1
  store double undef, double* %D4_Var
  %F5_Var = alloca float, i32 1
  store float undef, float* %F5_Var
  %D5_Var = alloca double, i32 1
  store double undef, double* %D5_Var
  %F6_Var = alloca float, i32 1
  store float undef, float* %F6_Var
  %D6_Var = alloca double, i32 1
  store double undef, double* %D6_Var
  %lc6b4 = alloca i64, i32 1
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln6ba = load i64*, i64** %Sp_Var
  %ln6bb = getelementptr inbounds i64, i64* %ln6ba, i32 -2
  %ln6bc = ptrtoint i64* %ln6bb to i64
  %ln6bd = icmp ult i64 %ln6bc, %SpLim_Arg
  %ln6be = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6bd, i1 0 )
  br i1 %ln6be, label %c6b8, label %c6b9
c6b9:
  %ln6bf = ptrtoint i64* %Base_Arg to i64
  %ln6bg = inttoptr i64 %ln6bf to i8*
  %ln6bh = load i64, i64* %R1_Var
  %ln6bi = inttoptr i64 %ln6bh to i8*
  %ln6bj = bitcast i8* @newCAF to i8* (i8*, i8*)*
  store i64 undef, i64* %R3_Var
  store i64 undef, i64* %R4_Var
  store i64 undef, i64* %R5_Var
  store i64 undef, i64* %R6_Var
  store float undef, float* %F1_Var
  store double undef, double* %D1_Var
  store float undef, float* %F2_Var
  store double undef, double* %D2_Var
  store float undef, float* %F3_Var
  store double undef, double* %D3_Var
  store float undef, float* %F4_Var
  store double undef, double* %D4_Var
  store float undef, float* %F5_Var
  store double undef, double* %D5_Var
  store float undef, float* %F6_Var
  store double undef, double* %D6_Var
  %ln6bk = call ccc i8* (i8*, i8*) %ln6bj( i8* %ln6bg, i8* %ln6bi ) nounwind
  %ln6bl = ptrtoint i8* %ln6bk to i64
  store i64 %ln6bl, i64* %lc6b4
  %ln6bm = load i64, i64* %lc6b4
  %ln6bn = icmp eq i64 %ln6bm, 0
  br i1 %ln6bn, label %c6b6, label %c6b5
c6b5:
  %ln6bp = ptrtoint i8* @stg_bh_upd_frame_info to i64
  %ln6bo = load i64*, i64** %Sp_Var
  %ln6bq = getelementptr inbounds i64, i64* %ln6bo, i32 -2
  store i64 %ln6bp, i64* %ln6bq, !tbaa !2
  %ln6bs = load i64, i64* %lc6b4
  %ln6br = load i64*, i64** %Sp_Var
  %ln6bt = getelementptr inbounds i64, i64* %ln6br, i32 -1
  store i64 %ln6bs, i64* %ln6bt, !tbaa !2
  %ln6bu = ptrtoint i8* @base_TextziRead_readEither2_closure to i64
  store i64 %ln6bu, i64* %R2_Var
  %ln6bv = load i64*, i64** %Sp_Var
  %ln6bw = getelementptr inbounds i64, i64* %ln6bv, i32 -2
  %ln6bx = ptrtoint i64* %ln6bw to i64
  %ln6by = inttoptr i64 %ln6bx to i64*
  store i64* %ln6by, i64** %Sp_Var
  %ln6bz = bitcast i8* @base_GHCziErr_errorWithoutStackTrace_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6bA = load i64*, i64** %Sp_Var
  %ln6bB = load i64, i64* %R1_Var
  %ln6bC = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6bz( i64* %Base_Arg, i64* %ln6bA, i64* %Hp_Arg, i64 %ln6bB, i64 %ln6bC, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6b6:
  %ln6bE = load i64, i64* %R1_Var
  %ln6bF = inttoptr i64 %ln6bE to i64*
  %ln6bG = load i64, i64* %ln6bF, !tbaa !4
  %ln6bH = inttoptr i64 %ln6bG to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6bI = load i64*, i64** %Sp_Var
  %ln6bJ = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6bH( i64* %Base_Arg, i64* %ln6bI, i64* %Hp_Arg, i64 %ln6bJ, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6b8:
  %ln6bK = load i64, i64* %R1_Var
  store i64 %ln6bK, i64* %R1_Var
  %ln6bL = getelementptr inbounds i64, i64* %Base_Arg, i32 -2
  %ln6bM = bitcast i64* %ln6bL to i64*
  %ln6bN = load i64, i64* %ln6bM, !tbaa !5
  %ln6bO = inttoptr i64 %ln6bN to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6bP = load i64*, i64** %Sp_Var
  %ln6bQ = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6bO( i64* %Base_Arg, i64* %ln6bP, i64* %Hp_Arg, i64 %ln6bQ, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_main1_closure_struct = type <{i64, i64}>
@Main_main1_closure$def = internal global %Main_main1_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main1_info$def to i64), i64 0}>
@Main_main1_closure = alias i8, bitcast (%Main_main1_closure_struct* @Main_main1_closure$def to i8*)
@s66A_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66A_info$def to i8*)
define internal ghccc void @s66A_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66A_info$def to i64)),i64 24), i64 1, i64 4294967312}>
{
c6cc:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln6ds = load i64*, i64** %Sp_Var
  %ln6dt = getelementptr inbounds i64, i64* %ln6ds, i32 -3
  %ln6du = ptrtoint i64* %ln6dt to i64
  %ln6dv = icmp ult i64 %ln6du, %SpLim_Arg
  %ln6dw = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6dv, i1 0 )
  br i1 %ln6dw, label %c6cd, label %c6ce
c6ce:
  %ln6dy = ptrtoint i8* @stg_upd_frame_info to i64
  %ln6dx = load i64*, i64** %Sp_Var
  %ln6dz = getelementptr inbounds i64, i64* %ln6dx, i32 -2
  store i64 %ln6dy, i64* %ln6dz, !tbaa !2
  %ln6dB = load i64, i64* %R1_Var
  %ln6dA = load i64*, i64** %Sp_Var
  %ln6dC = getelementptr inbounds i64, i64* %ln6dA, i32 -1
  store i64 %ln6dB, i64* %ln6dC, !tbaa !2
  %ln6dE = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6c5_info$def to i64
  %ln6dD = load i64*, i64** %Sp_Var
  %ln6dF = getelementptr inbounds i64, i64* %ln6dD, i32 -3
  store i64 %ln6dE, i64* %ln6dF, !tbaa !2
  %ln6dI = load i64, i64* %R1_Var
  %ln6dJ = add i64 %ln6dI, 16
  %ln6dK = inttoptr i64 %ln6dJ to i64*
  %ln6dL = load i64, i64* %ln6dK, !tbaa !4
  store i64 %ln6dL, i64* %R1_Var
  %ln6dM = load i64*, i64** %Sp_Var
  %ln6dN = getelementptr inbounds i64, i64* %ln6dM, i32 -3
  %ln6dO = ptrtoint i64* %ln6dN to i64
  %ln6dP = inttoptr i64 %ln6dO to i64*
  store i64* %ln6dP, i64** %Sp_Var
  %ln6dQ = load i64, i64* %R1_Var
  %ln6dR = and i64 %ln6dQ, 7
  %ln6dS = icmp ne i64 %ln6dR, 0
  br i1 %ln6dS, label %u6cl, label %c6c6
c6c6:
  %ln6dU = load i64, i64* %R1_Var
  %ln6dV = inttoptr i64 %ln6dU to i64*
  %ln6dW = load i64, i64* %ln6dV, !tbaa !4
  %ln6dX = inttoptr i64 %ln6dW to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6dY = load i64*, i64** %Sp_Var
  %ln6dZ = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6dX( i64* %Base_Arg, i64* %ln6dY, i64* %Hp_Arg, i64 %ln6dZ, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
u6cl:
  %ln6e0 = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6c5_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6e1 = load i64*, i64** %Sp_Var
  %ln6e2 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6e0( i64* %Base_Arg, i64* %ln6e1, i64* %Hp_Arg, i64 %ln6e2, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6cd:
  %ln6e3 = load i64, i64* %R1_Var
  store i64 %ln6e3, i64* %R1_Var
  %ln6e4 = getelementptr inbounds i64, i64* %Base_Arg, i32 -2
  %ln6e5 = bitcast i64* %ln6e4 to i64*
  %ln6e6 = load i64, i64* %ln6e5, !tbaa !5
  %ln6e7 = inttoptr i64 %ln6e6 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6e8 = load i64*, i64** %Sp_Var
  %ln6e9 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6e7( i64* %Base_Arg, i64* %ln6e8, i64* %Hp_Arg, i64 %ln6e9, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6c5_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6c5_info$def to i8*)
define internal ghccc void @c6c5_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6c5_info$def to i64)),i64 24), i64 0, i64 4294967326}>
{
c6c5:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln6ea = load i64, i64* %R1_Var
  %ln6eb = and i64 %ln6ea, 7
  switch i64 %ln6eb, label %c6c9 [i64 1, label %c6c9
i64 2, label %c6ca]
c6c9:
  %ln6ec = ptrtoint i8* @base_GHCziList_badHead_closure to i64
  store i64 %ln6ec, i64* %R1_Var
  %ln6ed = load i64*, i64** %Sp_Var
  %ln6ee = getelementptr inbounds i64, i64* %ln6ed, i32 1
  %ln6ef = ptrtoint i64* %ln6ee to i64
  %ln6eg = inttoptr i64 %ln6ef to i64*
  store i64* %ln6eg, i64** %Sp_Var
  %ln6eh = bitcast i8* @stg_ap_0_fast to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6ei = load i64*, i64** %Sp_Var
  %ln6ej = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6eh( i64* %Base_Arg, i64* %ln6ei, i64* %Hp_Arg, i64 %ln6ej, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6ca:
  %ln6em = load i64, i64* %R1_Var
  %ln6en = add i64 %ln6em, 6
  %ln6eo = inttoptr i64 %ln6en to i64*
  %ln6ep = load i64, i64* %ln6eo, !tbaa !4
  %ln6eq = and i64 %ln6ep, -8
  store i64 %ln6eq, i64* %R1_Var
  %ln6er = load i64*, i64** %Sp_Var
  %ln6es = getelementptr inbounds i64, i64* %ln6er, i32 1
  %ln6et = ptrtoint i64* %ln6es to i64
  %ln6eu = inttoptr i64 %ln6et to i64*
  store i64* %ln6eu, i64** %Sp_Var
  %ln6ew = load i64, i64* %R1_Var
  %ln6ex = inttoptr i64 %ln6ew to i64*
  %ln6ey = load i64, i64* %ln6ex, !tbaa !4
  %ln6ez = inttoptr i64 %ln6ey to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6eA = load i64*, i64** %Sp_Var
  %ln6eB = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6ez( i64* %Base_Arg, i64* %ln6eA, i64* %Hp_Arg, i64 %ln6eB, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@s66Q_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66Q_info$def to i8*)
define internal ghccc void @s66Q_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66Q_info$def to i64)),i64 24), i64 1, i64 133143986192}>
{
c6co:
  %ls66Q = alloca i64, i32 1
  %Hp_Var = alloca i64*, i32 1
  store i64* %Hp_Arg, i64** %Hp_Var
  %ls66v = alloca i64, i32 1
  %R4_Var = alloca i64, i32 1
  store i64 undef, i64* %R4_Var
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln6eC = load i64, i64* %R1_Var
  store i64 %ln6eC, i64* %ls66Q
  %ln6eD = load i64*, i64** %Sp_Var
  %ln6eE = getelementptr inbounds i64, i64* %ln6eD, i32 -2
  %ln6eF = ptrtoint i64* %ln6eE to i64
  %ln6eG = icmp ult i64 %ln6eF, %SpLim_Arg
  %ln6eH = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6eG, i1 0 )
  br i1 %ln6eH, label %c6cs, label %c6ct
c6ct:
  %ln6eI = load i64*, i64** %Hp_Var
  %ln6eJ = getelementptr inbounds i64, i64* %ln6eI, i32 3
  %ln6eK = ptrtoint i64* %ln6eJ to i64
  %ln6eL = inttoptr i64 %ln6eK to i64*
  store i64* %ln6eL, i64** %Hp_Var
  %ln6eM = load i64*, i64** %Hp_Var
  %ln6eN = ptrtoint i64* %ln6eM to i64
  %ln6eO = getelementptr inbounds i64, i64* %Base_Arg, i32 107
  %ln6eP = bitcast i64* %ln6eO to i64*
  %ln6eQ = load i64, i64* %ln6eP, !tbaa !5
  %ln6eR = icmp ugt i64 %ln6eN, %ln6eQ
  %ln6eS = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6eR, i1 0 )
  br i1 %ln6eS, label %c6cv, label %c6cu
c6cu:
  %ln6eT = load i64, i64* %ls66Q
  %ln6eU = add i64 %ln6eT, 16
  %ln6eV = inttoptr i64 %ln6eU to i64*
  %ln6eW = load i64, i64* %ln6eV, !tbaa !1
  store i64 %ln6eW, i64* %ls66v
  %ln6eY = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66A_info$def to i64
  %ln6eX = load i64*, i64** %Hp_Var
  %ln6eZ = getelementptr inbounds i64, i64* %ln6eX, i32 -2
  store i64 %ln6eY, i64* %ln6eZ, !tbaa !3
  %ln6f1 = load i64, i64* %ls66v
  %ln6f0 = load i64*, i64** %Hp_Var
  %ln6f2 = getelementptr inbounds i64, i64* %ln6f0, i32 0
  store i64 %ln6f1, i64* %ln6f2, !tbaa !3
  %ln6f4 = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cm_info$def to i64
  %ln6f3 = load i64*, i64** %Sp_Var
  %ln6f5 = getelementptr inbounds i64, i64* %ln6f3, i32 -2
  store i64 %ln6f4, i64* %ln6f5, !tbaa !2
  %ln6f6 = ptrtoint i8* @base_TextziRead_readEither7_closure to i64
  %ln6f7 = add i64 %ln6f6, 1
  store i64 %ln6f7, i64* %R4_Var
  %ln6f8 = ptrtoint i8* @base_TextziParserCombinatorsziReadPrec_minPrec_closure to i64
  store i64 %ln6f8, i64* %R3_Var
  %ln6f9 = ptrtoint i8* @base_GHCziRead_zdfReadInt2_closure to i64
  %ln6fa = add i64 %ln6f9, 1
  store i64 %ln6fa, i64* %R2_Var
  %ln6fc = load i64*, i64** %Hp_Var
  %ln6fd = getelementptr inbounds i64, i64* %ln6fc, i32 -2
  %ln6fe = ptrtoint i64* %ln6fd to i64
  %ln6fb = load i64*, i64** %Sp_Var
  %ln6ff = getelementptr inbounds i64, i64* %ln6fb, i32 -1
  store i64 %ln6fe, i64* %ln6ff, !tbaa !2
  %ln6fg = load i64*, i64** %Sp_Var
  %ln6fh = getelementptr inbounds i64, i64* %ln6fg, i32 -2
  %ln6fi = ptrtoint i64* %ln6fh to i64
  %ln6fj = inttoptr i64 %ln6fi to i64*
  store i64* %ln6fj, i64** %Sp_Var
  %ln6fk = bitcast i8* @base_GHCziRead_zdfReadIntzuzdsreadNumber_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6fl = load i64*, i64** %Sp_Var
  %ln6fm = load i64*, i64** %Hp_Var
  %ln6fn = load i64, i64* %R1_Var
  %ln6fo = load i64, i64* %R2_Var
  %ln6fp = load i64, i64* %R3_Var
  %ln6fq = load i64, i64* %R4_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6fk( i64* %Base_Arg, i64* %ln6fl, i64* %ln6fm, i64 %ln6fn, i64 %ln6fo, i64 %ln6fp, i64 %ln6fq, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6cv:
  %ln6fr = getelementptr inbounds i64, i64* %Base_Arg, i32 113
  store i64 24, i64* %ln6fr, !tbaa !5
  br label %c6cs
c6cs:
  %ln6fs = load i64, i64* %ls66Q
  store i64 %ln6fs, i64* %R1_Var
  %ln6ft = getelementptr inbounds i64, i64* %Base_Arg, i32 -2
  %ln6fu = bitcast i64* %ln6ft to i64*
  %ln6fv = load i64, i64* %ln6fu, !tbaa !5
  %ln6fw = inttoptr i64 %ln6fv to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6fx = load i64*, i64** %Sp_Var
  %ln6fy = load i64*, i64** %Hp_Var
  %ln6fz = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6fw( i64* %Base_Arg, i64* %ln6fx, i64* %ln6fy, i64 %ln6fz, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cm_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cm_info$def to i8*)
define internal ghccc void @c6cm_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cm_info$def to i64)),i64 48), i64 1, i64 12884901918}>
{
c6cm:
  %lc6c1 = alloca i64, i32 1
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln6fA = load i64*, i64** %Sp_Var
  %ln6fB = getelementptr inbounds i64, i64* %ln6fA, i32 1
  %ln6fC = bitcast i64* %ln6fB to i64*
  %ln6fD = load i64, i64* %ln6fC, !tbaa !2
  store i64 %ln6fD, i64* %lc6c1
  %ln6fF = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cr_info$def to i64
  %ln6fE = load i64*, i64** %Sp_Var
  %ln6fG = getelementptr inbounds i64, i64* %ln6fE, i32 1
  store i64 %ln6fF, i64* %ln6fG, !tbaa !2
  %ln6fH = load i64, i64* %lc6c1
  store i64 %ln6fH, i64* %R3_Var
  store i64 %R1_Arg, i64* %R2_Var
  %ln6fI = load i64*, i64** %Sp_Var
  %ln6fJ = getelementptr inbounds i64, i64* %ln6fI, i32 1
  %ln6fK = ptrtoint i64* %ln6fJ to i64
  %ln6fL = inttoptr i64 %ln6fK to i64*
  store i64* %ln6fL, i64** %Sp_Var
  %ln6fM = bitcast i8* @base_TextziParserCombinatorsziReadP_run_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6fN = load i64*, i64** %Sp_Var
  %ln6fO = load i64, i64* %R2_Var
  %ln6fP = load i64, i64* %R3_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6fM( i64* %Base_Arg, i64* %ln6fN, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln6fO, i64 %ln6fP, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cr_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cr_info$def to i8*)
define internal ghccc void @c6cr_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cr_info$def to i64)),i64 48), i64 0, i64 12884901918}>
{
c6cr:
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln6fQ = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cA_info$def to i64
  %ln6fR = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln6fQ, i64* %ln6fR, !tbaa !2
  store i64 %R1_Arg, i64* %R2_Var
  %ln6fS = bitcast i8* @base_TextziRead_readEither8_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6fT = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6fS( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln6fT, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cA_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cA_info$def to i8*)
define internal ghccc void @c6cA_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cA_info$def to i64)),i64 48), i64 0, i64 12884901918}>
{
c6cA:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ls66E = alloca i64, i32 1
  %ln6fU = load i64, i64* %R1_Var
  %ln6fV = and i64 %ln6fU, 7
  switch i64 %ln6fV, label %c6cH [i64 1, label %c6cH
i64 2, label %c6cM]
c6cH:
  %ln6fW = ptrtoint %Main_main3_closure_struct* @Main_main3_closure$def to i64
  store i64 %ln6fW, i64* %R1_Var
  %ln6fX = load i64*, i64** %Sp_Var
  %ln6fY = getelementptr inbounds i64, i64* %ln6fX, i32 1
  %ln6fZ = ptrtoint i64* %ln6fY to i64
  %ln6g0 = inttoptr i64 %ln6fZ to i64*
  store i64* %ln6g0, i64** %Sp_Var
  %ln6g2 = load i64, i64* %R1_Var
  %ln6g3 = inttoptr i64 %ln6g2 to i64*
  %ln6g4 = load i64, i64* %ln6g3, !tbaa !4
  %ln6g5 = inttoptr i64 %ln6g4 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6g6 = load i64*, i64** %Sp_Var
  %ln6g7 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6g5( i64* %Base_Arg, i64* %ln6g6, i64* %Hp_Arg, i64 %ln6g7, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6cM:
  %ln6g9 = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cK_info$def to i64
  %ln6g8 = load i64*, i64** %Sp_Var
  %ln6ga = getelementptr inbounds i64, i64* %ln6g8, i32 -1
  store i64 %ln6g9, i64* %ln6ga, !tbaa !2
  %ln6gd = load i64, i64* %R1_Var
  %ln6ge = add i64 %ln6gd, 6
  %ln6gf = inttoptr i64 %ln6ge to i64*
  %ln6gg = load i64, i64* %ln6gf, !tbaa !4
  store i64 %ln6gg, i64* %ls66E
  %ln6gj = load i64, i64* %R1_Var
  %ln6gk = add i64 %ln6gj, 14
  %ln6gl = inttoptr i64 %ln6gk to i64*
  %ln6gm = load i64, i64* %ln6gl, !tbaa !4
  store i64 %ln6gm, i64* %R1_Var
  %ln6go = load i64, i64* %ls66E
  %ln6gn = load i64*, i64** %Sp_Var
  %ln6gp = getelementptr inbounds i64, i64* %ln6gn, i32 0
  store i64 %ln6go, i64* %ln6gp, !tbaa !2
  %ln6gq = load i64*, i64** %Sp_Var
  %ln6gr = getelementptr inbounds i64, i64* %ln6gq, i32 -1
  %ln6gs = ptrtoint i64* %ln6gr to i64
  %ln6gt = inttoptr i64 %ln6gs to i64*
  store i64* %ln6gt, i64** %Sp_Var
  %ln6gu = load i64, i64* %R1_Var
  %ln6gv = and i64 %ln6gu, 7
  %ln6gw = icmp ne i64 %ln6gv, 0
  br i1 %ln6gw, label %u6dh, label %c6cN
c6cN:
  %ln6gy = load i64, i64* %R1_Var
  %ln6gz = inttoptr i64 %ln6gy to i64*
  %ln6gA = load i64, i64* %ln6gz, !tbaa !4
  %ln6gB = inttoptr i64 %ln6gA to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6gC = load i64*, i64** %Sp_Var
  %ln6gD = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6gB( i64* %Base_Arg, i64* %ln6gC, i64* %Hp_Arg, i64 %ln6gD, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
u6dh:
  %ln6gE = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cK_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6gF = load i64*, i64** %Sp_Var
  %ln6gG = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6gE( i64* %Base_Arg, i64* %ln6gF, i64* %Hp_Arg, i64 %ln6gG, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cK_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cK_info$def to i8*)
define internal ghccc void @c6cK_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cK_info$def to i64)),i64 48), i64 1, i64 4294967326}>
{
c6cK:
  %ls66E = alloca i64, i32 1
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln6gH = load i64, i64* %R1_Var
  %ln6gI = and i64 %ln6gH, 7
  switch i64 %ln6gI, label %c6d5 [i64 1, label %c6d5
i64 2, label %c6de]
c6d5:
  %ln6gJ = load i64*, i64** %Sp_Var
  %ln6gK = getelementptr inbounds i64, i64* %ln6gJ, i32 1
  %ln6gL = bitcast i64* %ln6gK to i64*
  %ln6gM = load i64, i64* %ln6gL, !tbaa !2
  store i64 %ln6gM, i64* %ls66E
  %ln6gO = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cR_info$def to i64
  %ln6gN = load i64*, i64** %Sp_Var
  %ln6gP = getelementptr inbounds i64, i64* %ln6gN, i32 1
  store i64 %ln6gO, i64* %ln6gP, !tbaa !2
  %ln6gQ = load i64, i64* %ls66E
  store i64 %ln6gQ, i64* %R1_Var
  %ln6gR = load i64*, i64** %Sp_Var
  %ln6gS = getelementptr inbounds i64, i64* %ln6gR, i32 1
  %ln6gT = ptrtoint i64* %ln6gS to i64
  %ln6gU = inttoptr i64 %ln6gT to i64*
  store i64* %ln6gU, i64** %Sp_Var
  %ln6gV = load i64, i64* %R1_Var
  %ln6gW = and i64 %ln6gV, 7
  %ln6gX = icmp ne i64 %ln6gW, 0
  br i1 %ln6gX, label %u6di, label %c6cS
c6cS:
  %ln6gZ = load i64, i64* %R1_Var
  %ln6h0 = inttoptr i64 %ln6gZ to i64*
  %ln6h1 = load i64, i64* %ln6h0, !tbaa !4
  %ln6h2 = inttoptr i64 %ln6h1 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6h3 = load i64*, i64** %Sp_Var
  %ln6h4 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6h2( i64* %Base_Arg, i64* %ln6h3, i64* %Hp_Arg, i64 %ln6h4, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
u6di:
  %ln6h5 = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cR_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6h6 = load i64*, i64** %Sp_Var
  %ln6h7 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6h5( i64* %Base_Arg, i64* %ln6h6, i64* %Hp_Arg, i64 %ln6h7, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6de:
  %ln6h8 = ptrtoint %Main_main2_closure_struct* @Main_main2_closure$def to i64
  store i64 %ln6h8, i64* %R1_Var
  %ln6h9 = load i64*, i64** %Sp_Var
  %ln6ha = getelementptr inbounds i64, i64* %ln6h9, i32 2
  %ln6hb = ptrtoint i64* %ln6ha to i64
  %ln6hc = inttoptr i64 %ln6hb to i64*
  store i64* %ln6hc, i64** %Sp_Var
  %ln6he = load i64, i64* %R1_Var
  %ln6hf = inttoptr i64 %ln6he to i64*
  %ln6hg = load i64, i64* %ln6hf, !tbaa !4
  %ln6hh = inttoptr i64 %ln6hg to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6hi = load i64*, i64** %Sp_Var
  %ln6hj = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6hh( i64* %Base_Arg, i64* %ln6hi, i64* %Hp_Arg, i64 %ln6hj, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cR_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cR_info$def to i8*)
define internal ghccc void @c6cR_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c6cR:
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln6hk = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cW_info$def to i64
  %ln6hl = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln6hk, i64* %ln6hl, !tbaa !2
  %ln6hm = add i64 %R1_Arg, 7
  %ln6hn = inttoptr i64 %ln6hm to i64*
  %ln6ho = load i64, i64* %ln6hn, !tbaa !4
  store i64 %ln6ho, i64* %R2_Var
  %ln6hp = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_zdwfib_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6hq = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6hp( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln6hq, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6cW_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6cW_info$def to i8*)
define internal ghccc void @c6cW_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c6cW:
  %R4_Var = alloca i64, i32 1
  store i64 undef, i64* %R4_Var
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln6hr = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6d0_info$def to i64
  %ln6hs = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln6hr, i64* %ln6hs, !tbaa !2
  %ln6ht = ptrtoint i8* @ghczmprim_GHCziTypes_ZMZN_closure to i64
  %ln6hu = add i64 %ln6ht, 1
  store i64 %ln6hu, i64* %R4_Var
  store i64 %R1_Arg, i64* %R3_Var
  store i64 0, i64* %R2_Var
  %ln6hv = bitcast i8* @base_GHCziShow_zdwshowSignedInt_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6hw = load i64, i64* %R2_Var
  %ln6hx = load i64, i64* %R3_Var
  %ln6hy = load i64, i64* %R4_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6hv( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln6hw, i64 %ln6hx, i64 %ln6hy, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6d0_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6d0_info$def to i8*)
define internal ghccc void @c6d0_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c6d0:
  %Hp_Var = alloca i64*, i32 1
  store i64* %Hp_Arg, i64** %Hp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R2_Var = alloca i64, i32 1
  store i64 %R2_Arg, i64* %R2_Var
  %ln6hz = load i64*, i64** %Hp_Var
  %ln6hA = getelementptr inbounds i64, i64* %ln6hz, i32 3
  %ln6hB = ptrtoint i64* %ln6hA to i64
  %ln6hC = inttoptr i64 %ln6hB to i64*
  store i64* %ln6hC, i64** %Hp_Var
  %ln6hD = load i64*, i64** %Hp_Var
  %ln6hE = ptrtoint i64* %ln6hD to i64
  %ln6hF = getelementptr inbounds i64, i64* %Base_Arg, i32 107
  %ln6hG = bitcast i64* %ln6hF to i64*
  %ln6hH = load i64, i64* %ln6hG, !tbaa !5
  %ln6hI = icmp ugt i64 %ln6hE, %ln6hH
  %ln6hJ = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6hI, i1 0 )
  br i1 %ln6hJ, label %c6da, label %c6d9
c6d9:
  %ln6hL = ptrtoint i8* @ghczmprim_GHCziTypes_ZC_con_info to i64
  %ln6hK = load i64*, i64** %Hp_Var
  %ln6hM = getelementptr inbounds i64, i64* %ln6hK, i32 -2
  store i64 %ln6hL, i64* %ln6hM, !tbaa !3
  %ln6hO = load i64, i64* %R1_Var
  %ln6hN = load i64*, i64** %Hp_Var
  %ln6hP = getelementptr inbounds i64, i64* %ln6hN, i32 -1
  store i64 %ln6hO, i64* %ln6hP, !tbaa !3
  %ln6hR = load i64, i64* %R2_Var
  %ln6hQ = load i64*, i64** %Hp_Var
  %ln6hS = getelementptr inbounds i64, i64* %ln6hQ, i32 0
  store i64 %ln6hR, i64* %ln6hS, !tbaa !3
  %ln6hU = load i64*, i64** %Hp_Var
  %ln6hV = ptrtoint i64* %ln6hU to i64
  %ln6hW = add i64 %ln6hV, -14
  store i64 %ln6hW, i64* %R1_Var
  %ln6hX = load i64*, i64** %Sp_Var
  %ln6hY = getelementptr inbounds i64, i64* %ln6hX, i32 1
  %ln6hZ = ptrtoint i64* %ln6hY to i64
  %ln6i0 = inttoptr i64 %ln6hZ to i64*
  store i64* %ln6i0, i64** %Sp_Var
  %ln6i1 = load i64*, i64** %Sp_Var
  %ln6i2 = getelementptr inbounds i64, i64* %ln6i1, i32 0
  %ln6i3 = bitcast i64* %ln6i2 to i64*
  %ln6i4 = load i64, i64* %ln6i3, !tbaa !2
  %ln6i5 = inttoptr i64 %ln6i4 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6i6 = load i64*, i64** %Sp_Var
  %ln6i7 = load i64*, i64** %Hp_Var
  %ln6i8 = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6i5( i64* %Base_Arg, i64* %ln6i6, i64* %ln6i7, i64 %ln6i8, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6da:
  %ln6i9 = getelementptr inbounds i64, i64* %Base_Arg, i32 113
  store i64 24, i64* %ln6i9, !tbaa !5
  %ln6ia = load i64, i64* %R2_Var
  store i64 %ln6ia, i64* %R2_Var
  %ln6ib = load i64, i64* %R1_Var
  store i64 %ln6ib, i64* %R1_Var
  %ln6ic = bitcast i8* @stg_gc_pp to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6id = load i64*, i64** %Sp_Var
  %ln6ie = load i64*, i64** %Hp_Var
  %ln6if = load i64, i64* %R1_Var
  %ln6ig = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6ic( i64* %Base_Arg, i64* %ln6id, i64* %ln6ie, i64 %ln6if, i64 %ln6ig, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@Main_main1_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main1_info$def to i8*)
define ghccc void @Main_main1_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main1_info$def to i64)),i64 24), i64 4294967299, i64 0, i64 2194728288270}>
{
c6dl:
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln6ih = load i64*, i64** %Sp_Var
  %ln6ii = getelementptr inbounds i64, i64* %ln6ih, i32 -1
  %ln6ij = ptrtoint i64* %ln6ii to i64
  %ln6ik = icmp ult i64 %ln6ij, %SpLim_Arg
  %ln6il = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6ik, i1 0 )
  br i1 %ln6il, label %c6dm, label %c6dn
c6dn:
  %ln6in = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6bV_info$def to i64
  %ln6im = load i64*, i64** %Sp_Var
  %ln6io = getelementptr inbounds i64, i64* %ln6im, i32 -1
  store i64 %ln6in, i64* %ln6io, !tbaa !2
  %ln6ip = load i64*, i64** %Sp_Var
  %ln6iq = getelementptr inbounds i64, i64* %ln6ip, i32 -1
  %ln6ir = ptrtoint i64* %ln6iq to i64
  %ln6is = inttoptr i64 %ln6ir to i64*
  store i64* %ln6is, i64** %Sp_Var
  %ln6it = bitcast i8* @base_SystemziEnvironment_getArgs1_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6iu = load i64*, i64** %Sp_Var
  %ln6iv = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6it( i64* %Base_Arg, i64* %ln6iu, i64* %Hp_Arg, i64 %ln6iv, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6dm:
  %ln6iw = ptrtoint %Main_main1_closure_struct* @Main_main1_closure$def to i64
  store i64 %ln6iw, i64* %R1_Var
  %ln6ix = getelementptr inbounds i64, i64* %Base_Arg, i32 -1
  %ln6iy = bitcast i64* %ln6ix to i64*
  %ln6iz = load i64, i64* %ln6iy, !tbaa !5
  %ln6iA = inttoptr i64 %ln6iz to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6iB = load i64*, i64** %Sp_Var
  %ln6iC = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6iA( i64* %Base_Arg, i64* %ln6iB, i64* %Hp_Arg, i64 %ln6iC, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6bV_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6bV_info$def to i8*)
define internal ghccc void @c6bV_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6bV_info$def to i64)),i64 24), i64 0, i64 957777707038}>
{
c6bV:
  %Hp_Var = alloca i64*, i32 1
  store i64* %Hp_Arg, i64** %Hp_Var
  %R4_Var = alloca i64, i32 1
  store i64 undef, i64* %R4_Var
  %R3_Var = alloca i64, i32 1
  store i64 undef, i64* %R3_Var
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %ln6iD = load i64*, i64** %Hp_Var
  %ln6iE = getelementptr inbounds i64, i64* %ln6iD, i32 3
  %ln6iF = ptrtoint i64* %ln6iE to i64
  %ln6iG = inttoptr i64 %ln6iF to i64*
  store i64* %ln6iG, i64** %Hp_Var
  %ln6iH = load i64*, i64** %Hp_Var
  %ln6iI = ptrtoint i64* %ln6iH to i64
  %ln6iJ = getelementptr inbounds i64, i64* %Base_Arg, i32 107
  %ln6iK = bitcast i64* %ln6iJ to i64*
  %ln6iL = load i64, i64* %ln6iK, !tbaa !5
  %ln6iM = icmp ugt i64 %ln6iI, %ln6iL
  %ln6iN = call ccc i1 (i1, i1) @llvm.expect.i1( i1 %ln6iM, i1 0 )
  br i1 %ln6iN, label %c6dq, label %c6dp
c6dp:
  %ln6iP = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @s66Q_info$def to i64
  %ln6iO = load i64*, i64** %Hp_Var
  %ln6iQ = getelementptr inbounds i64, i64* %ln6iO, i32 -2
  store i64 %ln6iP, i64* %ln6iQ, !tbaa !3
  %ln6iS = load i64, i64* %R1_Var
  %ln6iR = load i64*, i64** %Hp_Var
  %ln6iT = getelementptr inbounds i64, i64* %ln6iR, i32 0
  store i64 %ln6iS, i64* %ln6iT, !tbaa !3
  %ln6iU = ptrtoint void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6dj_info$def to i64
  %ln6iV = getelementptr inbounds i64, i64* %Sp_Arg, i32 0
  store i64 %ln6iU, i64* %ln6iV, !tbaa !2
  %ln6iW = ptrtoint i8* @ghczmprim_GHCziTypes_True_closure to i64
  %ln6iX = add i64 %ln6iW, 2
  store i64 %ln6iX, i64* %R4_Var
  %ln6iY = load i64*, i64** %Hp_Var
  %ln6iZ = getelementptr inbounds i64, i64* %ln6iY, i32 -2
  %ln6j0 = ptrtoint i64* %ln6iZ to i64
  store i64 %ln6j0, i64* %R3_Var
  %ln6j1 = ptrtoint i8* @base_GHCziIOziHandleziFD_stdout_closure to i64
  store i64 %ln6j1, i64* %R2_Var
  %ln6j2 = bitcast i8* @base_GHCziIOziHandleziText_hPutStr2_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6j3 = load i64*, i64** %Hp_Var
  %ln6j4 = load i64, i64* %R1_Var
  %ln6j5 = load i64, i64* %R2_Var
  %ln6j6 = load i64, i64* %R3_Var
  %ln6j7 = load i64, i64* %R4_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6j2( i64* %Base_Arg, i64* %Sp_Arg, i64* %ln6j3, i64 %ln6j4, i64 %ln6j5, i64 %ln6j6, i64 %ln6j7, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
c6dq:
  %ln6j8 = getelementptr inbounds i64, i64* %Base_Arg, i32 113
  store i64 24, i64* %ln6j8, !tbaa !5
  %ln6j9 = load i64, i64* %R1_Var
  store i64 %ln6j9, i64* %R1_Var
  %ln6ja = bitcast i8* @stg_gc_unpt_r1 to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6jb = load i64*, i64** %Hp_Var
  %ln6jc = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6ja( i64* %Base_Arg, i64* %Sp_Arg, i64* %ln6jb, i64 %ln6jc, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
@c6dj_info = internal alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @c6dj_info$def to i8*)
define internal ghccc void @c6dj_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64}><{i64 0, i64 30}>
{
c6dj:
  %R1_Var = alloca i64, i32 1
  store i64 %R1_Arg, i64* %R1_Var
  %Sp_Var = alloca i64*, i32 1
  store i64* %Sp_Arg, i64** %Sp_Var
  %ln6jd = ptrtoint i8* @ghczmprim_GHCziTuple_Z0T_closure to i64
  %ln6je = add i64 %ln6jd, 1
  store i64 %ln6je, i64* %R1_Var
  %ln6jf = load i64*, i64** %Sp_Var
  %ln6jg = getelementptr inbounds i64, i64* %ln6jf, i32 1
  %ln6jh = ptrtoint i64* %ln6jg to i64
  %ln6ji = inttoptr i64 %ln6jh to i64*
  store i64* %ln6ji, i64** %Sp_Var
  %ln6jj = load i64*, i64** %Sp_Var
  %ln6jk = getelementptr inbounds i64, i64* %ln6jj, i32 0
  %ln6jl = bitcast i64* %ln6jk to i64*
  %ln6jm = load i64, i64* %ln6jl, !tbaa !2
  %ln6jn = inttoptr i64 %ln6jm to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6jo = load i64*, i64** %Sp_Var
  %ln6jp = load i64, i64* %R1_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6jn( i64* %Base_Arg, i64* %ln6jo, i64* %Hp_Arg, i64 %ln6jp, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_main_closure_struct = type <{i64, i64}>
@Main_main_closure$def = internal global %Main_main_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main_info$def to i64), i64 0}>
@Main_main_closure = alias i8, bitcast (%Main_main_closure_struct* @Main_main_closure$def to i8*)
@Main_main_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main_info$def to i8*)
define ghccc void @Main_main_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main_info$def to i64)),i64 88), i64 4294967299, i64 0, i64 4294967310}>
{
c6ju:
  %ln6jx = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main1_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6jx( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%Main_main4_closure_struct = type <{i64, i64}>
@Main_main4_closure$def = internal global %Main_main4_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main4_info$def to i64), i64 0}>
@Main_main4_closure = alias i8, bitcast (%Main_main4_closure_struct* @Main_main4_closure$def to i8*)
@Main_main4_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main4_info$def to i8*)
define ghccc void @Main_main4_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main4_info$def to i64)),i64 88), i64 4294967299, i64 0, i64 12884901902}>
{
c6jC:
  %R2_Var = alloca i64, i32 1
  store i64 undef, i64* %R2_Var
  %ln6jF = ptrtoint %Main_main1_closure_struct* @Main_main1_closure$def to i64
  %ln6jG = add i64 %ln6jF, 1
  store i64 %ln6jG, i64* %R2_Var
  %ln6jH = bitcast i8* @base_GHCziTopHandler_runMainIO1_info to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  %ln6jI = load i64, i64* %R2_Var
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6jH( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 %ln6jI, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%ZCMain_main_closure_struct = type <{i64, i64}>
@ZCMain_main_closure$def = internal global %ZCMain_main_closure_struct<{i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @ZCMain_main_info$def to i64), i64 0}>
@ZCMain_main_closure = alias i8, bitcast (%ZCMain_main_closure_struct* @ZCMain_main_closure$def to i8*)
@ZCMain_main_info = alias i8, bitcast (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @ZCMain_main_info$def to i8*)
define ghccc void @ZCMain_main_info$def(i64* noalias nocapture %Base_Arg, i64* noalias nocapture %Sp_Arg, i64* noalias nocapture %Hp_Arg, i64 %R1_Arg, i64 %R2_Arg, i64 %R3_Arg, i64 %R4_Arg, i64 %R5_Arg, i64 %R6_Arg, i64 %SpLim_Arg) align 8 nounwind prefix <{i64, i64, i64, i64}><{i64 add (i64 sub (i64 ptrtoint (i8* @S6aj_srt to i64),i64 ptrtoint (void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @ZCMain_main_info$def to i64)),i64 104), i64 4294967299, i64 0, i64 4294967310}>
{
c6jN:
  %ln6jQ = bitcast void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)* @Main_main4_info$def to void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64)*
  tail call ghccc void (i64*, i64*, i64*, i64, i64, i64, i64, i64, i64, i64) %ln6jQ( i64* %Base_Arg, i64* %Sp_Arg, i64* %Hp_Arg, i64 %R1_Arg, i64 undef, i64 undef, i64 undef, i64 undef, i64 undef, i64 %SpLim_Arg ) nounwind
  ret void
}
%S6aj_srt_struct = type <{i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64}>
@S6aj_srt$def = internal constant %S6aj_srt_struct<{i64 ptrtoint (i8* @base_GHCziErr_errorWithoutStackTrace_closure to i64), i64 ptrtoint (i8* @base_TextziRead_readEither5_closure to i64), i64 ptrtoint (i8* @base_TextziRead_readEither2_closure to i64), i64 ptrtoint (i8* @base_GHCziList_badHead_closure to i64), i64 ptrtoint (i8* @base_GHCziRead_zdfReadIntzuzdsreadNumber_closure to i64), i64 ptrtoint (i8* @base_GHCziRead_zdfReadInt2_closure to i64), i64 ptrtoint (%Main_main2_closure_struct* @Main_main2_closure$def to i64), i64 ptrtoint (%Main_main3_closure_struct* @Main_main3_closure$def to i64), i64 ptrtoint (i8* @base_SystemziEnvironment_getArgs1_closure to i64), i64 ptrtoint (i8* @base_GHCziIOziHandleziFD_stdout_closure to i64), i64 ptrtoint (i8* @base_GHCziIOziHandleziText_hPutStr2_closure to i64), i64 ptrtoint (%Main_main1_closure_struct* @Main_main1_closure$def to i64), i64 ptrtoint (i8* @base_GHCziTopHandler_runMainIO1_closure to i64), i64 ptrtoint (%Main_main4_closure_struct* @Main_main4_closure$def to i64)}>
@S6aj_srt = internal alias i8, bitcast (%S6aj_srt_struct* @S6aj_srt$def to i8*)
@ghczmprim_GHCziTypes_TrNameS_con_info = external global i8
@ghczmprim_GHCziTypes_Module_con_info = external global i8
@ghczmprim_GHCziTypes_Izh_con_info = external global i8
@stg_gc_unbx_r1 = external global i8
@newCAF = external global i8
@stg_bh_upd_frame_info = external global i8
@base_TextziRead_readEither5_closure = external global i8
@base_GHCziErr_errorWithoutStackTrace_info = external global i8
@base_TextziRead_readEither2_closure = external global i8
@stg_upd_frame_info = external global i8
@base_GHCziList_badHead_closure = external global i8
@stg_ap_0_fast = external global i8
@base_TextziRead_readEither7_closure = external global i8
@base_TextziParserCombinatorsziReadPrec_minPrec_closure = external global i8
@base_GHCziRead_zdfReadInt2_closure = external global i8
@base_GHCziRead_zdfReadIntzuzdsreadNumber_info = external global i8
@base_TextziParserCombinatorsziReadP_run_info = external global i8
@base_TextziRead_readEither8_info = external global i8
@ghczmprim_GHCziTypes_ZMZN_closure = external global i8
@base_GHCziShow_zdwshowSignedInt_info = external global i8
@ghczmprim_GHCziTypes_ZC_con_info = external global i8
@stg_gc_pp = external global i8
@base_SystemziEnvironment_getArgs1_info = external global i8
@ghczmprim_GHCziTypes_True_closure = external global i8
@base_GHCziIOziHandleziFD_stdout_closure = external global i8
@base_GHCziIOziHandleziText_hPutStr2_info = external global i8
@stg_gc_unpt_r1 = external global i8
@ghczmprim_GHCziTuple_Z0T_closure = external global i8
@base_GHCziTopHandler_runMainIO1_info = external global i8
@base_GHCziErr_errorWithoutStackTrace_closure = external global i8
@base_GHCziRead_zdfReadIntzuzdsreadNumber_closure = external global i8
@base_SystemziEnvironment_getArgs1_closure = external global i8
@base_GHCziIOziHandleziText_hPutStr2_closure = external global i8
@base_GHCziTopHandler_runMainIO1_closure = external global i8
@llvm.used = appending constant [14 x i8*] [i8* bitcast (%S6aj_srt_struct* @S6aj_srt$def to i8*), i8* bitcast (%ZCMain_main_closure_struct* @ZCMain_main_closure$def to i8*), i8* bitcast (%Main_main4_closure_struct* @Main_main4_closure$def to i8*), i8* bitcast (%Main_main_closure_struct* @Main_main_closure$def to i8*), i8* bitcast (%Main_main1_closure_struct* @Main_main1_closure$def to i8*), i8* bitcast (%Main_main2_closure_struct* @Main_main2_closure$def to i8*), i8* bitcast (%Main_main3_closure_struct* @Main_main3_closure$def to i8*), i8* bitcast (%Main_fib_closure_struct* @Main_fib_closure$def to i8*), i8* bitcast (%Main_zdwfib_closure_struct* @Main_zdwfib_closure$def to i8*), i8* bitcast (%Main_zdtrModule_closure_struct* @Main_zdtrModule_closure$def to i8*), i8* bitcast (%Main_zdtrModule1_closure_struct* @Main_zdtrModule1_closure$def to i8*), i8* bitcast (%Main_zdtrModule2_bytes_struct* @Main_zdtrModule2_bytes$def to i8*), i8* bitcast (%Main_zdtrModule3_closure_struct* @Main_zdtrModule3_closure$def to i8*), i8* bitcast (%Main_zdtrModule4_bytes_struct* @Main_zdtrModule4_bytes$def to i8*)], section "llvm.metadata"
