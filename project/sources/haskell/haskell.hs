module Main where

import System.Environment (getArgs)

fib :: Int -> Int
fib n
  | n <= 1 = 1
  | otherwise = fib (n - 1) + (fib (n - 2))

main :: IO ()
main = do
  args <- getArgs
  let num = read (head args)
  putStrLn (show (fib num))
  return ()
