\documentclass{scrartcl}

\usepackage{hyperref}
\usepackage{minted}
%\newcommand{\JI}{\mintinline{Java}}
\usepackage{tikz}
\usetikzlibrary{shapes.multipart}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{semantic}
\usepackage{tikz}
\usetikzlibrary{positioning,shapes,fit,arrows,automata}

\title{Software Analysis}
\subtitle{Assignment 4}

\author{Erin van der Veen\\s4431200}

\begin{document}

\maketitle

\section{Theory}
\subsection{Distinction ``,'' and ``;''.}
At some instances it is needed to return a value.
This means that we must have an expression at this instance.
Unfortunately, $\langle \text{expr} \rangle$ does not contain the expression.
Nor would this work, because we want to be sure it returns a value.
In order to allow statements in expression, while preserving the fact that expressions must always return values, we add the ``,'' construct.

\subsection{While Construct}
Extension to grammar:
$$\langle \texttt{stmt} \rangle ::= \texttt{ 'while' } \langle \texttt{expr} \rangle \texttt{ 'then' } \langle \texttt{stmt} \rangle \texttt{ 'end' }$$
Extension to Regular Semantics:

\resizebox{\textwidth}{!}{%
$$\inference[(sWhile-True)]{\Delta^s \vdash \langle e, \sigma, \Gamma \rangle \stackrel{e}{\rightarrow} \langle n, \sigma', \Gamma' \rangle & n \neq 0 & \Delta^s \vdash \langle S, \sigma', \Gamma' \rangle \stackrel{s}{\rightarrow} \langle \sigma'', \Gamma'' \rangle & \Delta^s \vdash \langle \text{while } e \text{ then } S \text{ end}, \sigma'', \Gamma'' \rangle \stackrel{s}{\rightarrow} \langle \sigma''', \Gamma'''\rangle}{\Delta^s \vdash \langle \text{while } e \text{ then } S \text{ end}, \sigma, \Gamma \rangle \stackrel{s}{\rightarrow} \langle \sigma''', \Gamma'''\rangle}$$
}
$$\inference[(sWhile-False)]{\Delta^s \vdash \langle e, \sigma, \Gamma \rangle \stackrel{e}{\rightarrow} \langle 0, \sigma', \Gamma' \rangle}{\Delta^s \vdash \langle \text{while } e \text{ then } S \text{ end}, \sigma, \Gamma \rangle \stackrel{s}{\rightarrow} \langle \sigma', \Gamma' \rangle}$$

\subsection{Missing Language Features}
I think that the language as is is Turing complete.
There are, however, many features that one would like in a language.
Global variables, arrays (or lists), return-statements, more types, etc.
Most notably, however, is any form of ``sleep()'' functionality.
I would like to extend the system with this as well.
$$\inference[(sSleep)]{}{\Delta^s \vdash \langle sleep(n), \sigma, \Gamma \rangle \stackrel{s}{\rightarrow} \langle \sigma, \Gamma \rangle}$$
In this case it is not trivial what the energy aware semantics should be:
$$\inference[(sSleep)]{}{\Delta^{es};\Phi \vdash \langle sleep(n), \sigma, \Gamma \rangle \stackrel{s}{\rightarrow} \langle \sigma, \Gamma, \Phi(\Gamma) * n \rangle}$$

\section{Modelling}
\subsection{Control Software}
\texttt{https://github.com/bmantoni/pi-hue-motion}.

Given that this project was created for the Ardiuno, I think it is reasonable easy to translate it to ECA.

\subsection{Hardware Model}
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.8cm,semithick]

		\node[initial,state]   (0)              {};
		\node[label={$\Phi(t)=0$}, above = -0.3cm of 0] (l0) {};
		\node[state, right = of 0]   (1)              {};
		\node[label={$\Phi(t)=0$}, above = -0.3cm of 1] (l1) {};
		\node[state, right = of 1]   (2)              {};
		\node[label={$\Phi(t)=0.1t$}, above = -0.3cm of 2] (l2) {};

		\path
		(0) edge node {0.5 $\vert$ setmode()}   (1)
		(1) edge [loop below] node {0.5 $\vert$ setmode()}   (1)
		(1) edge node {0.5 $\vert$ setup()}   (2)
		(2) edge [loop below] node {0.5 $\vert$ setup()}   (2)
		(2) edge [loop right] node {0.5 $\vert$ input()}   (2);

	\end{tikzpicture}
	\caption{Sensor Model}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=5cm,semithick]

		\node[initial,state]       (0)              {};
		\node[label={$\Phi(t)=0$}, above=-0.3cm of 0] (l0)  {};
		\node[state, right of=0]   (1)              {OFF};
		\node[label={$\Phi(t)=0.1t$}, above=0.3cm of 1] (l1)  {};
		\node[state, right of=1]   (2)              {ON};
		\node[label={$\Phi(t)=2t$}, above=0.3cm of 2] (l2)  {};

		\path
		(0) edge node {0.5 $\vert$ \_init\_()} (1)
		(1) edge [bend left = 20] node {0.5 $\vert$ turnLightsOn()}  (2)
		(1) edge [loop below] node {0.5 $\vert$ getLightStatus()}  (1)
		(2) edge [bend left = 20] node {0.5 $\vert$ turnLightsOff()}  (1)
		(2) edge [loop below] node {0.5 $\vert$ getLightStatus()}  (2);

	\end{tikzpicture}
	\caption{Hue Model}
\end{figure}

Figure 1 shows the model for the motion sensor. Note that the power consumption of the sensor probably depends on wether the sensor is turned off/on. I decided to model it in such a way that the setup() function turns the sensor on. This is probably not true in the actual hardware, but it would not be hard to make it so. setmode() is in reality a syscall, but since I will not actually include the kernel in my analysis, it is easier to model it as being part of the hardware. I have also chosen that setmode() must occur before setup(), but they can be called more than once. I do not know if this is the right way to model it, but it allows me to ensure that setup was completed upon reaching the third state and thus prevent any kind of crash.

Figure 2 shows the model for the Hue lamps. I have eliminated API and instead modeled it as if the hue lamps are connected directly to the Raspberry pi. I have also removed any function calls that we not used, and abstracted from the interface that comes with the program. I chose to do this because they are all constant in time, so they might as well be incorporated in the hardware model. An assumption that I make is that the lamps are turned off when they are initialized. I chose to do this because I am not quite sure how to model transition constraints in this type automaton, or whether this would mess with the analysis.

\subsection{Precision}
I have not measured the actual consumption of the transitions.
In order to create a more accurate model, I would have to do this first.
The extension with the sleep function works reasonably well, I think.
The fact that I abstract from the bridge of the Hue lamps makes the analysis much less precise.
Unfortunately, I think it is impossible to include it in the analysis, since the bridge can communicate with many lamps/controllers at the same time.

\end{document}
