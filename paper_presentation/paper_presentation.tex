\documentclass[aspectratio=169, 10pt]{beamer}

\usetheme{metropolis}

\usepackage{xcolor}
\usepackage{minted}
\usepackage{csquotes}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,positioning}
\tikzstyle{block} = [draw,fill=none,rectangle,minimum height=3em, minimum width=6em]

\title{Software Analysis}
\subtitle{Static energy consumption analysis of LLVM IR programs}
\author{Erin van der Veen$^{\textcolor{yellow}{\heartsuit}}$ \and Jesper van de Putte$^{\textcolor{yellow}{\heartsuit}}$}
\institute{$^{\textcolor{yellow}{\heartsuit}}$Radboud University Nijmegen}

\begin{document}

\maketitle

\begin{frame}{Why LLVM}
	Level of LLVM:
	\begin{itemize}
		\item High Level Programming Languages (\texttt{C, Java, Haskell})
		\item LLVM IR
		\item Low Level Programming Languages (\texttt{x86, ARM64})
	\end{itemize}

	What LLVM can do:
	\begin{itemize}
		\item Multiple Languages: \texttt{C, C++, C\#, Haskell, Java, Rust \dots}
		\item Multiple Architectures: \texttt{x86, x86-64, ARM, ARM64 \dots}
	\end{itemize}
\end{frame}

\begin{frame}{What is LLVM I}
	\begin{figure}
		\centering
		\begin{tikzpicture}[auto, node distance=2cm,->,shorten <=5pt,shorten >=5pt]
			\node[block] (C) {\texttt{C, C++, XC}};
			\node[block,below of=C] (Haskell) {\texttt{Haskell}};
			\node[block,below of=Haskell] (Rust) {\texttt{Rust}};

			\node[block,right=of Haskell] (IR) {\texttt{LLVM IR}};

			\node[block,right=of IR] (x8664) {\texttt{x86-64}};
			\node[block,above of=x8664] (x86) {\texttt{x86}};
			\node[block,below of=x8664] (ARM64) {\texttt{ARM64}};

			\draw (C) -- (IR);
			\draw (Haskell) -- (IR);
			\draw (Rust) -- (IR);

			\draw (IR) -- (x8664);
			\draw (IR) -- (x86);
			\draw (IR) -- (ARM64);
		\end{tikzpicture}
	\end{figure}
\end{frame}

\begin{frame}[fragile]{What is LLVM II}
	\begin{minted}{llvm}
@.hello_str = private unnamed_addr constant [15 x i8] c"Hello, World!\0A\00"

declare i32 @puts(i8* nocapture) nounwind

define i32 @main() {
	%hello_ptr = getelementptr [15 x i8]* @.hello_str, i64 0, i64 0

	%ret = call i32 @puts(i8* %hello_ptr)
	ret i32 %ret
}
	\end{minted}
\end{frame}

\begin{frame}{LLVM-IR I}
	% KNOW THAT LLVM-IR is single assignment language
	\begin{align*}
		inst &= \texttt{br }p\ BB_1 BB_2\\
		&\mid x = \texttt{op} a_1 \dots a_2\\
		&\mid x = \phi \langle BB_1,x_1 \rangle \dots \langle BB_n,x_n \rangle\\
		&\mid x = \texttt{call } f\ a_1 \dots a_n\\
		&\mid x = \texttt{memload}\\
		&\mid \texttt{memstore}\\
		&\mid \texttt{ret } a
	\end{align*}
	With $p, f, a, x$ as predicates, functions, arguments and variables.
\end{frame}

\begin{frame}[fragile]{Single Assignment}
	\begin{minted}{llvm}
define i32 @main() {
	%1 = i32 5
	%2 = i32 4
	%1 = sub %1, %2

	ret i32 0
}
	\end{minted}
\end{frame}

\begin{frame}[fragile]{$\phi$ Instructions I}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\texttt{example.cpp}:

			\begin{minted}{C}
bool f(bool x, bool y) {
	bool r = x || y ;
	return r;
}
			\end{minted}
		\end{column}
		\pause
		\begin{column}{0.5\textwidth}
			\begin{minted}{C}
bool f(bool x, bool y) {
	bool r;
	if(x)
		r = true;
	else
		r = y;
	return r;
}
			\end{minted}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]{$\phi$ Instructions II}
	\texttt{clang -c -emit-llvm example.cpp -o example.bc \&\& llvm-dis example.bc}\\
	\texttt{example.ll}:

	\begin{minted}[fontsize=\tiny]{LLVM}
define zeroext i1 @_Z1fbb(i1 zeroext, i1 zeroext) #0 {
	%3 = alloca i8, align 1
	%4 = alloca i8, align 1
	%5 = alloca i8, align 1
	%6 = zext i1 %0 to i8
	store i8 %6, i8* %3, align 1
	%7 = zext i1 %1 to i8
	store i8 %7, i8* %4, align 1
	%8 = load i8, i8* %3, align 1
	%9 = trunc i8 %8 to i1
	br i1 %9, label %13, label %10

	; <label>:10: ; preds = %2
	%11 = load i8, i8* %4, align 1
	%12 = trunc i8 %11 to i1
	br label %13

	; <label>:13: ; preds = %10, %2
	%14 = phi i1 [ true, %2 ], [ %12, %10 ]
	%15 = zext i1 %14 to i8
	store i8 %15, i8* %5, align 1
	%16 = load i8, i8* %5, align 1
	%17 = trunc i8 %16 to i1
	ret i1 %17
}
	\end{minted}
\end{frame}

\begin{frame}{Energy Analysis on LLVM IR}
	\begin{figure}
		\centering
		\begin{tikzpicture}[auto, node distance=1.5cm,->]
			% Wide arrows
			\tikzstyle{myarrow}=[line width=0.3mm,draw=gray,-triangle 45,postaction={draw, line width=1mm, shorten >=2mm, -}]

			\node[block] (lang) {\texttt{C, XC, Haskell \dots}};
			\node[block,below of=lang] (ir) {optimized \texttt{LLVM IR}};
			\node[block,below of=ir] (isa) {\texttt{ISA}};

			\node[block,right=of ir] (ra) {Resource Analysis};
			\node[block,above of=ra] (lem) {LLVM IR E. Model};
			\node[block,below of=ra] (mt) {Mapper Tool};
			\node[block,below of=mt] (iem) {ISA E. Model};

			\draw[myarrow] (lang) -- (ir);
			\draw[myarrow] (ir) -- (isa);
			\draw[myarrow] (ir) -- (ra);
			\draw[myarrow] (ir) -- (mt);
			\draw[myarrow] (isa) -- (mt);
			\draw[myarrow] (lem) -- (ra);
			\draw[myarrow] (mt) -- (ra);
			\draw[myarrow] (iem) -- (mt);
		\end{tikzpicture}
	\end{figure}
\end{frame}

\begin{frame}{Resource consumption analysis for LLVM IR}
	Cost relations
	\begin{itemize}
		\item Recursively defined
		\item closely follow the flow of the program
	\end{itemize}

	We want to infer a closed form formula
	\begin{itemize}
		\item Parametric to relevant input arguments
		\item Requires solving using a cost relation solver
	\end{itemize}

	Solvers typically work with simplified control flow graph structures
	\begin{itemize}
		\item First simplify control flow graphs
	\end{itemize}
\end{frame}

\begin{frame}{Inferring block arguments}
	Block arguments
	\begin{itemize}
		\item Input data that flows into the block
			\begin{itemize}
				\item Consumed
				\item Propagated to another block 
			\end{itemize}
	\end{itemize}

	Solving multi-variate cost relations and recurrence relations automatically
	\begin{itemize}
		\item Still an open problem
		\item fewer arguments, easier to solve
	\end{itemize}

	Designed an analysis algorithm to minimize the block arguments before inferring the cost relation
\end{frame}

\begin{frame}{Algorithm inferring block arguments I}
	Define $gen$ which, given a basic block, returns the variables of interest in that block
	\[
		gen(BB) = gen_{blk}(BB) \cup gen_{fn}(BB)
	\]

	$gen_{blk}$ returns the input arguments that affect branching in a block $BB$, \\
	composed of $inst_1$ through $inst_n$. 
	\[
		gen_{blk}(BB) =
		\begin{cases}
			ref(seval(BB, p))   & \text{if } inst_n = \left[ \text{br } p  \text{ } .. \right]\\
			\emptyset           & \text{otherwise} 
		\end{cases}
	\]

	$gen_{fn}$ returns the variables that affect the input to any external calls in the block.
	\[
		gen_{fn}(BB) = \bigcup_{k=1}^{n} 
		\begin{cases}
			\cup_{i=1}^{m} ref(seval(BB, a_i))  & \text{if } inst_k \text{ is } \left[ x = \text{call } f \text{ } a_1 \text{ } .. \text{ } a_m \right] \\
			\emptyset                           & \text{otherwise} 
		\end{cases}
	\]
\end{frame}

\begin{frame}{Algorithm inferring block arguments II}
	Data flow analysis function $kill$ is defined as:
	\[
		kill(BB) = \bigcup_{k=1}^{n} 
		\begin{cases}
			\{x\} & \text{if } inst_k \text{ is } x = \text{call ...} \\
			\{x\} & \text{if } inst_k \text{ is } x = \text{op ...}\\
			\{x\} & \text{if } inst_k \text{ is } x = \text{memload ...}\\
			\emptyset                           & \text{otherwise} 
		\end{cases}
	\]
\end{frame}

\begin{frame}{Algorithm inferring block arguments III}
	Combine $gen$ and $kill$ by utilizing a transfer function which is inlined into $args_{in}$ and $args_{out}$.
	\[
		args_{out}(BB) = \bigcup_{BB'\in next(BB)} phimap_{\langle BB, BB' \rangle} (args_{in}(BB'))
	\]

	\[
		args_{in}(BB) = (args_{out}(BB) - kill(BB)) \cup gen(BB)
	\]

	Where $phimap$ maps the variables between adjecent blocks $BB$ and $BB'$ based on the $\phi$ instructions in $BB'$.

	Recumpute functions $args_{in}$ and $args_{out}$ until their least fixpoint is found. 
\end{frame}

\begin{frame}{Algorithm inferring block arguments IIII}
	Closely related to live variable analysis.

	A crucial difference in function $gen$:

	$gen$ returns a smaller subset of variables than live variable analysis.
	\begin{itemize}
		\item Only the ones that may affect control flow.
	\end{itemize}
\end{frame}

\begin{frame}{Generating and solving cost relations}
	Generate cost relations
	\begin{itemize}
		\item characterize the energy exerted by executing the instructions in a single block
		\item model the continuations of each block
			\begin{itemize}
				\item expressed as calls to other cost relations
				\item arise from either branching at the end of a block;
				\item or function calls in the middle of a block
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Example generating cost relation}
	LLVM IR block:
	\begin{minted}{llvm}
		LoopIncrement:
			%postinc = add i32 %i.0, 1
			%exitcond = icmp eq i32 %postinc, %1
		br i1 %exitcond, label %return, label %LoopBody
	\end{minted}

	Would translate into the cost relation:
	\begin{align*}
		C_{LI}(i) = C_0 + C_{ret}(i + 1) & \text{if } i + 1 = a_1\\
		C_{LI}(i) = C_1 + C_{LB}(i + 1) & \text{if } i + 1 \neq a_1
	\end{align*}

	Where $C_{LI}$, $C_{ret}$ and $C_{LB}$ characterize the energy exerted when running the blocks ``Loopincrement'', ``return'' and ``LoopBody'' respectively.

	$C_{ret}$ and $C_{LB}$ are continuations of $C_{LI}$.
\end{frame}

\begin{frame}{Transform to closed form}
	The cost relations extracted from recursive programs,
	as we have just seen how,
	can be automatically transformed to closed form by PUBS.
	PUBS uses techniques like:
	\begin{itemize}
		\item Computing ranking functions
		\item loop invariants
		\item intermediate results are mathematically composed to solve the whole set of given cost relations
	\end{itemize}
\end{frame}

\begin{frame}{Transformations for control flow graphs}
	After compilation, nested loop program structures are mangled by compiler optimizations.
	When the resulting Control Flow Graph is used directly, it is not possible to infer closed form solutions.

	For instance, PUBS can not handle complex CFG.

	In order to analyze the programs with nested loops, the CFG needs to be simplified.
	\begin{itemize}
		\item This is generally done at an early stage of the process, right after generating the CFG
	\end{itemize}
\end{frame}

\begin{frame}{Algorithm Simplify CFGs}
	\begin{enumerate}
		\item Identify a loop's CFG, A, that has nested loops
		\item Identify the sub-CFG, B, of A corresponding to the inner loop
		\item Extract B out of A, so that B is a separate CFG.
			\begin{itemize}
				\item This can be throught of as a new function with multiple return points
				\item Hence B's exit edges are removed
			\end{itemize}
		\item In A, in the place where B used to be, keep the continuation to B. Append a continuation to B's exit targets to B's caller in A.
	\end{enumerate}
\end{frame}

\begin{frame}[fragile]{Algorithm Simplify CFGs}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\texttt{example.cpp}:
			\begin{minted}{C}
void sort(int numbers[], int size) {
	int i=size, j, temp;
	while(j = i--)
	while(j--)
	if(numbers[j] > numbers[i]) {
		temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}
}
			\end{minted}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{CFG_inserttionsort.png}
		\end{column}
	\end{columns}
	Identify the nested loops, including the corresponding entries, re-entries, exit and loop headers.
\end{frame}

\begin{frame}[fragile]{Algorithm Simplify CFGs}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\texttt{example.cpp}:
			\begin{minted}{C}
void sort(int numbers[], int size) {
	int i=size, j, temp;
	while(j = i--)
	while(j--)
	if(numbers[j] > numbers[i]) {
		temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}
}
			\end{minted}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{CFG_inserttionsort.png}
		\end{column}
	\end{columns}
	Blocks \texttt{bb1}, \texttt{bb2} and \texttt{.backedge} form the inner loop. 
\end{frame}

\begin{frame}[fragile]{Algorithm Simplify CFGs}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\texttt{example.cpp}:
			\begin{minted}{C}
void sort(int numbers[], int size) {
	int i=size, j, temp;
	while(j = i--)
	while(j--)
	if(numbers[j] > numbers[i]) {
		temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}
}
			\end{minted}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{CFG_inserttionsort.png}
		\end{column}
	\end{columns}
	These blocks are hoisted and the exit edge from \texttt{.backedge} is eliminated.
\end{frame}

\begin{frame}[fragile]{Algorithm Simplify CFGs}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\texttt{example.cpp}:
			\begin{minted}{C}
void sort(int numbers[], int size) {
	int i=size, j, temp;
	while(j = i--)
	while(j--)
	if(numbers[j] > numbers[i]) {
		temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}
}
			\end{minted}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{CFG_inserttionsort.png}
		\end{column}
	\end{columns}
	Instead, \texttt{.loopexit} is then called after \texttt{bb1} ``returns''.
\end{frame}

\begin{frame}{Algorithm Simplify CFGs}
	\begin{itemize}
		\item Preserve the same order of operations when applied to an existing CFG compiled from typical \texttt{while} or \texttt{for} using \texttt{clang} or \texttt{xcc}.
		\item This means, the program on the left side will consume as much energy as the program on the right side.
		\item Limitation: When an induction variable of an outer loop is modified in an inner loop.
			\begin{itemize}
				\item Real benchmarks where this takes place has not been encountered.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Concerns}
	\begin{itemize}
		\item
			\dots the relation between the two is often blurred. For instance, optimizations such as dead code elimination \dots obfuscate the structure of the program and make the resultant code difficult to analyse.
		\item
			Specifically, we focus on \textit{optimized LLVM IR}, that has been compiled with optimization \dots
		\item
			\dots limitation of this approach is when an induction variable of an outer loop is modified in an inner loop. In this case the transformation cannot occur, however we have not encountered real benchmarks where this takes place.
		\item
			They test both \texttt{C} and \texttt{XC}, but they do not use the parallalize features of \texttt{XC}.
	\end{itemize}
\end{frame}

\begin{frame}{Results}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{results.png}
	\end{figure}
\end{frame}

\end{document}
